# -*- encoding : utf-8 -*-
require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
# require 'mina/rvm'    # for rvm support. (http://rvm.io)

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :user, 'root'
set :domain, 'www.51kehou.com'
set :deploy_to, '/var/www/core'
set :repository, 'https://Alex-Cheng:julia123@github.com/kfu321/core.git'
set :branch, 'master'
set :term_mode, :system

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['log', 'public/system']

# Optional settings:
#   set :user, 'foobar'    # Username in the server to SSH to.
#   set :port, '30000'     # SSH port number.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  # invoke :'rvm:use[ruby-1.9.3-p125@default]'
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/system"]
  queue! %[chmod -R 777 "#{deploy_to}/shared/system"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'

    to :launch do
      queue! 'mkdir -p tmp'
      #queue! 'touch tmp/restart.txt'
      queue! 'ln -s /home/seo/restart.txt tmp/restart.txt'
      queue! %[chmod 777 -R .]
      queue! 'ln -s /home/clearsky-data public/clearsky-data'
      queue! 'ln -s /usr/tomcat6/webapps/ROOT/data public/data'
      #queue! 'mv app/views/kehou/home.html.erb app/views/kehou/home.html.erb.bak'
      #queue! 'ln -s /home/seo/home.html.erb app/views/kehou/home.html.erb'
      #queue! 'killall rake' # kill existing PDF generator process.
      #queue! 'RAILS_ENV=production nohup rake kehou:gen_preview > log/gen_preview.log &' # relaunch PDF generator process.
      queue! 'sudo nginx -s reload'
    end
  end
end

# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers

