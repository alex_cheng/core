# -*- encoding : utf-8 -*-
# Load the rails application
require File.expand_path('../application', __FILE__)


Kehou::Application.configure do
  
  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.smtp_settings = {
    :address              => "smtp.51kehou.com",
    :port                 => 25,
    :domain               => 'www.51kehou.com',
    :user_name            => 'test@51kehou.com',
    :password             => '51kehou1234',
    :authentication       => 'login',
    
  }

  config.action_mailer.default_url_options = {
    host: 'www.51kehou.com'
  }
    
  # Initialize the rails application
  Kehou::Application.initialize!

end
