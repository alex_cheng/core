# -*- encoding : utf-8 -*-
# Be sure to restart your server when you modify this file.

# Override SessionStore with YAML-based marshal/unmarshal methods.
module ActiveRecord
  class SessionStore < ActionDispatch::Session::AbstractStore
    module ClassMethods # :nodoc:
      def marshal(data)
        YAML.dump(data) if data  # not encode it as Base64 because it is easy to debug.

        # use below statement to substitute above statement in prodcution.
        #ActiveSupport::Base64.encode64( YAML.dump(data) ) if data
      end

      def unmarshal(data)
        YAML.load(data) if data # not encode it as Base64 because it is easy to debug.

        # use below statement to substitute above statement in prodcution.
        #YAML.load( ActiveSupport::Base64.decode64(data) ) if data
      end

      def drop_table!
        connection_pool.clear_table_cache!(table_name)
        connection.drop_table table_name
      end

      def create_table!
        connection_pool.clear_table_cache!(table_name)
        connection.create_table(table_name) do |t|
          t.string session_id_column, :limit => 255
          t.text data_column_name
        end
        connection.add_index table_name, session_id_column, :unique => true
      end
    end
  end
end

#Kehou::Application.config.session_store :cookie_store, key: '_kehou_session'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
#Kehou::Application.config.session_store :active_record_store
