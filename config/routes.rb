# -*- encoding : utf-8 -*-
Kehou::Application.routes.draw do
  scope '/users/:user_id/' do
    resources :favorites, shallow: true
    resources :groups, shallow: true do
      resources :messages, shallow: true do
        get :reply, to: 'messages#reply'
      end
      resources :shared_files, shallow: true
    end
    resource :group_members, shallow: true
    resources :assignments, shallow: true do
      get :finished, to: 'assignments#finished_students'
      get :unfinished, to: 'assignments#unfinished_students'
    end

    scope '/assignments/:assignment_id/' do
      resources :assignment_attempts, shallow: true do
        post :post_answer
      end
    end

    get 'assignments/new/choose', :to => 'assignments#choose_practice'

    constraints lambda {|req| User.find(req['user_id']).usertype == :teacher} do
      root :to => "teacher#home", :as => :person_home
      get :home, :to => "teacher#home"
      get :exams, :to => "teacher#exams"
    end

    constraints lambda {|req| User.find(req['user_id']).usertype == :student} do
      root :to => "student#home", :as => :person_home
    end

    constraints lambda {|req| User.find(req['user_id']).usertype == :parent} do
      root :to => "parent#home", :as => :person_home
    end
  end

  resources :profiles

  get "clearsky/index_grade"
  get "clearsky/index_all"
  get "clearsky/landing"
  match '/clearsky/book/:id' => 'clearsky#show_book'
  match '/clearsky/books/:grade' => 'clearsky#show_book_by_grade'
  match "/clearsky/tongbu/:grade/" => 'clearsky#show_tongbu'
  match "/clearsky/tongbu/:grade/:unit" => 'clearsky#do_tongbu'
  match "/clearsky/exam/:id" => 'clearsky#show_practice'
  match "/clearsky/search" => 'clearsky#search'

  match "/explain/displayxml.do" => 'clearsky#displayxml'
  match "/examxml/:id" => 'clearsky#examxml'

  match "/404", :to => "errors#not_found"
  match "/422", :to => "errors#server_error"
  match "/500", :to => "errors#server_error"

  match '/home'  => "kehou#home"
  match '/home2'  => "kehou#home"
  match '/coursewares/:id' => 'kehou#courseware'
  match '/grades/:grade' => 'kehou#grade'
  match '/materials/:id' => 'kehou#material'
  match '/materials/:id/download' => 'kehou#material_download'
  match '/grades/:grade/:category' => 'kehou#grade_category'
  match '/search' => 'kehou#search'
  match '/about' => 'kehou#about'
  match '/comments' => 'kehou#comments'
  match '/contacts' => 'kehou#contacts'
  match '/signed_in_forum' => 'kehou#signed_in_forum'
  match '/logout_redirect' => 'kehou#logout_redirect'

  resources :group_member_invitations do
    collection do
      get :join
      get :regularize
      put :join, to: :active
    end
  end

  scope "/admin" do
    resources :coursewares, :user_type => :admin
  end

  # scope "/teacher" do
  #   resources :coursewares, :user_type => :teacher
  # end

  resources :courseware_materials

  match "/update_captcha" => 'simple_captcha#update_captcha'

  get "admin/home"

  get "admin/questions"

  get "admin/exams"

  get "admin/profile"

  devise_for :users, :path => "/", :controllers => { :sessions => "users/sessions", :registrations => "users/registrations" },
    :path_names => {
        :sign_in => 'login',
        :sign_out => 'logout',
        :registration => 'register',
        :sign_up => 'new' }

  devise_for :admins, :path => "/admin", :controllers => { :sessions => "admins/sessions" },
    :path_names => { :sign_in => 'login', :sign_out => 'logout' }

  # get "student/home"

  # get "student/words"

  # get "student/homeworks"

  # get "student/exams"

  # get "student/course_wares"

  # get "student/notifys"

  # get "student/messages"

  # get "student/profile"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'kehou#home'

  #match "/home" => 'welcome#home'
  #match "/login" => 'welcome#login'

  match "/admin" => 'admin#home'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
