# -*- encoding : utf-8 -*-
PYTHON_PATH = '/opt/openoffice4/program/python'
CONVERTER_PATH = File.expand_path('../DocumentConverter.py', __FILE__)

def gen_preview(beDaemon = false)
  begin
    no_preview_materials = CoursewareMaterial.where(preview_file_name: nil).where(['doc', 'docx', 'ppt', 'pptx'].map {|x| "upload_file_name LIKE '%.#{x}'"}.join(' or '))
    if no_preview_materials.size > 0
      puts "Prepare to process #{no_preview_materials.size} items."
      no_preview_materials.each do |material|
        begin
          generate_preview material
        rescue
          puts "[Exception] #{$!}".red
        end
      end
    end

    break if not beDaemon

    sleep 30
  end while true
end


def generate_preview material
  puts "Generating preview PDF for [#{material.upload_file_name}]..."
  if material.upload.path.to_s =~ /\.(doc|docx|ppt|pptx|pdf)$/
    tmpPdfPath = File.join(Dir.tmpdir, File.basename(material.upload.path, '.*') + '.pdf')

    # try to delete existing temporary file if it exists.
    begin
      File.delete tmpPdfPath
    rescue
      # do nothing
    end

    # relaunch openoffice if it quit.
    if `pgrep soffice`.size == 0
      puts 'Relaunch OpenOffice service.'
      spawn 'soffice "-accept=socket,host=localhost,port=8100;urp;StarOffice.ServiceManager" -norestore -nofirststartwizard -nologo -headless &'
    end

    out = nil
    cmd = "#{PYTHON_PATH} #{CONVERTER_PATH} '#{material.upload.path}' '#{tmpPdfPath}'"
    puts "Execute command: \n#{cmd}"
    out = `#{cmd}`
    if File.exists?(tmpPdfPath)
      material.preview = File.open(tmpPdfPath, 'rb')
      material.save
      File.delete tmpPdfPath
      puts "Preview PDF for [#{material.upload_file_name}] is generated.".green
    else
      puts "An error occured when invoke DocumentConverter.py. #{out}".red
    end
  else
    puts "The material [#{material.upload_file_name}] is not available for generating preview PDF.".yellow
  end
end
