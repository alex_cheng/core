# -*- encoding : utf-8 -*-

require 'FileUtils'

def create_fake_coursewares_folder(path)
  root = path or '.'
  open File.expand_path('../courseware_list.txt', __FILE__) do |input|
    while (line = input.gets)
      if line =~ /\.\w+$/
      	folder = line.gsub(/\.\w+$/, '')
      	FileUtils.mkdir_p File.expand_path(folder, root)
      	FileUtils.touch File.expand_path(line, root)
      end
    end
  end
end

if __FILE__ == $0
  create_fake_coursewares_folder ARGV[0]
end
