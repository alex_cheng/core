# -*- encoding : utf-8 -*-

require 'colorize' # install it by '# sudo gem install colorize'

KEY_WORDS = ['课件', '教案', '学案', '练习', '同步练习', '教学案', '试题', '期中', '期末', '月度', '季度', '选修', '必修', '中考', '高考', '高中', 
  '初中', '小学', '英语', '语文', '数学', '湖北', '四川', '安徽', '江苏', '北京', '上海', '天津', '重庆', '河北', '山西', '陕西', '辽宁', '吉林', '黑龙江',
  '浙江', '福建', '江西', '山东', '河南', '湖南', '广东', '海南', '贵州', '云南', '甘肃', '青海', '广西', '内蒙古', '新疆', '宁夏', '西藏', '听力', '阅读', '写作',
  '班主任', '声音', '图片', '动画', '教材', '习题', '习题集', '单元测试', '阅读', '精选', '精选文章', '训练', '精品资料', '论文', '心理健康', 
  '电子课本', '试卷', '模拟', '摸底']
SUBJECTS = ['英语', '语文', '数学', '不限']
Middle_School_Base = 60
High_School_Base = 90


def extract_courseware_materials(path)
  materials = []
  if Dir.exist?(path)
    Dir.foreach(path) do |f|
      materials << File.join(path, f)
    end
  else
    materials << path
  end
  return materails
end

def subject_parser(x)
  v = SUBJECTS.select { |s| x.include?(s) }.first
  v ? {:subject => v } : {:subject => '不限' } 
end

def topic_parser(x)
  x = x.gsub /\.+\w+$/, ''

  name = nil
  traits = [] # consists of subject, grade, keywords and so forth.
  x.split('/').reverse.each do |x|
    more_traits = (process(x, :topic_parser) or {}).collect {|k,v| (v if k == :keywords) or k }.flatten
    if (traits + more_traits).uniq.size > traits.size
      traits = (traits + more_traits).uniq
      (name.insert(0, x + ' - ') if name) or (name = x)
    end
  end
  { :topic => (name or x).strip }
end

GRADES_MAPPING = { '1' => 10, '2' => 20, '3' => 30, '4' => 40, '5' => 50, '6' => 60, '7' => 70, '8' => 80, '9' => 90, '一' => 10, 
									'二' => 20, '三' => 30, '四' => 40, '五' => 50, '六' => 60, '七' => 70, '八' => 80, '九' => 90, '上' => 1, 
									'下' => 5 }
def grade_parser(x)
  m = x.match /([\d一二三四五六七八九])年级([上下])/
  if m
    if GRADES_MAPPING[m[1]]
      return { :grade => GRADES_MAPPING[m[1]] + (GRADES_MAPPING[m[2]] or 0) }
    end
  end
  
  m = x.match /初([一二三])[^一二三]/
  if m && GRADES_MAPPING[m[1]]
    return { :grade => GRADES_MAPPING[m[1]] + Middle_School_Base } 
  end
  
  m = x.match /高([一二三])[^一二三]/
  if m && GRADES_MAPPING[m[1]]
    return { :grade => GRADES_MAPPING[m[1]] + High_School_Base }
  end
  
  m = x.match /初中.*([一二三])学期/
  if m
    return { :grade => GRADES_MAPPING[m[1]] + Middle_School_Base }
  end
  
  m = x.match /高中.*([一二三])学期/
  if m
    return { :grade => GRADES_MAPPING[m[1]] + High_School_Base }
  end

  m = x.match /高中.*必修([一二三四五六\d])/
  if m && GRADES_MAPPING[m[1]]
    mapping = {'一' => 101, '二' => 105, '三' => 111, '四' => 115, '五' => 121, '六' => 125, '1' => 101, '2' => 105, '3' => 111, '4' => 115, '5' => 121, '6' => 125}
    return { :grade => mapping[m[1]] }
  end  
end

def keywords_parser(x)
  keywords = KEY_WORDS.select { |w| x.include? w }

  x.match(/普通高等学校.*统一考试/) do |m|
		keywords.push '高考'
  end

  x.match(/(?<=[^\d-])\d+年/) do |m|
    keywords.push m[0]
  end

  { :keywords => keywords.uniq }
end

PUBLISHERS = ['人教版', '仁爱版', '北师大版', '外研版', '牛津译林版']
def publish_parser(x)
  { :publish => PUBLISHERS.select{|m| x.include? m}.first }
end

def unit_parser(x)
  m = x.match(/Unit\s*(\d+)/i)
  if m
    { :keywords => ["Unit #{m[1]}"] }
  end
end

PARSERS = [method(:subject_parser), method(:topic_parser), method(:grade_parser), method(:keywords_parser), method(:publish_parser), method(:unit_parser)]

def process(path, *except)
  traits = {}
  PARSERS.select{|x| not except.include? x.name}.each do |p|
    p.call(path).tap do |x|
      if x
        traits.merge!(x) do |key, oldval, newval|
          if oldval.instance_of?(Array) and newval.instance_of?(Array)
            (oldval + newval).uniq
          else
            newval
          end
        end
      end 
    end
  end
  traits
end

def import_coursewares(path)
  coursewares = {}
  count = 0
  Dir.glob(File.join(path, '**', '*')) do |e|
    next if e == '.' or e == '..'

    if File.file?(e)
      traits = process e

      if !traits[:topic]
        next
      end

      # new courseware
      descript = traits[:descript] || e.sub(path, '').gsub(/\.+\w+$/, '').gsub('/', '')
      grade = traits[:grade] || 0
      subject = traits[:subject] || '不限'
      topic = traits[:topic]
      publish = traits[:publish] || '无分类'
      keywords = (traits[:keywords] || []).join(', ')
      courseware = (coursewares[topic] || { :author => '', :author_type => 'admin', :descript => descript, 
                                          :grade => grade, :subject => subject, :topic => topic, 
                                          :keywords => keywords, :visibility => 'public', :publish => publish, 
                                          :materials => [] })
      courseware[:materials].push e
      coursewares[topic] = courseware
    else
      # the path refers to a folder, we don't need to handle it.
    end
  end

  #puts "materials count:"  + coursewares.collect{|k,v| v[:materials]}.flatten.size.to_s
  #puts "coursewares count" + coursewares.size.to_s

  if class_exists? :Courseware
    existing_coursewares = {}
    Courseware.find_each do |c|
      existing_coursewares[c.topic] = c
    end

    #coursewares.first(200).each do |k,v| # for test, only import first 200 coursewares
    coursewares.each do |k,v|
      existing = existing_coursewares[k]
      if existing
        puts "courseware #{k} already exists. Update it."
        updated_content = v.select {|k,v| k != :materials}
        existing.update_attributes updated_content
        existing.save
        next
      end

      tmp = v.select {|k,v| k != :materials}
      puts "create Courseware instance from #{tmp}"
      new_record = Courseware.create tmp
      if new_record.errors.empty?
        v[:materials].each do |m|
          begin
            new_mat = CoursewareMaterial.create(:upload => File.open(m, 'rb'))
            if new_mat.errors.empty?
              new_mat.courseware_id = new_record.id
              new_mat.save
              puts "\tmaterial: #{m}"
            else
              puts "\tmaterial errors:#{new_mat.errors.full_messages}"
            end
          rescue
            puts "create material failed: #{m}"
          end
        end
      else
        puts "errors: #{new_record.errors.full_messages.join('\n')}"
      end
    end
  else
    coursewares.each do |k,v|
      #puts "create new courseware #{k}"
      puts v[:descript]
      v[:materials].each do |m|
        puts "\tmaterial: #{m}"
      end
    end
  end
end

def class_exists?(class_name)
  klass = Module.const_get(class_name)
  return klass.is_a?(Class)
rescue NameError
  return false
end

def test_with_data
  open File.expand_path('../courseware_list.txt', __FILE__) do |input|
    while (line = input.gets)
      yield line.strip
    end
  end
end

def test_process
  courseware_paths = []
  test_with_data do |line|
      courseware = process line
      if courseware
        #puts "get a courseware: #{courseware} \n\tfrom #{line}"
        courseware_paths.push line
      else
        #puts "not a course: #{line}"
      end 
  end

  passed = true
  test_with_data do |line|
    if line =~ /\.\w+$/
      if courseware_paths.select{|x| line.start_with? x}.empty?
        puts "the file #{line} is not imported."
        passed = false
      end
    end
  end

  if passed
    puts "The test <test_process> is passed.".green
  else
    puts "The test <test_process> is failed.".red
  end
end

def test_grade_parser
  test_with_data do |line|
    grade = grade_parser line
    if grade
      puts "retrieve grade: #{grade} \n\tfrom #{line}"
    else
      puts "cannot find grade in #{line}"
    end 
  end
end

def test_keywords_parser
  test_with_data do |line|
    keywords = keywords_parser line
    if keywords[:keywords].empty?
      puts "cannot find keywords in #{line}"
    end
  end 
end

def test_publish_parser
  test_with_data do |line|
    publish = publish_parser line
    if !publish || !publish[:publish]
      puts "cannot find publish in #{line}"
    end
  end 
end

def test_unit_parser
  test_with_data do |line|
    unit = unit_parser line
    if unit
      puts "unit #{unit[:keywords]} from #{line}"
    else
      puts "cannot find unit in #{line}"
    end
  end 
end

def test_topic_parser
  test_with_data do |line|
    topic = topic_parser line
    if topic
      puts "topic #{topic[:topic].magenta} from #{line}"
    else
      puts "not available #{line}"
    end
  end
end

def test_subject_parser
  test_with_data do |line|
    subject = subject_parser line
    if subject && subject[:subject]
      puts "subject #{subject[:subject].magenta} from #{line}"
    else
      puts "not available #{line}"
    end
  end
end

def test_import_coursewares
  import_coursewares File.expand_path('../pool', __FILE__)
end

if __FILE__ == $0
  puts 'test core functions...'
  #test_process
  #test_grade_parser
  #test_keywords_parser
  #test_publish_parser
  #test_unit_parser
  #test_topic_parser
  #test_import_coursewares
  #test_subject_parser
end
