# -*- encoding : utf-8 -*-
require 'yomu'
require 'colorize'

def abstract_courseware_materials
	CoursewareMaterial.find_each do |m|
		if m.abstract == nil || m.abstract.size == 0
			if m.upload_file_name =~ /(\.doc|\.docx|\.xls|\.xlsx|\.ppt|\.pptx|.pdf|\.txt)$/
				begin
					yomu = Yomu.new m.upload.path
					part = yomu.text.first 2000
					m.abstract = '...' + (part.split("\n")[5..-1] || []).select{|e| e.gsub(/[\s*]/, '').size > 0}.first(25).join("\n") + '...'
					m.save
					puts "Updated material #{m.upload.path}"
				rescue Exception => e
					puts "Failed to update material #{m.upload.path}, #{e.message}"
				end
			end
		end
	end
end

TestData_Path = File.expand_path('../testdata', __FILE__)

Test_Files = {:doc => '1.doc', :docx => '1.docx', :xls => '1.xls', :xlsx => '1.xlsx', :ppt => '1.ppt', :pptx => '1.pptx', :pdf => '1.pdf'}

def test_extract_doc; test_extract(:doc); end

def test_extract_docx; test_extract(:docx); end

def test_extract_xls; test_extract(:xls); end

def test_extract_xlsx; test_extract(:xlsx); end

def test_extract_ppt; test_extract(:ppt); end

def test_extract_pptx; test_extract(:pptx); end

def test_extract_pdf; test_extract(:pdf); end

def test_extract(doctype)
	file_path = File.join(TestData_Path, Test_Files[doctype])
	yomu = Yomu.new file_path
	puts "cannot extract file #{file_path}".red if !yomu
	text = yomu.text
	puts "cannot extract file #{file_path}".red if !text
	puts text.green
end

if __FILE__ == $0
	puts 'test core functions...'
	test_extract_doc
	test_extract_docx
	test_extract_xls
	test_extract_xlsx
	test_extract_ppt
	test_extract_pptx
	test_extract_pdf
end
