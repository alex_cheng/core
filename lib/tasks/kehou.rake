# encoding: UTF-8
require File.expand_path('../courseware_importer/courseware_importer.rb', __FILE__)
require File.expand_path('../courseware_extractor/courseware_extractor.rb', __FILE__)
require File.expand_path('../show_none_51kehou_users', __FILE__)
require File.expand_path('../fix_forum_users', __FILE__)
require File.expand_path('../gen_preview/gen_preview', __FILE__)

namespace :kehou do
  desc "Import coursewares from file system."
  task :import => :environment do
    src_path = (ARGV[1] or File.expand_path('../courseware_importer/pool', __FILE__))
    puts "The source path is #{src_path}"
    import_coursewares src_path
  end

  desc "Extract content from courseware material as abstract of material."
  task :abstract => :environment do
  	abstract_courseware_materials
  end

  desc "Show all forum users who are not 51kehou registered users."
  task :none_51kehou_users => :environment do
    show_none_51kehou_users
  end

  desc "Fix forum users"
  task :fix_forum_users => :environment do
    fix_forum_users
  end

  desc "Generate preview PDF for courseware materials."
  task :gen_preview => :environment do |t, args|
    beDaemon = true
    puts "The task is running in daemon mode." if beDaemon
    gen_preview beDaemon
  end
end