# -*- encoding : utf-8 -*-
def fix_forum_users
  emails = User.all.map {|x| x.email}
  uidToEmail = Hash[*User.execute_sql_on_forum("SELECT uid, email FROM `ultrax`.`pre_common_member` WHERE email IN (?);", emails).entries.flatten]
  
  missingUid = uidToEmail.keys - User.execute_sql_on_forum("SELECT uid FROM `ultrax`.`pre_common_member_count` WHERE uid IN (?)", uidToEmail.keys).entries.flatten
  if missingUid.size > 0
    puts 'Found some forum users are not integrated. The corresponding rows in common_member_count are missing.'
    puts "These users' ID are listed as following:"
    puts missingUid
    puts '-- end --'
    
    isAll = false
    fixed = []
    missingUid.each do |uid|
      if !isAll
        puts "Would you like to fix the data of user '#{uidToEmail[uid]}'?(Yes/No/All)"
        yn = STDIN.gets
        if yn =~ /[nN]/
          puts "skipped."
          next
        elsif yn =~ /[aA]/
          isAll = true
        end
        User.execute_sql_on_forum("INSERT INTO `ultrax`.`pre_common_member_count`(uid) VALUES (?);", uid)
        fixed << uidToEmail[uid]
      end
    end
    
    if fixed.size > 0
      puts "The following users are fixed:"
      puts fixed
    end
  else
    puts 'Wonderful! All users data are integrated.'
  end
end
