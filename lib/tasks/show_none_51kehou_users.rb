# -*- encoding : utf-8 -*-
def show_none_51kehou_users
  puts '-- Following users are registered as forum user, but not registered as 51kehou user -- '
  puts User.all_forum_users - User.all.map {|user| user.email}
  puts '-- end --'
end
