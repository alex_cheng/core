# -*- encoding : utf-8 -*-
假如(/^我已经在登录页面上$/) do
  visit new_user_session_path
end

当(/^我在email地址文本框中填写注册的email地址$/) do
  within("#loginform") do
    fill_in('email_input', :with => 'teacher@51kehou.com')
  end
end

当(/^在密码框中填写正确的密码$/) do
  within("#loginform") do
    fill_in('pwd_input', :with => '12345678')
  end
end

当(/^点击了登录按钮$/) do
  within("#loginform") do
    click_button("用户登录")
  end
end

那么(/^我应该可以成功登录$/) do

end

