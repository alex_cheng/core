# -*- encoding : utf-8 -*-
When /^我点击"([^"]*)"$/ do |button|
  click_button(button)
end

When /^我点击([^"]*)按钮$/ do |button|
  click_button(button)
end

When /^我点击链接"([^"]*)"$/ do |link|
  click_link(link)
end

When /^我点击链接([^"]*)$/ do |link|
  click_link(link)
end

Then /^我应该在(.+)上$/ do |page_name|
  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    current_path.should == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end
