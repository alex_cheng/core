<?php
session_start();
include'conns.php';

$id = $_GET['id']?$_GET['id']:0;
$page = $_GET['page']?$_GET['page']:1;
$category = $_GET['category']?$_GET['category']:"";
$table = $table_per."coursewares";
$from = "ad_courseware.php?&category=".$category."&page=".$page;

if($_GET['m']=="del" && $id>0)
{
	$a = removeData($table, $id);
	$editGoTo = $from;
	header(sprintf("Location: %s", $editGoTo));
}


if($id>0)
{
	$data = getInfo($table, "id", $id);
	if($data['id']>0)
	{
		$type = "edit";
	}
}
else
{
	$type = "add";
}
/**/
?>

<!DOCTYPE html>
<html>
<head>
<link href="css/global.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>课件管理</title>
</head>
<body class="admin">
<div id="container">
	<?php include"ad_header.php"; ?>
	<div class="mainbody">
		<?php include"ad_nav.php"; ?>
		<div class="mainbox">
			<div class="uploadFrameDiv" style="display:none;">
				<div id="uploadFrameDiv">
					<h2><div style="width:100px; float:left;">上传图片</div><a href="###" class="closeIcon"></a></h2>
					<iframe id="uploadFrame" src="" frameborder="0"></iframe>
				</div>
			</div>
			<?php
			if ($type == "edit")
			{
				echo "<h2>编辑课件</h2>";
			}
			else
			{
				echo "<h2>添加课件</h2>";
			}
			?>
			<form action="ad_edit_funcs.php" method="post">
				<input type="hidden" name="type" value="coursewares" />
				<input type="hidden" name="fromPage" value="<?php echo $from; ?>" />
				<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
				<table cellpadding="0" cellspacing="0" border="0" class="normalTable">
					<tr>
						<th width="80">标题</th>
						<td><input type='text' name='topic' value="<?php echo $data['topic']; ?>" /></td>
					</tr>
					<tr>
						<th>科目</th>
						<td><input type='text' name='subject' value="<?php echo $data['subject']; ?>" /></td>
					</tr>
					<tr>
						<th>公开</th>
						<td>
							<select name='visibility'>
								<?php
									foreach($GLOBALS['courseware_visibility'] as $k=>$v)
									{
										echo '<option value="'.$v.'" ';
										if($v == $data['visibility'])
										{
											echo 'selected';
										}
										echo '>'.$k.'</option>';
									}
								?>
							</select>
					</tr>
					<tr>
						<th>年级</th>
						<td>
							<select name='grade'>
								<?php
									foreach($GLOBALS['courseware_grade'] as $k=>$v)
									{
										echo '<option value="'.$v.'" ';
										if($v == $data['grade'])
										{
											echo 'selected';
										}
										echo '>'.$k.'</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th>教材:</th>
						<td>
							<select name='publish'>
							<?php
								foreach($GLOBALS['courseware_publish'] as $v)
								{
									echo '<option value="'.$v.'" ';
									if($v == $data['publish'])
									{
										echo 'selected';
									}
									echo '>'.$v.'</option>';
								}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<th width="80">关键字/标签</th>
						<td><input type='text' name='keywords' value="<?php echo $data['keywords']; ?>" /></td>
					</tr>
					<tr>
						<th width="80">描述</th>
						<td><?php echo $data['descript']; ?></td>
					</tr>
					<tr>
						<th>
							<p>上传资料:</p>
							资料列表：
						</th>
						<td rowspan="2">
							<iframe src="<%= courseware_materials_path %>" id="uploadIframe" style="width:98%; height: auto; border:none;" class="auto_height_iframe"></iframe>
						</td>
					</tr>
				</table>
				<p></p>
				<p>
					<input type="submit" value="确认提交" />
					<input type="reset" value="重新设置" />
					<input type="button" onclick="window.location.href='ad_courseware.php?category=<? echo $category; ?>&page=<? echo $page; ?>';" value="返回列表" />
				</p>
			</form>
			<div class="space"></div>
			<p>&nbsp;</p>
		</div>
	</div>
</div>
<script language="javascript">
var types = ["title_img_zh", "title_img_en"];

$(document).ready(function(){
	//var context_zh = $('#context_zh').xheditor();
	//var context_en = $('#context_en').xheditor();

	//var imgs = $("#imgs").val();
	//var type = "mimg";
	//if(imgs!="")
	//{
	//	changeBgImg(type, imgs);
	//}
	//var cur = $("#thumb").val()?$("#thumb").val():0;
	//set_main($(".imgs .img img:eq("+cur+")"));
	set_thumb();
	//setCheckboxs();
}); 

function choose_thumb(obj, type_)
{
	$(".uploadFrameDiv").css("left",$(obj).position().left-0);
	$(".uploadFrameDiv").css("top",$(obj).position().top+30);
	$(".uploadFrameDiv").show();
	$("#uploadFrame").attr("src","ad_pictures_iframe.php?f="+type_);
}
function set_thumb()
{
	for (var a=0; a<types.length; a++)
	{
		var v_ = $("input[name='"+types[a]+"']").val();
		if (v_!="")
		{
			$("#"+types[a]+"_img").html("<div class='img'><div class='closeIcon' onclick='remove_img(\""+types[a]+"\");'></div><img src='" + v_ + "' /></div>\n");
		}
	}
}
function reset_thumb()
{
	for (var a=0; a<types.length; a++)
	{
		var v_ = $("#"+types[a]+"_img").find("img").attr("src")?$("#"+types[a]+"_img").find("img").attr("src"):"";
		if (v_!="")
		{
			$("input[name='"+types[a]+"']").val(v_);
			//$("#"+types[a]+"_img").html("<div class='img'><div class='closeIcon' onclick='remove_img(\""+types[a]+"\");'></div><img src='" + v_ + "' /></div>\n");
		}
	}
	
	//var main_img = $("#main_img").find("img").attr("src")?$("#main_img").find("img").attr("src"):"";
	//var list_img = $("#list_img").find("img").attr("src")?$("#list_img").find("img").attr("src"):"";
	//$("input[name='thumb']").val(main_img+"||"+list_img);	
}
function remove_img(type)
{
	$("#"+type+"_img").html("");
	$("input[name='#"+type+"']").val("");
	//reset_thumb();
}
function changeBgImg(type, imgs)
{
	if (type=="mimg")
	{
		var t = imgs.split("||");
		var imgList = new Array();
		var html = ""
		var vals = ""
		for(var a=0; a<t.length; a++)
		{
			if(t[a]!="")
			{
				vals += t[a] + "||";
				html += "<div class='img'><div class='closeIcon' onclick='removeImg(this);'></div><img src='images/upload/" + t[a] + "' onclick='set_main(this);' /></div>\n";
			}
		}
		$("#imgs").val(vals);
		$(".imgs").html(html);
	}
	else if(type=="mimgu")
	{
		var pics = $("#imgs").val() + "||" + imgs + "||";
		var type = "mimg";
		$("#imgs").val(pics);
		changeBgImg(type, pics);
	}
	else if(types.join("").indexOf(type) >= 0)
	{
		$("#"+type+"_img").html("<div class='img'><div class='closeIcon' onclick='remove_img(\""+type+"\");'></div><img src='images/upload/" + imgs + "' /></div>\n");
		//$("#main_img").html("<div class='img'><div class='closeIcon' onclick='remove_img(0);'></div><img src='images/upload/" + imgs + "' /></div>\n");
		reset_thumb();
	}
	else 
	{
		$("#uploadBtn").hide();
		$("#removeBtn").show();
		//var pics = $("#imgs").val();
		//var type = "mimg";
		//pics = pics.replace(imgs+"||");
		//changeBgImg(type, pics);
	}
	
	var cur = $("#thumb").val()?$("#thumb").val():0;
	if (cur<0) { cur = 0; }
	if (cur>$(".imgs .img").length-1) { cur = $(".imgs .img").length-1; }
	set_main($(".imgs .img img:eq("+cur+")"));
}
function set_main(obj)
{
	return;
	if($(obj).attr("src")!=""||$(obj).attr("src")!="undefined")
	{
		$(".imgs .img").removeClass("imgon");
		$(obj).parent().addClass("imgon");
	}
	$("#thumb").val($(".imgs .img img").index($(obj)));
}
$(".uploadFrameDiv, .InforDiv").draggable({
	handle: "h2"
});
$("#uploadBtn").bind("click",function(){
	$(".uploadFrameDiv").css("left",$(this).position().left-0);
	$(".uploadFrameDiv").css("top",$(this).position().top+30);
	$(".uploadFrameDiv").show();
	$("#uploadFrame").attr("src","ad_pictures_iframe.php?f=mimgu");
})
function removeImg(obj)
{
	var url = top.location.href.split("ad_")[0];
	var img = $(obj).next().attr("src").replace(url, "").replace("images/upload/", "");
	if (img!="")
	{
		var pics = $("#imgs").val();
		pics = pics.replace(img+"||", "");
		$("#imgs").val(pics);
		var type = "mimg";
		changeBgImg(type, pics);
	}
}
$("#uploadFrameDiv h2 a").bind("click",function(){
	$(".uploadFrameDiv").hide()
})
function setCheckboxs()
{
	var categorys = $("input[name='category']").val();
	var material = $("input[name='material']").val();
	var colour = $("input[name='colour']").val();
	setCheckbox("category", categorys);
	setCheckbox("material", material);
	setCheckbox("colour", colour);
}
function setCheckbox(name, vals)
{
	var list = $("input[name='"+name+"Checkbox']");
	var valList = vals.split("||");
	for(var a=0; a<(valList.length-1); a++)
	{
		for(var b=0; b<list.length; b++)
		{
			if(valList[a] == $(list[b]).val())
			{
				$(list[b]).attr("checked", "checked");
			}
		}
	}
}
$("input[name='categoryCheckbox'], input[name='materialCheckbox'], input[name='colourCheckbox']").bind("click",function(){
	var name = $(this).attr("name");
	var list = $("input[name='"+name+"']");
	var html = "";
	for(var a=0; a<list.length; a++)
	{
		if($(list[a]).attr("checked")==true)
		{
			html += $(list[a]).val() + "||"; 
		}
	}
	$("input[name='"+name.replace('Checkbox','')+"']").val(html);
})

$(".imgs").sortable({
	stop: function(event, ui) {
		setContent()
	}
});

function setContent()
{
	var url = top.location.href.split("ad_")[0];
	var list = $(".imgs .img");
	var val = ""
	for(var a=0; a<list.length; a++)
	{
		val += $(list[a]).find("img").attr("src").replace(url, "").replace("images/upload/", "") + "||";
	}
	$("#imgs").val(val);
}
</script>
</script>
</body>
</html>
