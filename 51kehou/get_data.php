<?
function getList($table, $in_sql, $first, $max)
{
	global $db_file; 
	$handle = new SQLite3($db_file);

	$sql = "SELECT * FROM %s %s ";
	$query_data_all = sprintf($sql, $table, $in_sql);

	if ($first!= "" && $first >= 0) { }
	else { $first = 0; }
	if ($max!= "" && $max >= 0) { }
	else { $max = 10; }
	$sql .= " limit %s, %s;";
	$query_data = sprintf($sql, $table, $in_sql, $first, $max);

	$query_data_all = str_replace('*', 'id', $query_data_all);

	$result_all = $handle->query($query_data_all); 
  $result = $handle->query($query_data); 

  $totalRows_data_all = 0;
	while($res = $result_all->fetchArray(SQLITE3_ASSOC)){
		if(!isset($res['id'])) continue;
		$totalRows_data_all++;
	}

	$dataList[0] = $totalRows_data_all;
	$i = 1;
	while($res = $result->fetchArray(SQLITE3_ASSOC)){
		if(!isset($res['id'])) continue;
		$dataList[$i] = $res;
		$i++;
	}
	return $dataList;
}

function getData($table, $sql)
{
	global $db_file; 
	$handle = new SQLite3($db_file);

	$sql = "SELECT * FROM ".$table." ".$sql ;
	$query_data = sprintf($sql);
	//echo $sql;

  $result = $handle->query($query_data); 
	
	$i = 1;
	while($res = $result->fetchArray(SQLITE3_ASSOC)){
		if(!isset($res['id'])) continue;
		$dataList[$i] = $res;
		$i++;
	}
	$dataList[0] =  count($dataList)-1 ;
	return $dataList;
}

function getInfo($table, $colum, $value)
{
	global $db_file;
	$handle = new SQLite3($db_file);

	$query_data = sprintf("SELECT * FROM %s where `%s` = %s ;", $table, $colum, GetSQLValueString($value, "text"));
	//echo $query_data;
	$result = $handle->query($query_data); 
	$res = $result->fetchArray(SQLITE3_ASSOC);
	return $res;
}

function upData($table, $keys, $vals)
{
	global $db_file;
	$handle = new SQLite3($db_file, SQLITE3_OPEN_READWRITE);

	for($a=0; $a<count($keys); $a++)
	{
		if($keys[$a]=="id" && $vals[$a]>0)
		{
			$id = $vals[$a] ;
			$data = getInfo($table, "id", $vals[$a]) ;
		}
		if($keys[$a]=="date" && $vals[$a]=="")
		{
			$vals[$a] = date("Y-m-s H:i:s");
		}
	}
	if($data['id']>0)
	{
		$sql_query = "" ;
		for($a=0; $a<count($keys); $a++)
		{
			if($keys[$a]!="id")
			{
				$sql_query .= "`".$keys[$a]."`=".GetSQLValueString($vals[$a], "text").", ";
			}
		}
		$sql_query = substr($sql_query, 0,(strlen($kys)-2)) ;
		$sql_query = $sql_query." WHERE id=".$id." ;" ;
		$sql = sprintf("UPDATE %s SET %s", $table, $sql_query) ;
		//echo $sql;
	}
	else
	{
		$sql_query = "" ;
		for($a=0; $a<count($keys); $a++)
		{
			if($keys[$a]!="id")
			{
				$kys .= "`".$keys[$a]."`, " ;
				$vls .= GetSQLValueString($vals[$a], "text").", " ;
			}
		}
		$kys = substr($kys, 0, (strlen($kys)-2)) ;
		$vls = substr($vls, 0, (strlen($vls)-2)) ;
		$sql_query .= " ( ".$kys.") values ( ".$vls.") ;" ;
		$sql = sprintf("INSERT INTO %s %s", $table, $sql_query) ;
	}
	//echo $sql."<br>" ;
	if($handle->query($sql))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

function removeData($table, $id)
{
	global $mysql; 
	$conn = mysql_pconnect($mysql[0], $mysql[2], $mysql[3]) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($mysql[1], $conn);
	mysql_query("set names 'utf8'");
	$removeSQL = sprintf("delete from `%s` where id=%s", $table, $id);
	$result = mysql_query($removeSQL, $conn) or die(mysql_error());
	return $result;
}

function searchList($table, $params, $orderBy, $order, $first, $max)
{
	global $mysql ; 
	$conn = mysql_pconnect($mysql[0], $mysql[2], $mysql[3]) or trigger_error(mysql_error(),E_USER_ERROR) ; 
	mysql_select_db($mysql[1], $conn) ;
	mysql_query("set names 'utf8'") ;
	
	$sql_all = sprintf("select * from %s where 1 ", $table);
	if($params!="")
	{
		foreach($params as $k => $v)
		{
			$tmp = "";
			for($a=0; $a<count($v); $a++)
			{
				$val = "%".$v[$a]."%";
				$tmp .= sprintf("`%s` like %s or ", $k, GetSQLValueString($val, "text"));
			}
			$sql_all .= " and ( ".substr($tmp, 0, -3)." ) ";
		}
	}
	$sql = $sql_all;
	if($orderBy!="")
	{
		if ($order == "desc")
		{
			$sql .= " order by ".$orderBy." ".$order ;
		}
		else
		{
			$sql .= " order by ".$orderBy." asc" ;
		}
	}
	if ($first > -1 && $max >0 )
	{
		$sql .= " limit ".$first.", ".$max ;
	}
	//echo $sql_all."<br>";
	//echo $sql."<br>";
	$data_all = mysql_query($sql_all, $conn) or die(mysql_error());
	$totalRows_data_all = mysql_num_rows($data_all);
	$data = mysql_query($sql, $conn) or die(mysql_error());
	$row_data = mysql_fetch_assoc($data);
	$totalRows_data = mysql_num_rows($data);

	if ( $totalRows_data > 0 ) {
		$i = 1 ;
		do {
			$dataList[$i] = $row_data;
			$i++;
		} while ($row_data = mysql_fetch_assoc($data));
	}
	$dataList[0] =  $totalRows_data_all ;
	return $dataList;
}

function getAllPics($dir) {
	$handle = opendir($dir);
	$i = 0 ;
	$names = array();
	while(false!==($filename=readdir($handle))) {
		if ($filename!="Thumbs.db"&&$filename!="."&&$filename!="..") {
			$names[$i] = $filename;
			//$sizes[$i] = (filesize($dir."/".$filename)/1000);
			//$type[$i] = (filetype($dir."/".$filename));
			$i++;
		}
	}
	return $names;
}
function listAllPic($dir,$page,$maxNo) {
	$handle = opendir($dir);
	$i = 0 ;
	while(false!==($filename=readdir($handle))) {
		if ($filename!="." && $filename!="..") {
			$names[$i] = $filename;
			$sizes[$i] = (filesize($dir."/".$filename)/1000);
			//$type[$i] = (filetype($dir."/".$filename));
			$i++;
		}
	}
	//array_multisort($names,SORT_DESC,$filename);     

	$pagenum = 1;
	if (isset($page)) {
	  $pagenum = $page;
	}
	$startnum = ($pagenum - 1) * $maxNo ;
	$endnum = $startnum + $maxNo;

	for($m = $startnum; $m<$endnum; $m++)
	{
		if($names[$m]!="")
		{
			echo "<tr style='background:#fff'>";
			echo "<td width='100' style='height:40px;'><a href='".$dir.$names[$m]."' target='_blank' class='folderNum' title='$names[$m]'><img src='".$dir.$names[$m]."'  height='30' border=0></a></td>";
			echo "<td>".$names[$m]."</td>";
			echo "<td><input style='width:320px;' value='".$dir.$names[$m]."'></td>";
			echo "<td><a href='?page=".$page."&method=del&name=".$names[$m]."'>Delete</a></td>";
			echo "</tr>";
		}
	}
	echo "</table>";
	echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='dataTable'>";
	echo "<tr><td colspan='3'>";
	pageNav($startnum, $maxNo, $i);
	echo "</td></tr>";
	echo "</table>";
}

function deleteImg($folder,$name)
{
	if(file_exists($folder.$name))
	{
		if(unlink($folder.$name))
		{
			$delmsg = "Delete success.";
		}
		else
		{
			$delmsg = "Delete Failed.";
		}
	}
}

function getTime($time, $format="Y-m-d H:i:s")
{
	$time2['y'] = substr($time,0,4);
	$time2['m'] = substr($time,5,2);
	$time2['d'] = substr($time,8,2);
	
	$time2['h'] = substr($time,11,2);
	$time2['i'] = substr($time,14,2);
	$time2['s'] = substr($time,17,2);
	
	if($format!="")
	{
		$res = $format;
		
		$res = str_replace("Y", $time2['y'], $format);
		$res = str_replace("m", $time2['m'], $res);
		$res = str_replace("d", $time2['d'], $res);
		$res = str_replace("h", $time2['H'], $res);
		$res = str_replace("i", $time2['i'], $res);
		$res = str_replace("s", $time2['s'], $res);
		return $res;
	}
	else
	{
		return $time2;
	}
}

function format_time($time, $format)
{
	return substr($time,0,4).".".substr($time,5,2).".".substr($time,8,2);
}
function u2utf82gb($c){
	return $c;
    $str="";
	if($c>1000)
	{
		if ($c < 0x80) {
			 $str.=$c;
		} else if ($c < 0x800) {
			 $str.=chr(0xC0 | $c>>6);
			 $str.=chr(0x80 | $c & 0x3F);
		} else if ($c < 0x10000) {
			 $str.=chr(0xE0 | $c>>12);
			 $str.=chr(0x80 | $c>>6 & 0x3F);
			 $str.=chr(0x80 | $c & 0x3F);
		} else if ($c < 0x200000) {
			 $str.=chr(0xF0 | $c>>18);
			 $str.=chr(0x80 | $c>>12 & 0x3F);
			 $str.=chr(0x80 | $c>>6 & 0x3F);
			 $str.=chr(0x80 | $c & 0x3F);
		}
		//return iconv('utf8', 'GB2312', $str);
		return mb_convert_encoding($str, "gbk", "utf8");
	}
	else
	{
		return chr($c);
	}
}
function strChange($str)
{
	$strTmp = explode("\\u",$str);
	for($a=1;$a<count($strTmp);$a++)
	{
		if($strTmp[$a]!="")
		{
			$strChange[$a] = u2utf82gb($strTmp[$a]);
		}
		$tmp .= $strChange[$a];
	}
	return $tmp;
}


function format_text($type, $lan, $val)
{
	$products = array("en"=>array(1=>"Limited Production", 2=>"Creative Life", 3=>"Kitchen Dining", 4=>"Home Appliance", 5=>"Paper & Stationery"),
				         "zh"=>array(1=>"限定生产", 2=>"创意生活", 3=>"餐厅厨房", 4=>"家具用品", 5=>"纸品文具"));
	if($type=="products")
	{
		$str = $products[$lan][$val];
	}
	return $str;
}


?>