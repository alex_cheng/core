<?
session_start();
include'conns.php';

header("Content-type: text/html; charset=utf-8");

$page = $_GET['page']?$_GET['page']:1;
$category = $_GET['category']?$_GET['category']:"";
$maxNo = 30;
$first = ($page-1)*$maxNo;
$table = $table_per."coursewares";

if($category != "")
{
	$sql = " where 1 and category='$category' order by id asc, updated_at desc ";
}
else
{
	$sql = " where 1 order by id asc, updated_at desc ";
}
$data = getList($table,$sql, $first, $maxNo);
/**/
?>

<!DOCTYPE>
<html>
<head>
<link href="css/global.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>课件管理</title>
</head>
<body class="admin">
<div id="container">
	<? include"ad_header.php"; ?>
	<div class="mainbody">
		<? include"ad_nav.php"; ?>
		<div class="mainbox">
		<p>
			<input type="button" onclick="javascript:window.location.href='ad_courseware_edit.php?category=<? echo $category; ?>&page=<? echo $page; ?>';" value=" + 添加 " />
			<!--<a href='ad_courseware.php?category=news&page=<? echo $page; ?>' style='margin-right:15px; padding:2px 8px;'>News</a>
			<a href='ad_courseware.php?category=story&page=<? echo $page; ?>' style='margin-right:15px; padding:2px 8px;'>Design Story</a>-->
		</p>

<div class="headerform">
	<label>
		标题:
		<input type="text" class="input_s" />
	</label>
	<label>
		<select>
			<option value="">所有资源</option>
			<option value="">公共资源</option>
			<option value="">个人资源</option>
		</select>
	</label>
	<label>
		<select>
			<option value="">所有科目</option>
			<option value="">英语</option>
			<option value="">语文</option>
			<option value="">数学</option>
		</select>
	</label>
	<label>
		<select>
			<option value="">所有年级</option>
			<option value="3">三年级</option>
			<option value="4">四年级</option>
			<option value="5">五年级</option>
			<option value="6">六年级</option>
			<option value="7">初一</option>
			<option value="8">初二</option>
			<option value="9">初三</option>
			<option value="10">高一</option>
			<option value="11">高二</option>
			<option value="12">高三</option>
		</select>
	</label>
	<label>
	From: <input type="text" class="input_s" />
	To: <input type="text" class="input_s" />
	</label>
	<input type="button" class="btn4" value="筛选" />
</div>

		<?
			if ($data[0] >0 )
			{
				//echo "<div class='pageNav'>";
				//pageNav($first, $maxNo, $data[0]);
				//echo "</div>";
				echo "<div class='space'></div>";
				echo "<table cellpadding=0 cellspacing=0 class='dataTable dataTable2'>";
				echo '<tr>';
				echo '<th width="50">来源</th>';
				echo '<th width="50">公开</th>';
				echo '<th width="50">科目</th>';
				echo '<th width="80">年级</th>';
				echo '<th>标题</th>';
				echo '<th width="50">附件</th>';
				echo '<th width="80">编辑时间</th>';
				echo '<th width="120">操作</th>';
				echo '</tr>';
				for($a=1;$a<=$data[0]; $a++)
				{
					if($data[$a]['id']>0)
					{
						echo '<tr>';
						echo '<td>'.$data[$a]['author_type'].'</td>';
						echo '<td>'.$data[$a]['visibility'].'</td>';
						echo '<td>'.$data[$a]['subject'].'</td>';
						echo '<td>'.$data[$a]['grade'].'</td>';
						echo '<td>['.$data[$a]['publish'].'] <a href="ad_courseware_edit.php?category='.$data[$a]['category'].'&page=$page&id='.$data[$a]['id'].'">'.$data[$a]['topic'].'</a></td>';
						echo '<td>&nbsp;</td>';
						echo '<td>'.$data[$a]['updated_at'].'</td>';
						echo "<td><a class='delete' link='ad_courseware_edit.php?m=del&category=".$data[$a]['category']."&page=$page&id=".$data[$a]['id']."'>删除</a></td>";
						echo '</tr>';
					}
				}
				echo "</table>";
				echo "<p>&nbsp;</p>";
				echo "<div class='space'></div>";
				echo "<div class='pageNav'>";
				pageNav($first, $maxNo, $data[0]);
				echo "</div>";
				echo "<p>&nbsp;</p>";
			}
			else
			{
				echo "<h2>no dates.</h2>";
			}
		?>
		</div>
	</div>
	<script language="javascript">
	var ctype = "coursewares";
	$(".delete").bind("click", function(){
		var obj = $(this);
		if(confirm("确认删除？"))
		{
			window.location.href = $(obj).attr("link");
		}
	})
	</script>
</div>
</body>
</html>
