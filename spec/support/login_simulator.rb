# -*- encoding : utf-8 -*-
module LoginSimulator
  # default teacher is the user instance with id equal to 1
  def login_with_default_teacher
    user = User.find(1)
    login(user.email, '12345678')
    return user
  end

  # default student is the user instance with id equal to 3
  def login_with_default_student
    user = User.find(3)
    login(user.email, '12345678')
    return user
  end

  def login(email, password)
    visit new_user_session_path
    within("#loginform") do
      fill_in 'email_input', with: email
      fill_in 'pwd_input', with: password
      click_button '用户登录'
    end
    page.should have_content('欢迎')
  end
end
