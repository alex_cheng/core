# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :favorite do
    book_id 770605
    comment 'It is a test!'
    user_id 1
  end

  factory :assignment do
    creator_id 1
    begin_time DateTime.now
    deadline DateTime.now + 3
    due_time DateTime.now + 3
    practice_id '33653'
    assigned_group_id 1
    status Assignment::Available
    sequence :comment do |n|
      "测试用作业#{n}"
    end
  end

end
