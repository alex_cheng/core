# -*- encoding : utf-8 -*-
require "spec_helper"

describe AssignmentsController do
  describe "routing" do

    it "routes to #finished" do
      get("/assignments/1/finished").should route_to("assignments#finished_students", assignment_id: "1")
    end

    it "routes to #unfinished" do
      get("/assignments/1/unfinished").should route_to("assignments#unfinished_students", assignment_id: "1")
    end

  end
end
