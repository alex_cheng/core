# -*- encoding : utf-8 -*-
require "spec_helper"

describe AssignmentAttemptsController do
  describe "routing" do

    it "routes to #index" do
      get("/users/1/assignments/1/assignment_attempts").should route_to("assignment_attempts#index", user_id: "1", assignment_id: "1")
    end

    it "routes to #new" do
      get("/users/1/assignments/1/assignment_attempts/new").should route_to("assignment_attempts#new", user_id: "1", assignment_id: "1")
    end

    it "routes to #show" do
      get("/assignment_attempts/1").should route_to("assignment_attempts#show", :id => "1")
    end

    it "routes to #edit" do
      get("/assignment_attempts/1/edit").should route_to("assignment_attempts#edit", :id => "1")
    end

    it "routes to #create" do
      post("/users/1/assignments/1/assignment_attempts").should route_to("assignment_attempts#create", user_id: "1", assignment_id: "1")
    end

    it "routes to #update" do
      put("/assignment_attempts/1").should route_to("assignment_attempts#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/assignment_attempts/1").should route_to("assignment_attempts#destroy", :id => "1")
    end

  end
end
