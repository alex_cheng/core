# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "TeacherListAssignments" do
  before(:all) do
    @assignment = FactoryGirl.create(:assignment)
    @student = User.find(3)
  end

  before(:each) do
    login_with_default_teacher
  end

  it "page is available!" do
    visit "/users/1/assignments"
    page.status_code.should be(200)
  end

  it "shows assignments" do
    visit "/users/1/assignments"
    find(:css, '.assignment_state .finished').native.text.should eq('0')
    find(:css, '.assignment_state .not_finished').native.text.should eq('1')

    visit "/assignments/#{@assignment.id}/finished"
    page.body.should include("没有学生做完作业")

    visit "/assignments/#{@assignment.id}/unfinished"
    page.body.should include(@student.name)
  end

  it "see student's assignment" do
    visit "/assignments/#{@assignment.id}/unfinished"
    page.should have_link("查看作业")
    find_link("查看作业").click
    page.body.should include(@assignment.name)
  end

  after(:all) do
    @assignment.destroy
  end
end
