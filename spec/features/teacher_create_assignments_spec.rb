# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "TeacherCreateAssignments" do
  before(:each) do
    login_with_default_teacher
  end

  describe 'create new assignment' do
    before(:all) do
      @favorite = FactoryGirl.create(:favorite)
      @group = Group.first
    end

    it "could allow teacher to create new assignment" do
      visit "/users/1/assignments"
      click_on '布置新作业'
      expect(page).to have_content(@favorite.book[:name].gsub(/\s/, "&nbsp;"))
      expect(page).to have_link(@favorite.book[:exams].first[:name].split(/\s/)[0])
      expect(page).to have_text('从书架中选择练习册中的某一组习题')

      all('a').select {|elt| elt.text == @favorite.book[:exams].first[:name]}.first.click
      expect(page).to have_selector('#new_assignment_div')

      within('#new_assignment_div') do
        fill_in '备注', with: '测试布置作业'
        select @group.name, from: '目标群组'
        click_on '布置'
      end

      visit "/users/1/assignments"
      expect(page.body).to include(@favorite.book[:exams].first[:name].split(/\s/)[0])
      expect(page.body).to include('有效')
    end

    after(:all) do
      @favorite.destroy
    end
  end
end
