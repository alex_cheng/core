# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "AssignmentAttempts" do
  before(:all) do
    @assignment = FactoryGirl.create(:assignment)
  end

  before(:each) do
    @student = login_with_default_student
  end

  it "should list assignments assigned to the student" do
    visit "/users/#{@student.id}/assignments"
    page.status_code.should be(200)
    expect(page.body).to include(@assignment.name)
    page.should_not have_button('布置')
  end

  it "should allow student to do the assignment" do
    visit "/users/#{@student.id}/assignments"
    find_link('做作业').click

    page.body.should include(%Q{new PracticeEngine($board, "#{@assignment.practice_id}"})

    visit "/users/#{@student.id}/assignments"
    page.should have_link('继续做作业')
  end

  after(:all) do
    @assignment.destroy
  end
end
