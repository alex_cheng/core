# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140114151218) do

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "assignment_attempts", :force => true do |t|
    t.integer  "assignment_id"
    t.integer  "participant_id"
    t.integer  "previous_attempt_id"
    t.integer  "status"
    t.float    "progress"
    t.text     "user_answers"
    t.float    "score"
    t.datetime "finished_time"
    t.integer  "total_count"
    t.integer  "right_count"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "assignment_attempts", ["assignment_id"], :name => "index_assignment_attempts_on_assignment_id"
  add_index "assignment_attempts", ["participant_id"], :name => "index_assignment_attempts_on_assignee_id"
  add_index "assignment_attempts", ["previous_attempt_id"], :name => "index_assignment_attempts_on_previous_attempt_id"

  create_table "assignments", :force => true do |t|
    t.datetime "begin_time"
    t.datetime "due_time"
    t.datetime "deadline"
    t.string   "practice_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.integer  "assigned_group_id"
    t.text     "comment"
    t.integer  "creator_id"
    t.integer  "status",            :default => 0
    t.integer  "assigned_user_id"
  end

  add_index "assignments", ["assigned_group_id"], :name => "index_assignments_on_assigned_group_id"
  add_index "assignments", ["creator_id"], :name => "index_assignments_on_creator_id"

  create_table "courseware_materials", :force => true do |t|
    t.integer  "courseware_id"
    t.string   "upload_file_name"
    t.string   "upload_content_type"
    t.integer  "upload_file_size"
    t.datetime "upload_updated_at"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "creating_by_who"
    t.string   "abstract"
    t.string   "preview_file_name"
    t.string   "preview_content_type"
    t.integer  "preview_file_size"
    t.datetime "preview_updated_at"
  end

  create_table "coursewares", :force => true do |t|
    t.string   "subject",     :default => "英语",     :null => false
    t.integer  "grade",       :default => 3,        :null => false
    t.string   "topic",       :default => "未命名课件",  :null => false
    t.text     "descript"
    t.text     "materials"
    t.string   "author"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "visibility",  :default => "public"
    t.string   "author_type", :default => "admin"
    t.string   "keywords"
    t.string   "publish"
  end

  create_table "favorites", :force => true do |t|
    t.integer  "user_id"
    t.string   "comment"
    t.integer  "book_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "favorites", ["book_id"], :name => "index_favorites_on_book_id"
  add_index "favorites", ["user_id"], :name => "index_favorites_on_user_id"

  create_table "group_member_invitations", :force => true do |t|
    t.string   "token"
    t.integer  "group_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.string   "email"
    t.string   "name"
  end

  create_table "group_member_relationships", :force => true do |t|
    t.integer  "child_id"
    t.integer  "parent_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "group_id"
  end

  create_table "group_member_virtual_invitations", :force => true do |t|
    t.string   "child_email"
    t.string   "child_name"
    t.integer  "group_id"
    t.string   "parent_email"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "group_members", :force => true do |t|
    t.integer  "group_id"
    t.string   "role"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "usertype"
    t.datetime "joined_at"
    t.string   "name"
  end

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.text     "desc"
    t.string   "type"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "founder_id"
  end

  create_table "messages", :force => true do |t|
    t.text     "head"
    t.text     "content"
    t.string   "tags"
    t.integer  "posted_by_id"
    t.integer  "parent_message_id"
    t.integer  "group_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "practice_histories", :force => true do |t|
    t.string   "exam_id"
    t.string   "user_id"
    t.text     "user_answers"
    t.integer  "status"
    t.datetime "begin_at"
    t.datetime "end_at"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "profiles", :force => true do |t|
    t.string   "nickname"
    t.string   "gender"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "shared_files", :force => true do |t|
    t.text     "description"
    t.string   "tags"
    t.integer  "shared_by_id"
    t.integer  "group_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
  end

  create_table "simple_captcha_data", :force => true do |t|
    t.string   "key",        :limit => 40
    t.string   "value",      :limit => 6
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "simple_captcha_data", ["key"], :name => "idx_key"

  create_table "user_relations", :force => true do |t|
    t.integer  "user_id"
    t.integer  "target_user_id"
    t.string   "type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "name"
    t.string   "im"
    t.string   "phone"
    t.string   "usertype"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "virtual",                :default => false
    t.string   "flags"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
