# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin1 = Admin.create :name => 'Yvonne', :email => "yvonne@51kehou.com", :password => "yvonne123"
admin2 = Admin.create :name => 'Dennis', :email => "dennis@51kehou.com", :password => "dennis123"
admin3 = Admin.create :name => 'Sam', :email => "sam@51kehou.com", :password => "samsam123"
admin4 = Admin.create :name => 'Alex', :email => "alex@51kehou.com", :password => "alexalex123"

teacher = User.create :name => 'Teacher Wang', :email => "teacher@51kehou.com", :password => "12345678", :usertype => "teacher"
teacher2 = User.create :name => 'Teacher Fu', :email => "teacher_fu@51kehou.com", :password => "12345678", :usertype => "teacher"
student = User.create :name => 'Student Lang', :email => "student@51kehou.com", :password => "12345678", :usertype => "student"
parent = User.create :name => 'Parent Tang', :email => "parent@51kehou.com", :password => "12345678", :usertype => "parent"

courseware1 = Courseware.create :author => admin1.email, :author_type => 'admin', :descript => '描述信息描述信息',
  :grade => 31, :subject => '英语', :topic => '小学三年级一班期中考试卷A01及讲座小学三年级一班期中考试卷A01及讲座',
  :visibility => 'public', :publish => '人教版'

courseware2 = Courseware.create :author => teacher.email, :author_type => 'teacher', :descript => '描述信息描述信息',
  :grade => 31, :subject => '英语', :topic => '小学三年级一班期中考试卷A02及讲座',
  :visibility => 'public', :publish => '人教版'

courseware3 = Courseware.create :author => teacher.email, :author_type => 'teacher', :descript => '描述信息描述信息',
  :grade => 31, :subject => '英语', :topic => "#{teacher.name}的私有课件",
  :visibility => 'private', :publish => '人教版'

courseware3 = Courseware.create :author => teacher2.email, :author_type => 'teacher', :descript => '描述信息描述信息',
  :grade => 31, :subject => '英语', :topic => "#{teacher2.name}的私有课件",
  :visibility => 'private', :publish => '人教版'

group1 = Group.new(name: '王老师英语培训', desc: '英语学习不再难')
group1.founder_id = teacher.id
group1.save

GroupMember.create group_id: group1.id, user_id: teacher.id, name: teacher.name
group_member_child = GroupMember.create group_id: group1.id, user_id: student.id, name: student.name
group_member_parent = GroupMember.create group_id: group1.id, user_id: parent.id, name: parent.name
GroupMemberRelationship.create child_id: group_member_child.id, parent_id: group_member_parent.id, group_id: group1.id

['test message 1', 'test message 2', 'test message 3'].each do |h|
  m = Group.first.messages.build head:h, content: 'this is a test.', posted_by_id: teacher.id
  m.save
end

shared_file = SharedFile.new(description: 'A shared file for test.', shared_by_id: teacher.id, group_id: group1.id)
shared_file.attachment = File.open(File.expand_path('../../doc/README_FOR_APP', __FILE__), 'rb')
shared_file.save
