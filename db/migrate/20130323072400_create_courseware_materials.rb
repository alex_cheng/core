# -*- encoding : utf-8 -*-
class CreateCoursewareMaterials < ActiveRecord::Migration
  def change
    create_table :courseware_materials do |t|
      t.integer  "courseware_id"
      t.string   "upload_file_name"
      t.string   "upload_content_type"
      t.integer  "upload_file_size"
      t.datetime "upload_updated_at"
      t.timestamps
    end
  end
end
