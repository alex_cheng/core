# -*- encoding : utf-8 -*-
class CreateGroupMemberVirtualInvitations < ActiveRecord::Migration
  def change
    create_table :group_member_virtual_invitations do |t|
      t.string :child_email
      t.string :child_name
      t.integer :group_id
      t.string :parent_email

      t.timestamps
    end
  end
end
