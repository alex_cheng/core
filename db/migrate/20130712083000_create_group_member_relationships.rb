# -*- encoding : utf-8 -*-
class CreateGroupMemberRelationships < ActiveRecord::Migration
  def change
    create_table :group_member_relationships do |t|
      t.integer :child_id
      t.integer :parent_id

      t.timestamps
    end
  end
end
