# -*- encoding : utf-8 -*-
class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.belongs_to :user
      t.string :comment
      t.integer :book_id

      t.timestamps
    end
    add_index :favorites, :user_id
    add_index :favorites, :book_id
  end
end
