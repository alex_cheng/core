# -*- encoding : utf-8 -*-
class CreateGroupMemberInvitations < ActiveRecord::Migration
  def change
    create_table :group_member_invitations do |t|
      t.string :token
      t.string :child_email
      t.string :parent_email
      t.string :child_name
      t.integer :group_id

      t.timestamps
    end
  end
end
