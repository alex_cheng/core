# -*- encoding : utf-8 -*-
class AddPublishToCourseware < ActiveRecord::Migration
  def change
    add_column :coursewares, :publish, :string
  end
end
