# -*- encoding : utf-8 -*-
class CreateUserRelations < ActiveRecord::Migration
  def change
    create_table :user_relations do |t|
      t.integer :user_id
      t.integer :target_user_id
      t.string :type

      t.timestamps
    end
  end
end
