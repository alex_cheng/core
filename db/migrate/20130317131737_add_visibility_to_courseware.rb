# -*- encoding : utf-8 -*-
class AddVisibilityToCourseware < ActiveRecord::Migration
  def change
    add_column :coursewares, :visibility, :string, :default => 'public'
    add_column :coursewares, :author_type, :string, :default => 'admin'
  end
end
