# -*- encoding : utf-8 -*-
class AddFounderToGroup < ActiveRecord::Migration
  def change
    add_column :groups, :founder_id, :integer
  end
end
