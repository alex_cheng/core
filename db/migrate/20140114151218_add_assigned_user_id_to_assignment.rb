class AddAssignedUserIdToAssignment < ActiveRecord::Migration
  def change
    add_column :assignments, :assigned_user_id, :integer
  end
end
