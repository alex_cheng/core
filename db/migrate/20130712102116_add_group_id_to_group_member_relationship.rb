# -*- encoding : utf-8 -*-
class AddGroupIdToGroupMemberRelationship < ActiveRecord::Migration
  def change
    add_column :group_member_relationships, :group_id, :integer
  end
end
