# -*- encoding : utf-8 -*-
class AddPreviewToCoursewareMaterials < ActiveRecord::Migration
  def self.up
    change_table :courseware_materials do |t|
      t.attachment :preview
    end
  end

  def self.down
    drop_attached_file :courseware_materials, :preview
  end
end
