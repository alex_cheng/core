# -*- encoding : utf-8 -*-
class CreateCoursewares < ActiveRecord::Migration
  def change
    create_table :coursewares do |t|
      t.string :subject, :null => false, :default => '英语'
      t.integer :grade, :null => false, :default => 3
      t.string :topic, :null => false, :default => '未命名课件'
      t.text :descript
      t.text :materials
      t.string :author, :null => true

      t.timestamps
    end
  end
end
