# -*- encoding : utf-8 -*-
class AddParentIdToAssignments < ActiveRecord::Migration
  def change
    add_column :assignments, :parent_id, :integer, references: :assignments
  end
end
