# -*- encoding : utf-8 -*-
class AddStatusToAssignment < ActiveRecord::Migration
  def change
    add_column :assignments, :status, :integer, :default => 0
  end
end
