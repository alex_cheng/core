# -*- encoding : utf-8 -*-
class CreateSharedFiles < ActiveRecord::Migration
  def change
    create_table :shared_files do |t|
      t.text :description
      t.string :tags
      t.belongs_to :shared_by, class_name: 'User'
      t.belongs_to :group

      t.timestamps
    end
  end
end
