# -*- encoding : utf-8 -*-
class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :nickname
      t.string :gender
      t.text :description
      t.belongs_to :user
      t.timestamps
    end
  end
end
