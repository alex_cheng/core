# -*- encoding : utf-8 -*-
class AddKeywordsToCourseware < ActiveRecord::Migration
  def change
    add_column :coursewares, :keywords, :string
  end
end
