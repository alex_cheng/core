# -*- encoding : utf-8 -*-
class AddAttachmentAttachmentToSharedFiles < ActiveRecord::Migration
  def self.up
    change_table :shared_files do |t|
      t.has_attached_file :attachment
    end
  end

  def self.down
    drop_attached_file :shared_files, :attachment
  end
end
