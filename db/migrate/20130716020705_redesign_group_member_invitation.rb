# -*- encoding : utf-8 -*-
class RedesignGroupMemberInvitation < ActiveRecord::Migration
  def up
    add_column :group_member_invitations, :user_id, :integer
    add_column :group_member_invitations, :email, :string
    add_column :group_member_invitations, :name, :string
    
    remove_column :group_member_invitations, :child_email
    remove_column :group_member_invitations, :parent_email
    remove_column :group_member_invitations, :child_name
  end
  
  def down
    remove_column :group_member_invitations, :user_id
    remove_column :group_member_invitations, :email
    remove_column :group_member_invitations, :name
    
    add_column :group_member_invitations, :child_email, :string
    add_column :group_member_invitations, :parent_email, :string
    add_column :group_member_invitations, :child_name, :string

  end
end
