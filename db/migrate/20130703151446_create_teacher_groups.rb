# -*- encoding : utf-8 -*-
class CreateTeacherGroups < ActiveRecord::Migration
  def self.up
    create_table :groups do |t|
      t.string :name
      t.text :desc
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :groups
  end
end
