# -*- encoding : utf-8 -*-
class AddCreatedByToAssignment < ActiveRecord::Migration
  def change
    add_column :assignments, :creator_id, :integer
    add_index :assignments, :creator_id
  end
end
