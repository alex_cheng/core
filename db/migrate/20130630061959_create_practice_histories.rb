# -*- encoding : utf-8 -*-
class CreatePracticeHistories < ActiveRecord::Migration
  def change
    create_table :practice_histories do |t|
      t.string :exam_id
      t.string :user_id
      t.text :user_answers
      t.integer :status
      t.datetime :begin_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
