# -*- encoding : utf-8 -*-
class CreateGroupMembers < ActiveRecord::Migration
  def change
    create_table :group_members do |t|
      t.integer :group_id
      t.string :role
      t.integer :user_id

      t.timestamps
    end
  end
end
