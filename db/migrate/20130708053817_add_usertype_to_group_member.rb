# -*- encoding : utf-8 -*-
class AddUsertypeToGroupMember < ActiveRecord::Migration
  def change
    add_column :group_members, :usertype, :string
  end
end
