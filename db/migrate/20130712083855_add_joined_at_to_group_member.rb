# -*- encoding : utf-8 -*-
class AddJoinedAtToGroupMember < ActiveRecord::Migration
  def change
    add_column :group_members, :joined_at, :timestamp
  end
end
