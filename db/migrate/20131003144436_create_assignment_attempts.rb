# -*- encoding : utf-8 -*-
class CreateAssignmentAttempts < ActiveRecord::Migration
  def up
    create_table :assignment_attempts do |t|
      t.references :assignment
      t.references :assignee, class_name: :user
      t.references :previous_attempt, class_name: :assignment_attempt
      t.integer :status
      t.float :progress
      t.text :user_answers
      t.float :score
      t.datetime :finished_time
      t.integer :total_count
      t.integer :right_count

      t.timestamps
    end

    add_index :assignment_attempts, :assignment_id
    add_index :assignment_attempts, :assignee_id
    add_index :assignment_attempts, :previous_attempt_id


    change_table :assignments do |t|
      t.remove :user_id, :user_answers, :status, :score, :finish_time, :total_count, :progress, :right_count, :parent_id
      t.references :assigned_group, class_name: :group
      t.text :comment

      t.index :assigned_group_id
    end
  end

  def down
    change_table :assignments do |t|
      t.integer  :user_id
      t.text     :user_answers
      t.integer  :status
      t.float    :score
      t.datetime :finish_time
      t.integer  :total_count
      t.integer  :progress
      t.integer  :right_count
      t.integer  :parent_id

      t.remove :assigned_group_id, :comment
    end

    drop_table :assignment_attempts
  end
end
