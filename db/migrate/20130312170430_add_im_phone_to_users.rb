# -*- encoding : utf-8 -*-
class AddImPhoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :im, :string
    add_column :users, :phone, :string
  end
end
