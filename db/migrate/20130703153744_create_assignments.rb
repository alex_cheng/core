# -*- encoding : utf-8 -*-
class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.datetime :begin_time
      t.datetime :due_time
      t.datetime :deadline
      t.string :practice_id
      t.belongs_to :user
      t.text :user_answers
      t.integer :status
      t.float :score
      t.datetime :finish_time

      t.timestamps
    end
  end
end
