class RenameAssignmentAttemptColumnAssigneeToParticipant < ActiveRecord::Migration
  def up
    rename_column :assignment_attempts, :assignee_id, :participant_id
  end

  def down
    rename_column :assignment_attempts, :participant_id, :assignee_id
  end
end
