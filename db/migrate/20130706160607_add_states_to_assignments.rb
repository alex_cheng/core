# -*- encoding : utf-8 -*-
class AddStatesToAssignments < ActiveRecord::Migration
  def change
    add_column :assignments, :total_count, :integer
    add_column :assignments, :progress, :integer
    add_column :assignments, :right_count, :integer
  end
end
