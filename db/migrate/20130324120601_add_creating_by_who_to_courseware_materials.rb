# -*- encoding : utf-8 -*-
class AddCreatingByWhoToCoursewareMaterials < ActiveRecord::Migration
  def change
    add_column :courseware_materials, :creating_by_who, :integer
  end
end
