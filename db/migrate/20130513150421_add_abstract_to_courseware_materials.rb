# -*- encoding : utf-8 -*-
class AddAbstractToCoursewareMaterials < ActiveRecord::Migration
  def change
    add_column :courseware_materials, :abstract, :string
  end
end
