# -*- encoding : utf-8 -*-
class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :head
      t.text :content
      t.string :tags
      t.belongs_to :posted_by, class_name: 'User'
      t.belongs_to :parent_message, class_name: 'Message'
      t.belongs_to :group
      t.timestamps
    end
  end
end
