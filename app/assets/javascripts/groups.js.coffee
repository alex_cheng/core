# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  # 群描述信息展开触发器
  $('.group-desc-more-trigger').click (e) ->
    $desc = $(this).closest('.block').find('.group-desc')
    $desc.text $desc.data('fulldesc')
    
    $(this).remove()
    e.preventDefault()

  # 点击添加成员按钮弹出form
  $(".addcharbtn").click ->
    $trigger = $(this)
    trigger_offset = $trigger.position();
    $target = $('#addchar').css 
      top: trigger_offset.top + 50
      left: trigger_offset.left - 10
    .toggle()
    .find('input:visible:first').focus()
  
  $('.closeIcon').click ->
    $('#addchar').hide()


  # 添加新成员表单控制
  $new_member_form = $('form#invite_member_form').bind 'ajax:success', ->
    alert('成功发出邀请！')
    $new_member_form.trigger('reset-errors')[0].reset()
    $new_member_form.find('input:visible:first').focus()
    
    window.location.reload()
  
  # 表单验证出错
  .bind 'ajax:error', (e, data)->
    $new_member_form.trigger('reset-errors')
    # 输出验证错误信息
    visible_error_fields = []
    for k, v of $.parseJSON(data.responseText)
      $error_field = $new_member_form.find("[name$='[#{k}]']").addClass('error')
      $('<span class="error-block"/>').text(v[0]).insertAfter $error_field
      
      visible_error_fields.push $error_field if $error_field.is(":visible")
    
    $new_member_form.find('.error:visible:first').focus()
    alert("提交数据时发生错误，请您刷新页面后重试！") if visible_error_fields.length == 0
  
  .bind 'reset-errors', ->
    $new_member_form.find('.error').removeClass('error').end()
      .find('.error-block').remove() 
  # 防止连点，禁用提交按钮
  .bind 'submit', ->
    $new_member_form.find(':submit').attr('disabled', 'disabled')
  # 提交完成后，还原启用提交按钮
  .bind 'ajax:complete', ->
    $new_member_form.find(':submit').removeAttr('disabled')
    
    
  show_content = ->
    $(".blocks").not('#title').hide()
    $(".blocks").not('#title').eq(parseInt(id)).show()
    $(".tabs a").removeClass("on")
    $(".tabs a:eq("+parseInt(id)+")").addClass("on")