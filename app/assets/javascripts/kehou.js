// JavaScript Document

function Books($obj)
{
	this.$obj = $obj;
	this.cur = 0;
	this.per_page = 5;
	this.list = $obj.find(".block");
	this.len = this.list.length;
	this.page_num = Math.ceil(this.len / this.per_page);
	this.time_span = 300;

	this.init = function() {
		var self = this;
		this.$obj.find(".p_left").on("click", function(){
			self.goto(-1);
		});
		this.$obj.find(".p_right").on("click", function(){
			self.goto(1);
		})
		this.goto(0);
	};

	this.goto = function(page){
		if(page>0)
		{
			this.cur = ((this.cur + page) >= this.page_num) ? 0 : (this.cur + page);
		}
		else
		{
			this.cur = ((this.cur + page) < 0) ? this.page_num-1 : (this.cur + page);
		}
		this.list.hide(this.time_span);
		this.list.slice(this.cur*this.per_page, (this.cur+1)*this.per_page).show(this.time_span);
	}
	this.init();
}



function Pages($obj, parent)
{
	this.page_max = 30;
	this.$obj = $obj;
	this.parent = parent;
	this.$list = $obj.find(".block");
	this.length = $obj.find(".block").length;
	
	this.init = function() {
		this.init_html();
	};
	this.init_html = function(){
		var self = this;
		if(this.length > 0)
		{
			var $cont = $("<div />").attr({"class":"page_nav"});
			var i = 0;
			for(var a=0; a<this.length; a++)
			{
				if(a % this.page_max == 0)
				{
					i ++ ;
					$cont.append($("<a />").attr({"data":i,
												  "class":"pagenum",
												  "id": "pagenav_"+i
												 })
											.html(i)
											.bind("click", function(){
												self.goto_page($(this).attr("data"));
											})
								)
				}
				$(this.$list[a]).addClass("p")
								.addClass("p_"+i);
			}
			// add first and last
			$("<a />").attr({"data":1,
							 "class":"pagenum",
							 "id":"pagenav_first"
							})
					  .html("<<")
					  .bind("click", function(){
					  	self.goto_page(1);
					  })
					  .prependTo($cont);
			$("<a />").attr({"data": Math.ceil(this.length / this.page_max),
							 "class":"pagenum",
							 "id":"pagenav_last"
							})
					  .html(">>")
					  .bind("click", function(){
					  	self.goto_page(Math.ceil(self.length / self.page_max));
					  })
					  .appendTo($cont);
			
			$(this.$obj).prepend($cont);
		}
	};
	this.show = function() {
		this.goto_page(1);
		$(this.$obj).show();
	};
	this.hide = function(){
		$(this.$obj).hide();
	};
	this.goto_page = function(page) {
		$(this.$obj).find(".p").hide();
		$(this.$obj).find(".p_"+page).show();
		
		$(this.$obj).find(".page_nav a").removeClass("disabled");
		$(this.$obj).find(".page_nav #pagenav_"+page).addClass("disabled");
		
		if(page == 1) 
		{ 
			$(this.$obj).find("#pagenav_first").addClass("disabled"); 
		}
		if(page >= Math.ceil(this.length / this.page_max) ) 
		{
			$(this.$obj).find("#pagenav_last").addClass("disabled");
		}
	};
	this.init();
}	