// JavaScript Document

function getPageSize()
{
	var xScroll, yScroll;
	if (window.innerHeight && window.scrollMaxY)
	{
		xScroll = document.body.scrollWidth + window.scrollMaxX;
		yScroll = window.innerHeight + window.scrollMaxY;
	}
	else if (document.body.scrollHeight > document.body.offsetHeight)
	{ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	}
	else
	{ // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.scrollWidth;
		//xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}

	var windowWidth, windowHeight;
	if (self.innerHeight)
	{  // all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	}
	else if (document.documentElement && document.documentElement.clientHeight)
	{ // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	}
	else if (document.body)
	{ // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}

	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight)
	{
		pageHeight = windowHeight;
	}
	else
	{
		pageHeight = yScroll;
	}

	if(xScroll < windowWidth)
	{
		pageWidth = windowWidth;
	}
	else
	{
		pageWidth = xScroll;
	}

	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight,xScroll,yScroll)
	return arrayPageSize;
}

function unicode(s){
	var len=s.length;
	var rs="";
	for(var i=0;i<len;i++){
	var k=s.substring(i,i+1);
	rs+="\\u"+s.charCodeAt(i)+";";
	}
	return rs;
	//alert(rs);
}

/***** float msg obj on top *****/
var ajaxGlobalMsgT = null;
function showGlobalAjaxMsg(msg, style, timeout){
	if(msg!="")
	{
		var pageSizes = getPageSize();
		var MsgDiv = $("#divAjaxGlobalMsg");
		$(MsgDiv).removeClass("warningMsgDiv");
		$(MsgDiv).css("width","");
		if(ajaxGlobalMsgT!=null)
		{
			clearTimeout(ajaxGlobalMsgT);
		}
		if(style=="warning")
		{
			$(MsgDiv).addClass("warningMsgDiv");
		}
		$(MsgDiv).html("<p>"+msg+"</p>");
		var width = $(MsgDiv).width();
		$(MsgDiv).css("left", ((pageSizes[2]-width)/2)+"px");
		$(MsgDiv).css("width", width+"px");
		$(MsgDiv).css("display",'inline');
		ajaxGlobalMsgT = setTimeout("hideGlobalAjaxMsg()", timeout);
	}
	else
	{
		hideGlobalAjaxMsg();
	}
}
function hideGlobalAjaxMsg()
{
	$("#divAjaxGlobalMsg").hide();
	ajaxGlobalMsgT = null;
}
/***** float msg obj on top *****/


// add help tip to each label
function addHelpTipsFunc()
{
	$('*[notice]').mousemove(function(event){

		var current = $(this);
		var flag = 0;

		var text = "";
		var text1 = $(this).attr("title");
		var text2 = $(this).attr("notice");

		if(text2&&text2!="")
		{
			text = 	text2;
		}
		else
		{
			if(text1&&text1!="")
			{
				text = text1;
			}
		}

		if($(".helpTip").html()==null||$(".helpTip").html()=="")
		{
			if(text!="")
			{
				$("body").append("<div class='helpTip'><div class='tipContent'>"+text+"</div></div>");
			}
		}

		if(event.pageX+$(".helpTip").width()>($("body").width()-25))
		{
			$(".helpTip").addClass("helpTipRight");
			$(".helpTip").css("left",event.pageX-$(".helpTip").width() - 5+"px");
			$(".helpTip").css("top",event.pageY -8 +"px");
		}
		else
		{
			$(".helpTip").removeClass("helpTipRight");
			$(".helpTip").css("left",event.pageX + 15+"px");
			$(".helpTip").css("top",event.pageY -8 +"px");
		}
		$(".helpTip").mouseout(function(){
			$(this).remove("div");
		})
	});
	$('*[notice]').mouseout(function(){
		$(".helpTip").remove("div");
	});


	$(".helpTips").bind("mousemove",function(event){
	})
	$(".helpTips").bind("mouseout",function(){
		$(this).remove("div");
	})
}

/* remove duplicate value in list */
function distinct_list(list)
{
	var arr = [];
	var len = list.length;

	for ( var i = 0; i < len; i++ )
	{
		for( var j = i+1; j < len; j++ )
		{
			if( list[i] === list[j] ){
				j = ++i;
			}
		}
		arr.push( list[i] );
	}
	return arr;
}

/* replace upload img name to "_" in it */
function change_img_name(src, type)
{
	if(type==1)
	{
		return src.split(".")[0]+"_."+src.split(".")[1];
	}
	else
	{
		return src.replace("_.", ".");
	}
}

/* limit input
	type: 1: only alphabet
*/
function format_input(id, type)
{
	var txt = $("#"+id).val();
	if(type == 1)
	{
		var regex = /[a-zA-Z0-9]/;
		var str = ""
		for(var a=0; a<txt.length; a++)
		{
			if(regex.test(txt[a]))
			{
				str += txt[a];
			}
		}
	}
	$("#"+id).val(str);
}


function set_disabled(type,obj)
{
	if(type)
	{
		$(obj).addClass("errorInput");
	}
	else
	{
		$(obj).removeClass("errorInput");
	}
}

function re_val(type, str_in)
{
	var re = /^/
	var msg = "";
	if (type == "email")
	{
		re =  /^[a-zA-Z0-9_][a-zA-Z0-9_\-\.]*@[a-zA-Z0-9_\-]+(\.[a-zA-Z0-9_\-]+)+$/
		msg = "eg: xxx@xxx.xx";
	}
	else if(type == "password" || type == "name")
	{
		re =  /^[a-zA-Z0-9_]{3,12}/
		msg = "must be 3-12 alphabet or number";
	}
	return [re.test(str_in), msg];
}

function resetInnerPageHight(iframe_id)
{
	var p = getPageSize();
	//parent.$("#"+iframe_id).css("height", p[1]);
	parent.document.getElementById(iframe_id).style.height = p[1]+"px";
}

/* header topmenu show float div */
$(function () {
	$(".header .noticeWhiteIcon").toggle(
		function () {
			$(".header #noticeMsg").css("left", $(this).offset().left - $(".header #noticeMsg").width() + $(this).width() - 3);
			$(".header #noticeMsg").show();
		},
		function  () {
			$(".header #noticeMsg").hide();
		}
	);
	$(".header .mailWhiteIcon").toggle(
		function  () {
			$(".header #mailMsg").css("left", $(this).offset().left - $(".header #mailMsg").width() + $(this).width() - 3);
			$(".header #mailMsg").show();
		},
		function  () {
			$(".header #mailMsg").hide();
		}
	);
	$(".header #topmenuIcon").toggle(
		function  () {
			$(this).removeClass("folderIcon");
			$(this).addClass("unfolderIcon");
			$(".header #topmenu").css("left", $(this).offset().left - $(".header #topmenu").width() + $(this).width() - 3);
			$(".header #topmenu").show();
		},
		function  () {
			$(this).removeClass("unfolderIcon");
			$(this).addClass("folderIcon");
			$(".header #topmenu").hide();
		}
	);

	addHelpTipsFunc();

});




function show_content(ind)
{
	$(".course_list .blocks").hide();
	$(".course_list .blocks:eq("+ind+")").show();
	$(".groups_list .blocks").hide();
	$(".groups_list .blocks:eq("+ind+")").show();
	$(".tabs a").removeClass("on");
	$(".tabs a:eq("+parseInt(ind)+")").addClass("on");
}

$.fn.clicktoggle = function(a, b) {
    return this.each(function() {
        var clicked = false;
        $(this).click(function() {
            if (clicked) {
                clicked = false;
                return b.apply(this, arguments);
            }
            clicked = true;
            return a.apply(this, arguments);
        });
    });
};



// homepage top ads. block
function PicShow($obj)
{
    this.$obj = $obj;
    this.play_list = $obj.find(".block");
    this.time_span = 7000;
    this.cur = 0;
    this.p = null;

    this.init = function(){
        this.hide_all();
        this.init_controller();
        this.play(this.cur);
    };
    this.hide_all = function(){
        for(var a=0; a<this.play_list.length; a++)
        {
            $(this.play_list[a]).fadeOut();
        }
    };
    this.init_controller = function(){
        var self = this;
        var $contr = $("<div />").attr({"class": "pic_contrs"})
                                 .css({"position": "absolute",
                                        "z-index": 1000,
                                        "width": self.$obj.width(),
                                        "text-align": "center",
                                       "margin-top": self.$obj.height() - 26});
        for(var a=0; a<this.play_list.length; a++)
        {
            $contr.append($("<div />").attr({"class": "dot",
                                             "idx": a})
                                      .css({"width": 12,
                                              "height": 12,
                                            "margin": "5px",
                                            "cursor": "pointer",
                                            "display": "inline-block",
                                            "-moz-border-radius": 8,
                                            "-webkit-border-radius" : 8,
                                            "border-radius" : 8,
                                            "background": "#ddd"})
                                      .on("click", function(){
                                              this.p
                                            self.play($(this).attr("idx"));
                                      }))
        }
        this.$obj.prepend($contr);
    };
    this.play = function(idx){
        this.stop_play();
        var self = this;
        var next = this.cur<this.play_list.length-1 ? parseInt(this.cur)+1 : 0;
        var cur = arguments.length>0 ? idx : next;
        this.cur = cur;
        //this.hide_all();
        //alert($(this.play_list).filter(":visible").html())
        var $s = this.play_list.filter(":visible");
        if ($s.length > 0)
        {
            $($s).fadeOut(300, function(){
                                            $(self.play_list[self.cur]).fadeIn(300);
                                            self.auto_play();
                                        })
        }
        else
        {
            $(self.play_list[self.cur]).fadeIn(300);
            self.auto_play();
        }
        this.$obj.find(".dot").css("background", "#ddd")
        this.$obj.find(".dot").eq(this.cur).css("background", "#0083B7");
    };
    this.auto_play = function(){
        var self = this;
        this.p = setTimeout(function(){
                                self.play();
                            }, this.time_span);
    };
    this.stop_play = function(){
        clearTimeout(this.p);
        this.p = null;
    };

    this.init();
}
