// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery.ui.all
//= require jquery_ujs
//= require jquery.remotipart
//= require jquery-fileupload
//= require swf_fu
//= require_tree .

$(function(){
	$(document).on("click", "#recognize_captcha", function(){
	   $.get("/update_captcha", function(data){
	      $(".simple_captcha").replaceWith(data);
	   });
	});
})

function add_captcha_funcs()
{
	$(".finput").on("focus", function(ev){
					$(this).removeClass($(this).attr("addclass"));
					})
				.on("blur", function(ev){
					var input_list = $(".finput");
					for(var a=0; a<input_list.length; a++)
					{
						if($(input_list[a]).val() == "") {
							$(input_list[a]).addClass($(input_list[a]).attr("addclass"));
						}
						else
						{
							$(input_list[a]).removeClass($(input_list[a]).attr("addclass"));
						}
					}
				})
}
