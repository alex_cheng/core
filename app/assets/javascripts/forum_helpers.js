// This function logins forum with given email address and password.
// It will try 3 times if login fails. If the attemptions are more than
// 3, then it won't login again and report an alert to users.
function sign_in_forum(email, password, triedTimes) {
    var loginReq = "/forum/member.php?mod=logging&action=login&loginsubmit=yes&infloat=yes&username="
                    + encodeURIComponent(email) + "&password="+ encodeURIComponent(password)
    if(typeof triedTimes == "undefined") {
        triedTimes = 0;
    }

    var retry = function(){
        triedTimes ++;
        if(triedTimes <= 3) {
            setTimeout("sign_in_forum(\"" + email + "\", \"" + password + "\", " + triedTimes+ ");", triedTimes * 500);
        }
        else {
            alert('无法登录本站的论坛，请与管理员联系。');
        }
    };

    $.ajaxSetup({
        cache: false
    });

    $.ajax({
        url: loginReq,
        type: "POST",
        success: function () {
            check_and_retry(retry);
        },
        error: function () {
            retry();
        }
    });
}


function check_and_retry(retry)
{
    $.ajaxSetup({
        cache: false
    });

    $.ajax({
        url: "/forum/_get_user.php",
        type: "GET",
        success: function(data){
        	//alert(data)
            var data = eval("("+data+")");
            if (typeof data.uid == "undefined" || data.uid == "")
            {
                retry();
            }
            else {
                $.ajax({
                    url: "/signed_in_forum",
                    type: "GET"
                })
            }
        }
    });
}


function do_logout(triedTimes)
{
    if(typeof triedTimes == "undefined") {
        triedTimes = 0;
    }

    var retry = function(){
        triedTimes ++;
        if(triedTimes <= 3) {
            setTimeout("do_logout(" + triedTimes + ");", triedTimes * 500);
        }
        else {
            alert('无法退出本站的论坛，请与管理员联系。');
            $("#logoutlink").click();
        }
    };

    $.ajaxSetup({
        cache: false
    });

    $.ajax({
        url: "/forum/_get_user.php",
        type: "GET",
        success: logout_forum,
        error: retry
    });

    function logout_forum(data) {
        data = eval("("+data+")");
        if (typeof data.uid != "undefined" && data.uid != "")
        {
            var logout_link = "/forum/" + data.logout_link;

            $.ajaxSetup({
                cache: false
            });

            $.ajax({
                url: logout_link,
                type: "GET",
                success: retry,
                error: retry
            });
        }
        else
        {
            $("#logoutlink").click();
        }
    }
}

