# -*- encoding : utf-8 -*-
module CoursewaresHelper
  
  def trans_author_type(author_type)
    { 'admin' => '管理员', 'teacher' => '老师' }[author_type]
  end
  
  def trans_visibility(visibility)
    { 'public' => '公开', 'private' => '私有' }[visibility]
  end
  
  def trans_grade(grade)
    Courseware::Grades.select{ |x| x[1] == grade }.map{ |x| x[0] }.first or '不限'
  end
  
  def coursewares_path(*courseware)
    url_for :action => :index, :controller => :coursewares, :user_type => params[:user_type], :only_path => true
  end
  
  def edit_courseware_path(*args)
    if args.size > 0
      courseware = args[0]
    else
      courseware = @courseware
    end
    url_for :action => :edit, :controller => :coursewares, :user_type => params[:user_type], :only_path => true, :id => courseware.id
  end
  
  def courseware_path(*args)
    url_for(:action => :show, :controller => :coursewares, :user_type => params[:user_type], :only_path => true, :id => args[0].id)
  end
  
  def current_user_or_admin
    if params[:user_type] == :admin
      current_admin
    else
      current_user
    end
  end
  
end

