# -*- encoding : utf-8 -*-
module ApplicationHelper  
    def truncate(text, length = 30, truncate_string = "...")
        if text
        l = length - truncate_string.chars.length
        chars = text.chars
            (chars.length > length ? chars[0...l] + truncate_string : text).to_s
        end
    end
    
    def cut_string(charset,src,start,length)
        require "iconv"
        @conv=Iconv.new("UTF-16",charset)
        @reverse_conv=Iconv.new(charset,"UTF-16")
        p_start=start.class==Fixnum&&start>=0
        p_length=length.class==Fixnum&&length>=0
        return "" unless src&&p_start&&p_length
        src_utf16=@conv.iconv(src)
        cutted_src_utf_16=src_utf16[2*start+2,2*length]
        @reverse_conv.iconv(cutted_src_utf_16)
    end

    def truncate_u(text, length = 30, truncate_string = "..")
        text ||= ""
        l = 0
        char_array = text.unpack("U*")
        char_array.each_with_index do |c,i|
          l = l + (c < 127 ? 1 : 2)
          if l >= length
            return char_array[0..i].pack("U*") + (i < char_array.length - 1 ? truncate_string : "")
          end
        end
        return text
    end
  
    # 获取文字实际长度，ASCII码大于127即为2，否则为1
    def realength (text)
        length = 0
        text.unpack("U*").each_with_index do |c, i|
          length += (c < 127 ? 1 : 2)
        end
        length
    end
end
