# -*- encoding : utf-8 -*-
module SharedFilesHelper
  def edit_shared_file_path shared_file
    url_for action: :edit, group_id: shared_file.group_id, id: shared_file.id
  end

  def shared_file_path shared_file, options = {}
    url_for action: :show, group_id: shared_file.group_id, id: shared_file.id
  end
end
