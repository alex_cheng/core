# -*- encoding : utf-8 -*-

class Profile < ActiveRecord::Base
  has_attached_file :avatar, :styles => { :thumb => "48x48>", :medium => "100x100>" }, :default_url => "face.jpg"

	symbolize :gender, :in => {
    :male => '男',
    :female => '女',
    :unknown => '保密',
  }, :scopes => :shallow, :default => :male

  attr_accessible :description, :gender, :nickname, :avatar

  belongs_to :user
end
