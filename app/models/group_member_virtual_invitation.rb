# -*- encoding : utf-8 -*-
class GroupMemberVirtualInvitation < ActiveRecord::Base
  attr_accessible :child_email, :child_name, :group_id, :parent_email
  
  belongs_to :group
  
  validates_presence_of :child_email, :child_name, :group
  validates :child_email, email: true
  validates :parent_email, email: true
  
  validate do 
    errors.add :child_email, :usertype_invalid if User.where(email: child_email).where("usertype = ? or usertype = ?", :teacher, :parent).any?
    errors.add :parent_email, :usertype_invalid if invited_parent and User.where(email: parent_email).where("usertype = ? or usertype = ?", :teacher, :student).any?
    
    # 学生邮箱唯一，家长不限制唯一（考虑到双胞胎有可能在同一个班）
    errors.add :child_email, :taken if GroupMember.joins(:user).where("users.email = ? and group_members.group_id = ?", child_email, group_id).any?

    errors.add :parent_email, :same if invited_parent and child_email == parent_email
  end
  
  def invited_parent
    parent_email.strip.present?
  end  
end
