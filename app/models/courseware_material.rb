# -*- encoding : utf-8 -*-
class CoursewareMaterial < ActiveRecord::Base
  attr_accessible :upload, :abstract, :preview
  has_attached_file :upload
  has_attached_file :preview

  belongs_to :courseware

  include Rails.application.routes.url_helpers

  def to_jq_upload
    {
      "name" => read_attribute(:upload_file_name),
      "size" => read_attribute(:upload_file_size),
      "url" => upload.url(:original),
      "delete_url" => courseware_material_path(self),
      "delete_type" => "DELETE"
    }
  end


  def is_previewable
    preview? || upload_file_name =~ /\.(pdf|swf)$/
  end

  def preview_url
    if preview?
      preview.url(:original)
    elsif is_previewable
      upload.url(:original)
    end
  end

  def preview_type
    if preview?
      if preview_file_name =~ /\.pdf$/
        :pdf
      elsif preview_file_name =~ /\.swf$/
        :swf
      end
    else
      if upload_file_name =~ /\.pdf$/
        :pdf
      elsif upload_file_name =~ /\.swf$/
        :swf
      end
    end
  end
end
