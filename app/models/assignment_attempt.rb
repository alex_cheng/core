# -*- encoding : utf-8 -*-
class AssignmentAttempt < ActiveRecord::Base
	Status = { :not_begin => 0, :in_progress => 1, :finished => 2}
	NotBegin = Status[:not_begin]
	InProgress = Status[:in_progress]
	Finished = Status[:finished]
  StatusToDisplayText = {NotBegin => '未开始', InProgress => '进行中', Finished => '已结束'}

  attr_accessible :assignment_id, :participant_id, :previous_attempt_id, :status, :progress
  attr_accessible :user_answers, :score, :finished_time, :total_count, :right_count

  validates :status, :inclusion => { :in => Status.values }

  belongs_to :assignment
  belongs_to :participant, class_name: :user
  belongs_to :previous_attempt, class_name: :AssignmentAttempt

  has_one :next_attempt, foreign_key: :previous_attempt_id, class_name: :AssignmentAttempt

  def status_display
    StatusToDisplayText[status]
  end
end
