# -*- encoding : utf-8 -*-
class Courseware < ActiveRecord::Base
  Subjects = ['英语', '数学', '语文', '不限']
  Grades = [['不限', 0],
            ['小学三年级', 30], ['小学三年级上', 31], ['小学三年级下', 35], ['小学四年级', 40], ['小学四年级上', 41], ['小学四年级下', 45],
            ['小学五年级', 50], ['小学五年级上', 51], ['小学五年级下', 55], ['小学六年级', 60], ['小学六年级上', 61], ['小学六年级下', 65],
            ['初中一年级', 70], ['初中一年级上', 71], ['初中一年级下', 75], ['初中二年级', 80], ['初中二年级上', 81], ['初中二年级下', 85],
            ['初中三年级', 90], ['初中三年级上', 91], ['初中三年级下', 95],
            ['高中一年级', 100], ['高中一年级上', 101], ['高中一年级下', 105], ['高中二年级', 110], ['高中二年级上', 111], ['高中二年级下', 115],
            ['高中三年级', 120], ['高中三年级上', 121], ['高中三年级下', 125],
            ['留学资料', 200],]
  Middle_School_Base = 60
  High_School_Base = 90
  Visibilities = [['公开','public'], ['私有','private']]
  Publishes = ['人教版', '仁爱版', '北师大版', '外研版', '牛津译林版', '无分类']
  NoPublish = '无分类'
  UserTypes = ['admin', 'teacher']
  Categories = {
    ['电子课本教材', 'dzkbjc'] => ['电子课本', '教材'], ['教学课件', 'jxkj'] => ['课件'],
    ['教案学案', 'jaxa'] => ['教案', '学案'], ['习题集', 'xtj'] => ['习题', '习题集'], 
    ['单元测试练习', 'dycslx'] => ['单元测试'], ['拓展阅读', 'tzyd'] => ['阅读'], 
    ['精选文章', 'jxwz'] => ['精选', '精选文章']
  }
  
  validates :topic, :grade, :subject, :presence => true
  validates :topic, :uniqueness => true
  validates :subject, :inclusion => { :in => Subjects }
  validates :publish, :inclusion => { :in => Publishes }
  validates :grade, :inclusion => { :in => Grades.collect { |x| x[1] } }
  validates :visibility, :inclusion => { :in => Visibilities.collect { |x| x[1] } }
  validates :author_type, :inclusion => { :in => UserTypes }
  
  has_many :courseware_materials, :dependent => :destroy
 
  attr_accessible :author, :author_type, :descript, :grade, :materials, :subject, :publish, :topic, :visibility,
    :created_at, :updated_at, :keywords

  def Courseware.tags
    Courseware.all.reduce({}) { |mem, e| e.keywords.split(',').each { |item| mem[item.strip] = 1; }; mem }.keys
  end

  def belong_to_category?(cat_id)
    keywords_list = keywords.split(',').collect{ |x| x.strip }.uniq
    Courseware::Categories.select{ |k,v| k[1] == cat_id }.map{|k, v| v.any? {|tag| keywords_list.include? tag }}.first
  end
    
  def writable_for?(user_or_admin)
    user_or_admin.instance_of?(Admin) || (author_type == 'teacher' && author == user_or_admin.email)
  end

  def topic_with_publish
    if publish != NoPublish
      "[#{publish}]#{topic}" 
    else
      topic
    end
  end
end
