# -*- encoding : utf-8 -*-
class GroupMember < ActiveRecord::Base

  attr_accessible :group_id, :user_id, :role, :joined_at, :name
  attr_accessor :email
  before_validation do
    # usertype冗余，方便查询
    self.role ||= :member
    self.usertype ||= self.user.usertype
    self.name ||= self.user.name
  end

  symbolize :role, :in => {
    founder: '创办人',
    admin: '管理员',
    member: '成员'
  }, :scopes => true

  symbolize :usertype, :in => {
    teacher: '老师',
    student: '学生',
    parent: '家长'
  }, :scopes => true

  belongs_to :group
  belongs_to :user

  def parent
    GroupMemberRelationship.where(child_id: self.id, group_id: self.group_id).first.try :parent
  end

  def child
    GroupMemberRelationship.where(parent_id: self.id, group_id: self.group_id).first.child
  end

  scope :students, -> { where(usertype: :student) }
  scope :teachers, -> { where(usertype: :teacher) }
  scope :guardians, -> { where(usertype: :parent) }

end
