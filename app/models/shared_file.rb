# -*- encoding : utf-8 -*-
class SharedFile < ActiveRecord::Base
  attr_accessible :attachment, :description, :tags, :shared_by_id, :group_id
  has_attached_file :attachment

  validates :group_id, :presence => true # 必须在某个群里发消息

  belongs_to :shared_by, class_name: 'User'
  belongs_to :group
end
