# -*- encoding : utf-8 -*-

class User < ActiveRecord::Base
  USERTYPES = ['teacher', 'student', 'parent']

  has_attached_file :avatar, :styles => { :thumb => "48x48>", :medium => "100x100>" }, :default_url => "face.jpg"

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :validatable, :timeoutable

  symbolize :usertype, :in => {
    teacher: '老师',
    student: '学生',
    parent: '家长'
  }, :scopes => true

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :usertype, :email, :password, :password_confirmation, :remember_me, :im, :phone, :avatar, :virtual, :flags
  # attr_accessible :title, :body

  validates :name, :uniqueness => true, :presence => true

  has_many :group_members, dependent: :destroy
  has_many :groups, through: :group_members, :uniq => true
  has_many :assignments, foreign_key: :creator_id, order: 'updated_at DESC'
  has_one :profile
  has_many :favorites
  has_many :assignment_attempts, foreign_key: :participant_id

  def children_assignments
    if usertype == :parent
      group_members.map{|gm| gm.child.user}.uniq.map{|u| u.assigned_assignments}.flatten + assignments.where({assigned_user_id: child(groups.first.id) })
    else
      []
    end
  end

  # todo: 限制是一个群组内家长只能和一个学生对应
  def child(group_id)
    if usertype == :parent
      group_members.select{|g| g.group_id == group_id}.map{|gm| gm.child.user}.first
    else
      nil
    end
  end

  def assigned_assignments
    if usertype == :student
      groups.map{|g| g.assignments}.flatten + Assignment.where({assigned_user_id: id})
    else
      []
    end
  end

  def students
    if usertype == :teacher
      groups.map{|g| g.members.where(usertype: :student).map(&:user) }.flatten
    else
      []
    end
  end

  def final_avatar
    if profile && profile.avatar?
      profile.avatar
    else
      avatar
    end
  end

  after_create :create_forum_account
  after_update :update_forum_account
  after_destroy :destroy_forum_account

  def User.execute_sql_on_forum(*sql_array)
    ForumAccount.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_array))
  end

  def User.all_forum_users
    execute_sql_on_forum("SELECT email FROM `ultrax`.`pre_common_member`").collect {|x| x[0]}
  end

  def create_forum_account
    return if !password

    if User.execute_sql_on_forum("SELECT count(*) FROM `ultrax`.`pre_ucenter_members` WHERE email=?;", email).first.first > 0
      update_forum_account
    else
      salt = Random.rand.to_s.last(6)
      passwd_md5 = Digest::MD5.hexdigest(Digest::MD5.hexdigest(password) + salt)
      regdate = DateTime.now.to_i
      User.execute_sql_on_forum("INSERT INTO `ultrax`.`pre_ucenter_members`(username, password, email, regdate, salt) VALUES (?,?,?,?,?);",
                            name, passwd_md5, email, regdate, salt)
      User.execute_sql_on_forum("INSERT INTO `ultrax`.`pre_common_member`(username, password, email, regdate, groupid, credits) VALUES (?,?,?,?,?,?);",
                            name, passwd_md5, email, regdate, 10, 2)
      uid = User.execute_sql_on_forum("SELECT uid FROM `ultrax`.`pre_common_member` WHERE email=?", email).first
      User.execute_sql_on_forum("INSERT INTO `ultrax`.`pre_common_member_count`(uid) VALUES (?);", uid)
    end
  end

  def update_forum_account
    return if !password
    forum_user = User.execute_sql_on_forum("SELECT username, password, salt FROM `ultrax`.`pre_ucenter_members` WHERE email=?;", email).first
    if forum_user
      passwd_md5 = Digest::MD5.hexdigest(Digest::MD5.hexdigest(password) + forum_user[2])
      if forum_user[1] != passwd_md5 or forum_user[0] != name
        User.execute_sql_on_forum("UPDATE `ultrax`.`pre_ucenter_members` SET username=?, password=? WHERE email=?;",
                              name, passwd_md5, email)
        User.execute_sql_on_forum("UPDATE `ultrax`.`pre_common_member` SET username=?, password=? WHERE email=?;",
                              name, passwd_md5, email)
      end
    else
      create_forum_account
    end
  end

  def destroy_forum_account
    uid = User.execute_sql_on_forum("SELECT uid FROM `ultrax`.`pre_common_member` WHERE email=?", email).first
    User.execute_sql_on_forum("DELETE FROM `ultrax`.`pre_ucenter_members` WHERE email=?;", email)
    User.execute_sql_on_forum("DELETE FROM `ultrax`.`pre_common_member` WHERE email=?;", email)
    User.execute_sql_on_forum("DELETE FROM `ultrax`.`pre_common_member_count` WHERE uid=?", uid)
  end

end
