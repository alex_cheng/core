# -*- encoding : utf-8 -*-
require 'courseware'

class Clearsky
	@@all_tongbu = nil
	@@all_books = nil
	@@all_resources = nil

	def Clearsky.all_tongbu
		if @@all_tongbu == nil
			begin
				@@all_tongbu = Dir.foreach(unitlist_path).map { |path|
					if path =~ /\.xml$/
						File.open(File.expand_path(path, unitlist_path), 'r:bom|utf-8') do |f|
							Hash.from_xml(f.read)['unitlist']
						end
					end
				}.select{ |e| e != nil }
			rescue
				@@all_tongbu = []
			end
		end

		@@all_tongbu
	end

	def Clearsky.all_books
		if @@all_books == nil
			begin
				File.open(books_list_path, 'r:bom|utf-8') do |f|
					@@all_books = Hash.from_xml(f.read)['booklist']['book'].map do |b|
			      {
			        :id => b['id'],
			        :isfree => b['isfree'] != 'n',
			        :priority => b['priority'].to_i,
			        :oriprice => b['oriprice'].to_f,
			        :price => b['price'].to_f,
			        :name => b['name'],
			        :desc => b['desc'],
			        :pic => b['pic'],
			        :exam_ids => b['exam'].split(','),
			        :exams => b['exam'].split(',').map {|exam_id|
			        	all_resources[exam_id]
			        }.select { |exam| exam != nil },
			        :free_exams => b['freeexam'].split(','),
			        :grade => (Courseware::Grades.select{|g| g[1] == b['grade'].to_i * 10}.first || Courseware.Grades.first)
			      }
			    end
				end
			rescue
				@@all_books = []
			end
		end

		@@all_books
	end

	def Clearsky.all_resources
		if @@all_resources == nil
			begin
				@@all_resources = {}
				Dir.glob(File.join(xml_root, '**', "index.xml")) do |p|
					File.open p, 'r:bom|utf-8' do |f|
						begin
							Hash.from_xml(f.read)['allresource']['resource'].each do |r|
								@@all_resources[r['id']] = {
									:id => r['id'],
									:name => r['name'],
									:length => r['length'].to_i,
									:difficulty => r['difficulty'],
									:priority => r['priority']
								}
							end
						rescue
							# skip exceptionn
						end
					end
				end
			rescue
				@@all_resources = {}
			end
		end

		@@all_resources
	end

	@@all_exps = nil
	def Clearsky.all_exps
		if @@all_exps == nil
			begin
				@@all_exps = {}
				Dir.glob(File.join(explain_xml_root, "*.xml")) do |p|

					quesId = p[/[\d_]+(?=\.xml$)/]
					@@all_exps[quesId] = "http://www.51kehou.com:8080/data/explain_mp3/#{quesId}.mp3"
				end
			rescue
				@@all_exps = {}
			end
		end

		@@all_exps
	end

	@@all_exams = {}
	def Clearsky.get_exam(exam_id)
		if @@all_exams[exam_id] == nil
			File.open get_xml_path(exam_id), 'r:bom|utf-8' do |f|
				exam = Hash.from_xml(f.read.gsub(/<p n='([^']+)'>([^<]+)<\/p>/, %q{<p n='\1'><v>\2</v></p>}))
				fill_exp_info_to_question(exam)
				@@all_exams[exam_id] = {
					'exam' => exam['newenglishexam'].instance_eval { |e|
						{
							'id' => e['id'],
							'parts' => (if e['part'].class == Array; e['part']; else [ e['part'] ]; end ).map { |p|
								{
									'num' => p['num'].to_i,
									'type' => p['type'],
									'sound' => p['sound'],
									'd' => p['d'],
									'sections' => (if p['section'].class == Array; p['section']; else [ p['section'] ]; end ).map { |s|
										{
											'num' => s['num'].to_i,
											'questions' => (if s['question'].class == Array; s['question']; else [ s['question'] ]; end ).map { |q|
												{
													'id' => "#{q['id']}_#{q['num']}",
													'num' => q['num'].to_i,
													'partType' => q['partType'],
													'score' => q['score'].to_f,
													'type' => q['type'],
													'option' => q['option'].to_i,
													'image1' => q['image1'],
													'image2' => q['image2'],
													'answer' => q['answer'],
													'explain' => q['explain']
												}
											}
										}
									}
								}
							}
						}
					}
				}
      end
		end
		@@all_exams[exam_id]
	end

	def Clearsky.get_tongbu_url(grade, unit_no)
		class_path = '/clearsky-data/xml/tongbu/class'
		if grade.to_s[-1] == '5'
			name = "gradex#{grade.to_i / 10}_unit#{unit_no}.xml"
		else
			name = "grade#{grade.to_i / 10}_unit#{unit_no}.xml"
		end
		File.join class_path, name
	end

	@@xml_path_map = {}
	def Clearsky.get_xml_path(id)
		if @@xml_path_map[id] == nil
			@@xml_path_map[id] = Dir.glob(File.join(xml_root, '**', "#{id}.xml")).first
		end
		@@xml_path_map[id]
	end

  # grade_range 范围为 30...130
	def Clearsky.get_tongbu_by_grade_range grade_range
		Clearsky.all_tongbu.select { |x| grade_range.include? x['grade'].to_f * 10 }
	end

	# books 是指练习册
	# grade_range 范围为 30...130
	def Clearsky.get_books_by_grade_range grade_range
		Clearsky.all_books.select { |x| grade_range.include? x[:grade].last }
	end

	# books 是指练习册
	# grade_range 范围为 30...130
	def Clearsky.get_books_by_grade grade
		Clearsky.all_books.select { |x| x[:grade].last == grade }
	end

	# 根据id获取一个练习册
	def Clearsky.get_book id
		Clearsky.all_books.select { |x| x[:id] == id.to_s }.first
	end

	# 根据年级和关键字搜索练习册
	def Clearsky.get_books_by_grade_range_keyword (grade_range, keyword)
			Clearsky.all_books.select { |x| (grade_range.include? x[:grade].last) and (x[:name].include? keyword) }
	end

  # 根据关键字搜索练习册
	def Clearsky.get_books_by_keyword keyword
		Clearsky.all_books.select { |x| x[:name].include? keyword }
	end

	# 根据exam id获取练习册
	def Clearsky.get_book_by_exam_id id
		Clearsky.all_books.select { |x| x[:exam_ids].to_s.include? id.to_s }.first
	end

private
	def Clearsky.unitlist_path
		File.expand_path('./clearsky-data/xml/tongbu/unitlist', Rails.public_path)
	end

	def Clearsky.books_list_path
		File.expand_path('./clearsky-data/xml/book/booklist.xml', Rails.public_path)
	end

	def Clearsky.xml_root
		xml_root = File.expand_path("./clearsky-data/xml", Rails.public_path)
	end

	def Clearsky.explain_xml_root
		File.expand_path('./clearsky-data/explain/xml', Rails.public_path)
	end

	def Clearsky.fill_exp_info_to_question(exam_hash)
		exam_hash.each do |k,v|
			if k == "question" && v.class == Array
				v.each do |x|
					exp = all_exps[x['id'] + '_' + x['num']]
					if exp
						x['explain'] = exp
					end
				end
			elsif v.class == Hash
				fill_exp_info_to_question v
			end
		end
	end
end
