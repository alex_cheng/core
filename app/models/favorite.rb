# -*- encoding : utf-8 -*-
class Favorite < ActiveRecord::Base
  attr_accessible :book_id, :comment, :user_id

  belongs_to :user

  def book
    if book_id
      Clearsky.get_book book_id
    end
  end
end
