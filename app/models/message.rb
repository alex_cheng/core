# -*- encoding : utf-8 -*-

class Message < ActiveRecord::Base
  # Setup accessible (or protected) attributes for your model
  attr_accessible :head, :content, :tags, :group_id, :posted_by_id, :parent_message_id

  validates :head, :presence => true # 标题不能为空
  validates :group_id, :presence => true # 必须在某个群里发消息

  belongs_to :posted_by, class_name: 'User'
  belongs_to :parent_message, class_name: 'Message'
  belongs_to :group

  has_many :child_messages, class_name: 'Message', foreign_key: 'parent_message_id', dependent: :destroy
end
