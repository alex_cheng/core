# -*- encoding : utf-8 -*-
class Assignment < ActiveRecord::Base
	Status = { :available => 0, :expired => 1, :canceled => 2}
	Available = Status[:available]
	Expired = Status[:expired]
	Canceled = Status[:canceled]
  StatusDisplayNames = { Available => '有效', Expired => '已过期', Canceled => '已撤销' }

  attr_accessible :begin_time, :deadline, :due_time, :practice_id, :assigned_group_id, :comment, :creator_id, :status, :assigned_user_id

  validates :status, :inclusion => {:in => Status.values}

  belongs_to :assigned_group, class_name: "Group"
  belongs_to :assigned_user, class_name: "User"
  belongs_to :creator, class_name: "User"

  has_many :attempts, foreign_key: :assignment_id, class_name: "AssignmentAttempt", dependent: :destroy

  def name
  	practice = Clearsky.all_resources[practice_id]
  	if practice
  		practice[:name]
  	else
  		''
  	end
  end

  def finished_students_ids
    attempts.where(status: AssignmentAttempt::Finished).select(:participant_id).uniq.map(&:participant_id)
  end

  def finished_students
    User.find finished_students_ids
  end

  def unfinished_students_ids
    assigned_group.members.students.map(&:user_id) - finished_students_ids
  end

  def unfinished_students
    User.find(unfinished_students_ids)
  end

  def attempts_by_user_id(user_id)
    attempts.where(participant_id: user_id)
  end
end
