# -*- encoding : utf-8 -*-
class GroupMemberInvitation < ActiveRecord::Base
  attr_accessible :email, :name, :group_id, :user_id, :token, :usertype
  attr_accessor :usertype
  
  belongs_to :group
  belongs_to :user

  
  def member
    user.group_members.find_by_group_id(self.group_id)
  end
    
  validates_presence_of :email, :name, :group
  
  before_create do 
    self.token = SecureRandom.urlsafe_base64(32)
    
    # 关联User，如果User不存在，创建虚拟User
    unless user = User.find_by_email(email)
      user = User.create! virtual: true, email: email, name: name, usertype: usertype,
        password: SecureRandom.urlsafe_base64(32)
    end
    
    self.user_id = user.id
    
    # 创建没有确认加入的member      
    GroupMember.create! group_id: group_id, user_id: user_id, name: name
        
  end
  
  after_create do 
    GroupMailer.invite(self).deliver
  end
end
