# -*- encoding : utf-8 -*-
class GroupMemberRelationship < ActiveRecord::Base
  attr_accessible :child_id, :parent_id, :group_id
  
  belongs_to :child, foreign_key: :child_id, class_name: "GroupMember"
  belongs_to :parent, foreign_key: :parent_id, class_name: "GroupMember"
  
  belongs_to :group
end
