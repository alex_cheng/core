# -*- encoding : utf-8 -*-
class Group < ActiveRecord::Base
  attr_accessible :name, :desc, :avatar, :type, :founder_id
  has_attached_file :avatar, :styles => { :medium => "100x100#", :thumb => "54x54#" }, :default_url => 'face.jpg'
  validates_attachment :avatar, :content_type => { :content_type => /image/ }, :size => { :in => 0..2048.kilobytes }

  validates_presence_of :name, :desc

  has_many :members, class_name: "GroupMember", dependent: :destroy, order: 'joined_at desc, created_at desc'
  has_many :invitations, class_name: "GroupMemberInvitation", dependent: :destroy
  has_many :messages, dependent: :destroy, order: 'updated_at DESC'
  has_many :shared_files, dependent: :destroy, order: 'updated_at DESC'
  has_many :assignments, dependent: :destroy, order: 'updated_at DESC', foreign_key: :assigned_group_id, class_name: "Assignment"

  belongs_to :founder, class_name: "User", foreign_key: :founder_id

  validate :name, :length => {:in => 4..30}

end
