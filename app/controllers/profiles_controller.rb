# -*- encoding : utf-8 -*-
class ProfilesController < ApplicationController
	layout :choose_layout
	before_filter :authenticate_user!, except: [:show]

  # GET /profiles/1
  def show
  	user = User.find(params[:id])
  	if !user
  		render_404
  		return
  	end

    user.create_profile if !user.profile
  	@profile = user.profile

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /profiles/1/edit
  def edit
    @curent_user = current_user
  	user = User.find(params[:id])
  	if !user or user != current_user
  		render :nothing => true, :status => :not_found
  		return
  	end

    user.create_profile if !user.profile
  	@profile = user.profile
  end

  # PUT /profiles/1/update
  def update
  	user = User.find(params[:id])
    if user == current_user
    	if user
    		user.profile.update_attributes(params[:profile])
        redirect_to :action => :show
      else
      	render :nothing => true, :status => :not_found
      end
    else
      redirect_to action: 'show'
    end
  end

  protected
  def choose_layout
    if current_user
      if current_user.usertype == :admin
        "admin"
      elsif current_user.usertype == :teacher
        "teacher"
      elsif current_user.usertype == :student
        "student"
      elsif current_user.usertype == :parent
        "parent"
      end
    else
      "application"
    end
  end
end
