# -*- encoding : utf-8 -*-
class AssignmentAttemptsController < UserOrientedController
  # GET /assignment_attempts
  # GET /assignment_attempts.json
  def index
    @assignment_attempts = AssignmentAttempt.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assignment_attempts }
    end
  end

  # GET /assignment_attempts/1
  def show
    @assignment_attempt = AssignmentAttempt.find(params[:id])
    @assignment = @assignment_attempt.assignment

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /assignment_attempts/new
  def new
    if params[:user_id] == current_user.id.to_s && current_user.assigned_assignments.map(&:id).include?(params[:assignment_id].to_i)
      @assignment = Assignment.find_by_id(params[:assignment_id])
      if @assignment
        @assignment_attempt = AssignmentAttempt.new({
          assignment_id: @assignment.id,
          participant_id: current_user.id,
          status: AssignmentAttempt::InProgress,
          user_answers: {}.to_json,
          score: 0,
          progress: 0
          })

        if @assignment_attempt.save
          redirect_to @assignment_attempt
        else
          redirect_to assignments_path, alert: '因为某种原因无法做题，请与网站管理员联系。'
        end
      else
        flash[:error] = '找不到指定的作业。'
        redirect_to assignments_path
      end
    else
      flash[:error] = '不能帮别人做题。'
      redirect_to assignments_path
    end
  end

  # GET /assignment_attempts/1/edit
  def edit
    @assignment_attempt = AssignmentAttempt.find(params[:id])
  end

  # POST /assignment_attempts
  # POST /assignment_attempts.json
  def create
    @assignment_attempt = AssignmentAttempt.new(params[:assignment_attempt])

    respond_to do |format|
      if @assignment_attempt.save
        format.html { redirect_to @assignment_attempt, notice: 'Assignment attempt was successfully created.' }
        format.json { render json: @assignment_attempt, status: :created, location: @assignment_attempt }
      else
        format.html { render action: "new" }
        format.json { render json: @assignment_attempt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /assignment_attempts/1
  # PUT /assignment_attempts/1.json
  def update
    @assignment_attempt = AssignmentAttempt.find(params[:id])

    respond_to do |format|
      if @assignment_attempt.update_attributes(params[:assignment_attempt])
        format.html { redirect_to @assignment_attempt, notice: 'Assignment attempt was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @assignment_attempt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assignment_attempts/1
  # DELETE /assignment_attempts/1.json
  def destroy
    @assignment_attempt = AssignmentAttempt.find(params[:id])
    @assignment_attempt.destroy

    respond_to do |format|
      format.html { redirect_to assignment_attempts_url }
      format.json { head :no_content }
    end
  end

  # POST /assignment_attempts/1/post_answer.json
  def post_answer
    # 只有学生才能做题
    if current_user.usertype != :student
      head :no_content
      return
    end

    @assignment_attempt = AssignmentAttempt.find(params[:assignment_attempt_id])
    @assignment_attempt.user_answers = params['user_answers']
    state = JSON.parse(params['state'])
    @assignment_attempt.total_count = state['totalCount'].to_i
    @assignment_attempt.progress = state['progress'].to_i
    @assignment_attempt.right_count = state['rightCount'].to_i
    @assignment_attempt.score = state['score'].to_f
    if @assignment_attempt.progress == @assignment_attempt.total_count
      @assignment_attempt.status = AssignmentAttempt::Finished
      @assignment_attempt.finished_time = DateTime.now
    else
      @assignment_attempt.status = AssignmentAttempt::InProgress
    end

    if @assignment_attempt.save
      head :no_content
    else
      head :no_content, :status => 500
    end
  end
end
