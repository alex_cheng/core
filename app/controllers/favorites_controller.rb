# -*- encoding : utf-8 -*-
class FavoritesController < UserOrientedController
  # GET /favorites
  # GET /favorites.json
  def index
    if session[:book_id] != nil
      favorite = Favorite.new user_id: current_user.id, book_id: session[:book_id]
      if current_user.favorites.where(book_id: favorite.book_id).any?
        # do nothing
      else
        favorite.save
      end
      session[:book_id] = nil
    end

    @favorites = current_user.favorites

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @favorites }
    end
  end

  # GET /favorites/1.js
  def show
    @favorite = Favorite.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.js # show.js.erb
    end
  end

  # GET /favorites/new
  # GET /favorites/new.js
  def new
    if params["book_id"] == nil
      render :nothing => true
      return
    end

    @favorite = Favorite.new user_id: current_user.id, book_id: params["book_id"]

    respond_to do |format|
      if current_user.favorites.where(book_id: @favorite.book_id).any?
        format.html do # new.html.erb
          flash[:alert] = "此练习册已经被收藏了，无需重复收藏。"
        end
        format.js { render inline: 'alert("此练习册已经被收藏了，无需重复收藏。");'}
      else
        format.html # new.html.erb
        format.js # new.js.erb
      end
    end
  end

  # GET /favorites/1/edit
  def edit
    render_404
  end

  # POST /favorites.js
  def create
    @favorite = Favorite.new(params[:favorite])
    respond_to do |format|
      if current_user.favorites.where(book_id: @favorite.book_id).any?
        format.js { render inline: 'alert("此练习册已经被收藏了，无需重复收藏。");' }
      else
        if @favorite.save
          format.js
        else
          format.js { render action: "new" }
        end
      end
    end
  end

  # PUT /favorites/1
  # PUT /favorites/1.json
  def update
    @favorite = Favorite.find(params[:id])

    respond_to do |format|
      if @favorite.update_attributes(params[:favorite])
        format.html { redirect_to @favorite, notice: 'Favorite was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @favorite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favorites/1
  # DELETE /favorites/1.js
  def destroy
    @favorite = Favorite.find(params[:id])
    @favorite.destroy

    respond_to do |format|
      format.html { redirect_to favorites_url }
      format.js
    end
  end
end
