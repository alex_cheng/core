# -*- encoding : utf-8 -*-

require File.expand_path('../../models/forum_account.rb', __FILE__)
require File.expand_path('../../helpers/coursewares_helper.rb', __FILE__)

class KehouController < ApplicationController
  include CoursewaresHelper
  include Rails.application.routes.url_helpers
  include SimpleCaptcha::ControllerHelpers

  before_filter :init_common, :init_nav

  NavigateCategories = ["高中英语资料下载", "初中英语资料下载", "小学英语资料下载", "留学英语资料下载"]
  DZ_icons = {'小学' => [2,38,39,45,48],
              '中学' => [40,41,42,43,46],
              '班主任' => [44,47],
              '留学' => [49,50,51,52,53,54,55]}
  

  def execute_sql_on_forum(*sql_array)
    ForumAccount.connection.execute(ActiveRecord::Base.send(:sanitize_sql_array, sql_array))
  end

  def home
    @elementary_school = {
      #tongbu: Clearsky.get_tongbu_by_grade_range(30...70),
      books: Clearsky.get_books_by_grade_range_keyword((30...70), '语法')
    }

    @middle_school = {
      #tongbu: Clearsky.get_tongbu_by_grade_range(70...100),
      books: Clearsky.get_books_by_grade_range_keyword((70...100), '语法')
    }

    @high_school = {
      #tongbu: Clearsky.get_tongbu_by_grade_range(100...130),
      books: Clearsky.get_books_by_grade_range_keyword((100...130), '语法')
    }

    # if request.original_url.to_s =~ /home2$/
    #   render 'home', :layout => false
    # else
    #   render :layout => false
    # end
  end

  def courseware
    @courseware = Courseware.find(params[:id])
    @page_title = @courseware.topic
  end

  def grade
    @publish_index = (Courseware::Publishes.index(params[:publish]) || 0)
    publish = Courseware::Publishes[@publish_index]
    @grade = params[:grade].to_i
    @subtitle = get_subtitle(@grade)
    @page_title = @subtitle
    @number_categories = (1..Courseware::Categories.size).zip(Courseware::Categories.keys)
    @coursewares_by_pub = Courseware::Publishes.collect{|publish| [publish, Courseware.find(:all, :conditions => {:grade => @grade, :publish => publish})]}


    if @grade > 31
      if @grade.to_s[-1] == '1'
        @grade_pre = @grade - 6
      elsif @grade.to_s[-1] == '5'
        @grade_pre = @grade - 4
      else
        @grade_pre = nil
      end
    end

    if @grade < 125
      if @grade.to_s[-1] == '1'
        @grade_nex = @grade + 4
      elsif @grade.to_s[-1] == '5'
        @grade_nex = @grade + 6
      else
        @grade_nex = nil
      end
    end

    @grade_pre_title = get_subtitle(@grade_pre) if @grade_pre != nil
    @grade_nex_title = get_subtitle(@grade_nex) if @grade_nex != nil
  end

  def grade_category
    @publish_index = (Courseware::Publishes.index(params[:publish]) || 0)
    publish = Courseware::Publishes[@publish_index]

    @grade = params[:grade].to_i
    cat_id = params[:category].strip
    @category = Courseware::Categories.select {|k,v| k[1].strip == cat_id}.first # may be nil
    @coursewares_by_pub = Courseware::Publishes.collect{|publish|
      [publish, Courseware.find(:all, :conditions => {:grade => @grade, :publish => publish}).select { |courseware|
      courseware.belong_to_category? cat_id }]
    }
    @subtitle = get_subtitle(@grade) + @category[0][0]
    @page_title = @subtitle
    @number_categories = (1..Courseware::Categories.size).zip(Courseware::Categories.keys)
  end

  def material
    @material = CoursewareMaterial.find(params[:id])
    render :layout => false
  end

  def material_download
    if Rails.env.test? || simple_captcha_valid?
      material = CoursewareMaterial.find(params[:id])
      if material
        redirect_to material.upload.url(:original)
      else
        flash[:error] = "文件不存在"
        redirect_to :action => :material, :id => params[:id]
      end
    else
      flash[:error] = "验证码不正确"
      redirect_to :action => :material, :id => params[:id]
    end
  end

  def search
    keywords = params[:keywords].split(/[,\s]/).map {|x| '%' + x + '%'}
    if keywords.size > 0
      cond_clause_and = (["(topic LIKE ? OR keywords LIKE ?)"] * keywords.size).join(' AND ')
      cond_clause_or = (["(topic LIKE ? OR keywords LIKE ?)"] * keywords.size).join(' OR ')
      @coursewares = Courseware.find(:all, :limit => 100, :conditions => [" visibility='public' AND #{cond_clause_and}"] + keywords.zip(keywords).flatten)
      coursewared_ids = @coursewares.map {|x| x.id}
      @coursewares = @coursewares + Courseware.find(:all, :limit => 100,
        :conditions => [" visibility='public' AND #{cond_clause_or} AND id NOT IN (?) "] + keywords.zip(keywords).flatten + [coursewared_ids])

      logger.debug "#{@coursewares.size} records are retrieved."
      @subtitle = %Q{搜索: "#{params[:keywords]}", 共 #{@coursewares.count} 条记录}
      @page_title = @subtitle
    else
      redirect_to :action => :home
    end
  end

  def about
    @subtitle = '关于51课后'
  end

  def comments
    @subtitle = '投诉与建议'
  end

  def contacts
    @subtitle = '联系我们'
  end

  def signed_in_forum
    session.delete :require_sign_in_forum
    session.delete :password
    render json: ["okay"]
  end

  def logout_redirect
    render :layout => false
  end

  def tongbu

  end

  private
  def init_common
    @page_title = ('首页' if params[:action] == 'home')
    @extra_desc = ""

    if session[:suggested_courseware_ids] == nil
      session[:suggested_courseware_ids] = (1..30).collect{
        Courseware.first(:offset => rand(Courseware.count)) }.select{|c|
          c != nil and c.visibility == 'public' and c.topic.include?('英语')
        }.first(10).collect{|c|
          c.id }
    end

    @coursewares_suggested = Courseware.find(session[:suggested_courseware_ids])
    @forum_update = nil
    @star_users = nil
    @classified_navigate = { "小学英语" => 0..69, "初中英语" => 70..99, "高中英语" => 100..999 }.collect { |category, range|
      [category, Courseware::Grades.select{|grade| range.include?(grade[1]) && grade[1].odd? }.collect { |grade|
        grade
      }]
    }
    @actived_user = get_forum("activest_user")
    @recommended_collect = get_forum("recommends")
    @listen_materials = Courseware.where("visibility='public' AND keywords like ?", "%听力%").first(15)
    @class_adviser_materials = Courseware.where("visibility='public' AND keywords like ?", "%班主任%").first(15)

  end

  def init_nav
    home_item = ['51课后', root_path]
    action = params[:action]
    @nav_path = (if action == 'home' or action == 'search'
        [home_item]
      elsif action == 'grade' or action == 'grade_category'
        grade = params[:grade]
        subtitle = get_subtitle(grade)
        grade_item = [subtitle, url_for(:action => :grade, :grade => grade)]
        if action == 'grade'
          [home_item, grade_item]
        else
          cat_id = params[:category]
          category = Courseware::Categories.select {|k,v| k[1].strip == cat_id}.first # may be nil
          cat_item = [category[0][0], url_for(:action => :grade_category, :grade => grade, :category => cat_id)]
          [home_item, grade_item, cat_item]
        end
      elsif action == 'courseware'
        courseware = Courseware.find(params[:id])

        grade_item = [get_subtitle(courseware.grade), url_for(:action => :grade, :grade => courseware.grade)]
        courseware_item = [courseware.topic, url_for(:action => :courseware, :id => courseware.id)]
        [home_item, grade_item, courseware_item]
      elsif action == 'about'
        [['关于51课后', about_path]]
      elsif action == 'comments'
        [['投诉与建议', comments_path]]
      elsif action == 'contacts'
        [['联系我们', contacts_path]]
      end)
    
    @material_navigate = { "小学英语资料下载" => 0..69, "初中英语资料下载" => 70..99, "高中英语资料下载" => 100..199, "留学英语资料下载" => 200..299 }.collect { |category, r|
      [category, Courseware::Grades.select{|grade| r.include?(grade[1]) && grade[1].odd? }.collect { |grade|
        [grade[0], Courseware::Publishes.collect {|p| [p, { :action => :grade, :grade => grade[1], :publish => p }]}]
      }]
    }
    @latest_forum_left, @latest_forum_right = format_form_data(get_forum("latest"))
    @topped_forum_left, @topped_forum_right = format_form_data(get_forum("topped"))

    @recomm_school = {
      #tongbu: Clearsky.get_tongbu_by_grade_range(30...130),
      books: Clearsky.get_books_by_grade_range(30...130).sort_by{|x| -x[:grade][1]}
    }
  end

  def get_forum(type)
    if type == "topped"
      rows = execute_sql_on_forum("SELECT tid, subject, author, lastpost, fid FROM `ultrax`.`pre_forum_thread` WHERE digest=1 order by lastpost desc limit 0, 20;")
    elsif type == "latest"
      rows = execute_sql_on_forum("SELECT tid, subject, author, dateline, fid FROM `ultrax`.`pre_forum_thread` order by lastpost desc limit 0, 20;")
    elsif type == "newest_user"
      rows = execute_sql_on_forum("SELECT uid, username, email FROM `ultrax`.`pre_common_member` WHERE adminid=0 order by regdate desc limit 0, 10;")
    elsif type == "recommends"
      rows = execute_sql_on_forum(%q{SELECT ctid, name FROM `ultrax`.`pre_forum_collection` order by lastposttime desc limit 0, 10;})
    elsif type == "activest_user"
      rows = execute_sql_on_forum("SELECT uid, username, email FROM `ultrax`.`pre_common_member` WHERE adminid=0 order by credits desc limit 0, 10;")
    end
    #rows = []
    return rows
  end

  def format_form_data(data)
    _forum_left, _forum_right = [], []
    i = 0
    data.each do |f|
      if i < 10
        _forum_left.push(f)
      else
        _forum_right.push(f)
      end
      i += 1
    end
    return _forum_left, _forum_right
  end

  def get_subtitle(grade)
    grade_name = trans_grade(grade.to_i)
    if grade_name != '不限'
      grade_name[0, grade_name.size - 1] + '英语' + grade_name[-1]
    else
      '不限年级'
    end
  end
end
