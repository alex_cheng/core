# -*- encoding : utf-8 -*-
class GroupsController < UserOrientedController
  before_filter :check_group!

  def index
    @groups = current_user.groups
  end

  def show
    @group = current_user.groups.find(params[:id])
    @messages = @group.messages
    @shared_files = @group.shared_files
  end

  def new
    @group = Group.new founder_id: current_user.id
  end

  def create
    @group = Group.new(params[:group])
    @group.founder_id = current_user.id

    @group.members.build role: :founder, user_id: current_user.id, joined_at: Time.now

    if @group.save
      redirect_to groups_path, :notice => "Successfully created teacher/group."
    else
      render :action => 'new'
    end
  end

  def edit
    @group = current_user.groups.find(params[:id])
  end

  def update
    @group = current_user.groups.find(params[:id])
    if @group.update_attributes(params[:group])
      redirect_to @group, :notice  => "Successfully updated teacher/group."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @group = current_user.groups.find(params[:id])
    @group.destroy
    redirect_to groups_path({user_id: current_user.id}), :notice => "Successfully destroyed teacher/group."
  end
end
