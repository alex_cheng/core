# -*- encoding : utf-8 -*-
class SharedFilesController < ApplicationController
  before_filter :authenticate_user!, :check_group!

  # GET /groups/1/shared_files
  # GET /groups/1/shared_files.json
  def index
    group = Group.find_by_id params[:group_id]
    @shared_files = group.shared_files

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @shared_files }
    end
  end

  # GET /groups/1/shared_files/1
  # GET /groups/1/shared_files/1.js
  def show
    @shared_file = SharedFile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.js # show.js.erb
    end
  end

  # GET /groups/1/shared_files/new
  # GET /groups/1/shared_files/new.js
  def new
    @group = Group.find(params[:group_id])
    @shared_file = SharedFile.new
    @shared_file.shared_by_id = current_user.id
    @shared_file.group_id = params[:group_id]

    respond_to do |format|
      format.html # new.html.erb
      format.js # new.js.erb
    end
  end

  # GET /groups/1/shared_files/1/edit
  def edit
    @shared_file = SharedFile.find(params[:id])
  end

  # POST /groups/1/shared_files
  # POST /groups/1/shared_files.js
  def create
    @shared_file = SharedFile.new(params[:shared_file])

    respond_to do |format|
      if @shared_file.save
        format.html { redirect_to url_for(action: :show, group_id: @shared_file.group_id, id: @shared_file.id), notice: '共享文件已经成功上传。' }
        format.js
      else
        format.html { render action: "new" }
        format.js { render action: "new" }
      end
    end
  end

  # PUT /groups/1/shared_files/1
  # PUT /groups/1/shared_files/1.json
  def update
    @shared_file = SharedFile.find(params[:id])

    respond_to do |format|
      if @shared_file.update_attributes(params[:shared_file])
        format.html { redirect_to @shared_file, notice: '共享文件已经成功更新。' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render action: "edit" }
      end
    end
  end

  # DELETE /groups/1/shared_files/1
  # DELETE /groups/1/shared_files/1.js
  def destroy
    @shared_file = SharedFile.find(params[:id])
    @shared_file.destroy

    respond_to do |format|
      format.html { redirect_to shared_files_url }
      format.js
    end
  end
end
