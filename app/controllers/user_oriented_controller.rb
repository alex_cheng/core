# -*- encoding : utf-8 -*-

# 表示面向用户的controller。
class UserOrientedController < ApplicationController
  before_filter :authenticate_user!, :check_user_permission
  layout :choose_layout_by_user_role

  protected
  def choose_layout_by_user_role
    if current_user.usertype == :admin
      "admin"
    elsif current_user.usertype == :teacher
      "teacher"
    elsif current_user.usertype == :student
      "student"
    elsif current_user.usertype == :parent
      "parent"
    end
  end

  def check_user_permission
    if current_user
      if params[:user_id] and params[:user_id] != current_user.id.to_s
        render_404
      end
    end
  end

end
