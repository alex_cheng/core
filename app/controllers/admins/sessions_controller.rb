# -*- encoding : utf-8 -*-
class Admins::SessionsController < Devise::SessionsController
  skip_before_filter :require_no_authentication, :only => [:new]

  include SimpleCaptcha::ControllerHelpers
  
  def create 
    if simple_captcha_valid?
      super
    else
      build_resource
      clean_up_passwords(resource)
      flash[:error] = "验证码不正确"
      respond_with_navigational(resource) { render :new }
    end
  end  

  def after_sign_out_path_for(resource_or_scope)
    new_admin_session_path
  end
  
  def after_sign_in_path_for(resource_or_scope)
    admin_path
  end    

end
