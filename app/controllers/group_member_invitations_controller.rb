# -*- encoding : utf-8 -*-
class GroupMemberInvitationsController < ApplicationController
  include SimpleCaptcha::ControllerHelpers
  
  # 如果邀请函不存在或者已经被使用过，则跳转
  def redirect_if_invitation_invalid token
    invitation = GroupMemberInvitation.find_by_token(token)
    if !invitation or invitation.member.joined_at
      # 可跳转到一个提示邀请函无效的页面
      redirect_to root_path
      nil
    else
      invitation
    end
  end
  
  # 通过邮件邀请加入
  def join
    return unless invitation = redirect_if_invitation_invalid(params[:token])
    
    user = invitation.user
    if user.virtual?
      redirect_to action: "regularize", token: params[:token]
    else
      sign_in user, :bypass => true
      invitation.member.update_attribute :joined_at, Time.now
      
      # TODO 跳转到小组页面
      redirect_to root_path
    end
  end
  
  def regularize
    return unless invitation = redirect_if_invitation_invalid(params[:token])
    
    @user = invitation.user
    @user.name = ""
  end
  
  def active
    return unless invitation = redirect_if_invitation_invalid(params[:token])
    
    @user = invitation.user
    @user.assign_attributes params[:user]
    @user.virtual = false
    unless simple_captcha_valid?
      flash[:error] = "验证码不正确"
      return render action: 'regularize'
    end
    if @user.save
      sign_in @user, :bypass => true
         
      invitation.member.update_attribute :joined_at, Time.now
      
      session[:group_member_invitation] = nil
      
      # TODO 跳转到当前小组的主页
      # invication = session[:group_member_invitation]
      # group = invication.group
      redirect_to root_path
    else  
      render action: "regularize" 
    end
  end
end
