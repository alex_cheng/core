# -*- encoding : utf-8 -*-
class StudentController < ApplicationController
  before_filter :authenticate_user!

  def home
  	redirect_to favorites_path(user_id: current_user.id)
  end
end
