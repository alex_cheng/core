# -*- encoding : utf-8 -*-
require 'date'

class ClearskyController < ApplicationController
  layout :choose_layout
  before_filter :init_nav_guide

  def index_grade
  end

  def index_all
    @tongbus = Clearsky.all_tongbu.map do |t|
      grade = t['grade'].to_f * 10
      grade += 1 if grade % 10 == 0

      # generate a Hash table.
      {
        :grade =>
          (Courseware::Grades.select{|g| g[1] == grade}.first || Courseware.Grades.first),
        :units =>
          t['unit'].select{|u| u['status'] == 't'}
            .map{|u|
              {
                :no => u['num'].to_i,
                :name => u['name'],
                :pic => u['pic'],
                :price => u['price'].to_f,
                :pricedesc => u['pricedesc']
              }
          }
      }
    end

    @books = Clearsky.all_books
  end

  def do_tongbu
    @tongbu_url = Clearsky.get_tongbu_url(params[:grade], params[:unit])
    @grade_name = Courseware::Grades.select{ |g| g.last == params[:grade].to_i }.first.first
    @nav_path.pop
    @nav_path << [@grade_name, "/clearsky/tongbu/#{params[:grade]}"]
    @nav_path << ['加载中。。。', '']
  end

  # this action is for compatibility of old system.
  def displayxml
    begin
      File.open File.expand_path("./clearsky-data/explain/xml/#{params[:id]}.xml", Rails.public_path), 'r:bom|utf-8' do |f|
        render :inline => f.read
      end
    rescue
      render :nothing => true
    end
  end

  # this action is for getting xml of quiz/exams
  def examxml
    begin
      render :inline => Clearsky.get_exam(params[:id]).to_json
    rescue
      render :nothing => true
    end
  end

  def search
    @search_key = params[:keyword]
    @books = Clearsky.get_books_by_keyword(@search_key)
    @nav_path << [@search_key, ""]
  end

  # GET /clearsky/book/1
  def show_book
    @book = Clearsky.get_book(params[:id])
    if !@book
      render_404
      return
    end

    if @book[:grade]
      @books_r = Clearsky.get_books_by_grade(@book[:grade].last)
    else
      @books_r = []
    end
    @nav_path << [@book[:grade][0], "/clearsky/books/#{@book[:grade][1]}"]
    @nav_path << [@book[:name], ""]
  end

  # GET /clearsky/books/30
  def show_book_by_grade
    grade = (params[:grade].to_i / 10 * 10).to_i
    @books = Clearsky.get_books_by_grade(grade)
    @tongbus = Clearsky.get_tongbu_by_grade_range(grade ... (grade+10))
    @grade_name = Courseware::Grades.select{ |g| g.last == grade }.first.first
    @nav_path << [@grade_name, "/clearsky/book/#{grade}"]
  end

  # GET /clearsky/exam/43001
  def show_practice
    @practice_id = params[:id]
    @book = Clearsky.get_book_by_exam_id(@practice_id)
    if !@book
      render_404
      return
    end

    if @book[:grade]
      @books_r = Clearsky.get_books_by_grade(@book[:grade].last)
    else
      @books_r = []
    end
    @nav_path << [@book[:grade][0], "/clearsky/books/#{@book[:grade][1]}"]
    @nav_path << [@book[:name], "/clearsky/book/#{@book[:id]}"]
    @nav_path << [@book[:exams].select {|x| x[:id].to_s.include? @practice_id.to_s}.first[:name], ""]
  end

  # GET /clearsky/tongbu/1
  def show_tongbu
    grade = (params[:grade].to_i / 10 * 10).to_i
    @books = Clearsky.get_books_by_grade(grade)
    @tongbus = Clearsky.get_tongbu_by_grade_range(grade ... (grade+10))
    @grade_name = Courseware::Grades.select{ |g| g.last == grade }.first.first
    @nav_path.pop
    @nav_path << [@grade_name, '']
  end

  def landing
    #@tongbu_url = Clearsky.get_tongbu_url(params[:grade], params[:unit])
    #render :layout => false
  end

  def init_nav_guide
    @nav_path = [['51课后', '/']]
    @nav_path << ["所有练习", "/clearsky/landing"]
  end

  protected
  def choose_layout
    "clearsky"
  end
end
