# -*- encoding : utf-8 -*-
class AssignmentsController < UserOrientedController
  before_filter :check_user_permission, except: :index

  # GET /assignments
  # GET /assignments.json
  def index
    user = User.find(params[:user_id])
    if user.usertype == :teacher and user == current_user
      @assignments = Assignment.find_all_by_creator_id(params[:user_id]).sort_by{|x| -x[:id]}
      update_assignment_status(@assignments)

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @assignments }
      end
    elsif user.usertype == :student and current_user.id == user.id
      @assignments = current_user.assigned_assignments.sort_by{|x| -x[:id]}
      update_assignment_status(@assignments)

      respond_to do |format|
        format.html { render 'index_student' }
        format.json { render json: @assignments }
      end
    elsif user.usertype == :student and current_user.usertype == :teacher and current_user.students.include?(user)
      @assignments = user.assigned_assignments.select{|x| x.creator == current_user }.sort_by{|x| -x[:id]}
      update_assignment_status(@assignments)

      @head_caption = "#{user.name} 的作业情况"
      respond_to do |format|
        format.html { render 'index_student' }
        format.json { render json: @assignments }
      end
    elsif user.usertype == :parent and user == current_user
      @assignments = user.children_assignments.sort_by{|x| -x[:id]}
      update_assignment_status(@assignments)

      respond_to do |format|
        format.html { render 'index_parent' }
        format.json { render json: @assignments }
      end
    else
      render_404
    end
  end

  # GET /assignments/1
  # GET /assignments/1.json
  def show
    @assignment = Assignment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @assignment }
    end
  end

  # GET /assignments/new/choose_practice
  def choose_practice
    @user = User.find(params[:user_id])
    @favorites = @user.favorites
  end

  # GET /assignments/new
  # GET /assignments/new.js
  def new
    if !params[:practice_id]
      # show all favorite books for users to select one as assignment
      redirect_to assignments_new_choose_path
    else
      if current_user.groups.any?
        @assignment = Assignment.new({
          practice_id: params[:practice_id],
          due_time: DateTime.now + 3,
          begin_time: DateTime.now,
          creator_id: current_user.id,
          assigned_group_id: (if current_user.usertype == :teacher; current_user.groups.first.id else nil end),
          assigned_user_id: (if current_user.usertype == :parent; current_user.groups.map {|g| current_user.child(g.id)}.select {|child| child != nil}.first.id else nil end),
          status: Assignment::Available
          })
        @assignment.deadline = @assignment.due_time

        respond_to do |format|
          format.html # new.html.erb
          format.js # new.html.js
        end
      else
        respond_to do |format|
          format.html { render inline: '必须先创建群组，然后才能布置作业。' }
          format.js { render inline: 'alert("必须先创建群组，然后才能布置作业。");' }
        end
      end
    end
  end

  # GET /assignments/1/edit
  def edit
    render_404
  end

  # POST /assignments
  # POST /assignments.js
  def create
    @assignment = Assignment.new(params[:assignment])
    @assignment.status = Assignment::Available

    respond_to do |format|
      if @assignment.save
        format.html { redirect_to @assignment, notice: '新作业已经布置了。' }
        format.js
      else
        format.html { render action: "new" }
        format.js { render action: "new" }
      end
    end
  end

  # PUT /assignments/1
  # PUT /assignments/1.json
  def update
    @assignment = Assignment.find(params[:id])

    respond_to do |format|
      if @assignment.update_attributes(params[:assignment])
        format.html { redirect_to @assignment, notice: 'Assignment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assignments/1
  # DELETE /assignments/1.json
  def destroy
    @assignment = Assignment.find(params[:id])
    @assignment.destroy

    respond_to do |format|
      format.html { redirect_to assignments_url }
      format.json { head :no_content }
    end
  end

  # POST /assignment/1/post_answer
  def post_answer
    @assignment = Assignment.find(params[:id])
    @assignment.user_answers = params['user_answers']
    state = JSON.parse(params['state'])
    @assignment.total_count = state['totalCount'].to_i
    @assignment.progress = state['progress'].to_i
    @assignment.right_count = state['rightCount'].to_i
    @assignment.score = state['score'].to_f
    if @assignment.progress == @assignment.total_count
      @assignment.status = Assignment::Finished
    end

    if @assignment.save
      head :no_content
    else
      head :no_content, :status => 500
    end
  end

  # GET /assignments/1/finished.js
  def finished_students
    assignment = Assignment.find(params[:assignment_id])
    @students = assignment.finished_students
    @students_scores = {}
    @students.each do |student|
      @students_scores[student.id] = assignment.attempts_by_user_id(student.id).map(&:score).max
    end

    respond_to do |format|
      format.js
      format.html
    end
  end

  # GET /assignments/1/unfinished.js
  def unfinished_students
    assignment = Assignment.find(params[:assignment_id])
    @students = assignment.unfinished_students

    respond_to do |format|
      format.js
      format.html
    end
  end

  def update_assignment_status(assignments)
    n = Time.now
    assignments.each do |assignment|
      if assignment.due_time < n
       assignment.status = Assignment::Expired
      end
    end
  end
end
