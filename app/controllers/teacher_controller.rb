# -*- encoding : utf-8 -*-
class TeacherController < UserOrientedController
  include Rails.application.routes.url_helpers

  before_filter :authenticate_user!

  def home
    #@current_teacher_page = :home
    redirect_to favorites_path(user_id: current_user.id)
  end

  def exams
  end

  def edit

  end
end
