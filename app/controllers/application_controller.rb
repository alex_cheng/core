# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery

  protected
  def render_404
    respond_to do |format|
      format.html { render :template => 'errors/not_found', :layout => 'errors', :status => :not_found }
      format.xml  { head :not_found, :status => :not_found }
      format.any  { head :not_found, :status => :not_found }
    end
  end

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  def check_group!
    if ['show', 'destroy', 'update', 'edit'].include? params[:action]
      if !current_user.groups.map(&:id).include? params[:id].to_i
        render_404
      end
    elsif params[:group_id]
      if !current_user or !(current_user.groups.map(&:id).include?(params[:group_id].to_i))
        render_404
      end
    end
  end
end
