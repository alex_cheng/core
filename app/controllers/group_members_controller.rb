# -*- encoding : utf-8 -*-
class GroupMembersController < UserOrientedController
  def create
    @invitation = GroupMemberVirtualInvitation.new(params[:group_member_virtual_invitation])
    vi = @invitation

    if @invitation.save
      GroupMemberInvitation.transaction do
        child_invitation = GroupMemberInvitation.create email: vi.child_email, name: vi.child_name,
          group_id: vi.group_id, usertype: :student

        # 如果添加了家长，创建相关信息
        if vi.invited_parent then
          # 关联家长User，如果不存在，创建虚拟User
          parent_invitation = GroupMemberInvitation.create email: vi.parent_email, name: "#{vi.child_name}的家长",
            group_id: vi.group_id, usertype: :parent

          # 搭建在此小组中的父子关系
          GroupMemberRelationship.create child_id: child_invitation.member.id, parent_id: parent_invitation.member.id, group_id: vi.group_id
        end
      end

      render json: {}, status: 200
    else
      render json: @invitation.errors, status: :unprocessable_entity
    end
  end

end
