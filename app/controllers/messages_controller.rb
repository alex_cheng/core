# -*- encoding : utf-8 -*-
class MessagesController < ApplicationController
  before_filter :authenticate_user!, :check_message_group!

  # GET /groups/1/messages
  # GET /groups/1/messages.json
  def index
    if !params[:group_id]
      render_404
      return
    end

    group = Group.find_by_id(params[:group_id])
    if !group || !current_user.groups.include?(group)
      render_404
      return
    end

    @messages = group.messages

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @messages }
    end
  end

  # GET /groups/1/messages/1
  # GET /groups/1/messages/1.js
  def show
    @message = Message.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.js # show.js.erb
    end
  end

  # GET /groups/1/messages/new
  # GET /groups/1/messages/new.js
  def new
    @group = Group.find(params[:group_id])
    @message = Message.new posted_by_id: current_user.id, group_id: @group.id

    respond_to do |format|
      format.html # new.html.erb
      format.js
    end
  end

  #GET /groups/1/messages/1/reply
  #GET /groups/1/messages/1/reply.js
  def reply
    parent = Message.find_by_id(params[:message_id])
    if !parent
      render_404
      return
    end

    @group = parent.group
    @message = Message.new({
      posted_by_id: current_user.id,
      group_id: @group.id,
      parent_message_id: parent.id,
      head: "回复： #{parent.head}"
    })

    respond_to do |format|
      format.html { render action: 'new' }
      format.js { render action: 'new' }
    end
  end

  # GET /groups/1/messages/1/edit
  # TODO: GET /groups/1/messages/1/edit.js
  def edit
    @message = Message.find(params[:id])
  end

  # POST /groups/1/messages
  # POST /groups/1/messages.js
  def create
    @message = Message.new(params[:message])
    @group = @message.group

    respond_to do |format|
      if @message.save
        format.html { redirect_to @message, notice: 'Message was successfully created.' }
        format.js
      else
        format.html { render action: "new" }
        format.js { render action: "new" }
      end
    end
  end

  # PUT /groups/1/messages/1
  # PUT /groups/1/messages/1.js
  def update
    @message = Message.find(params[:id])

    respond_to do |format|
      if @message.update_attributes(params[:message])
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.js { render action: "edit" }
      end
    end
  end

  # DELETE /groups/1/messages/1
  # DELETE /groups/1/messages/1.js
  def destroy
    @message = Message.find(params[:id])
    @message.destroy

    respond_to do |format|
      format.html { redirect_to messages_url }
      format.js
    end
  end

  def check_message_group!
    #TODO: check user's permission to messages.
  end
end
