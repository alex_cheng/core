# -*- encoding : utf-8 -*-
class CoursewaresController < UserOrientedController

  include CoursewaresHelper

  before_filter :login_filter

  # GET /coursewares
  # GET /coursewares.json
  def index
    if params[:user_type] == :admin
      @coursewares = Courseware.order('topic').page(params[:page]).per(5)
    else
      @coursewares = Courseware.where(["author_type = 'admin' or author = ? or visibility = 'public'", current_user_or_admin.email]).order('topic').page(params[:page]).per(5)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @coursewares }
    end
  end

  # GET /coursewares/1
  # GET /coursewares/1.json
  def show
    @courseware = Courseware.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @courseware }
    end
  end

  # GET /coursewares/new
  # GET /coursewares/new.json
  def new
    @courseware = Courseware.new
    @courseware.author_type = params[:user_type]
    @courseware.author = current_user_or_admin.email
    session[:editing_courseware_id] = nil

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @courseware }
    end
  end

  # GET /coursewares/1/edit
  def edit
    @courseware = Courseware.find(params[:id])
    session[:editing_courseware_id] = @courseware.id
  end

  # POST /coursewares
  # POST /coursewares.json
  def create
    @courseware = Courseware.new(params[:courseware])

    respond_to do |format|
      if @courseware.save
        CoursewareMaterial.find_all_by_creating_by_who(current_user_or_admin.id).each do |m|
          m.courseware_id = @courseware.id
          m.creating_by_who = nil
          m.save
        end

        format.html { redirect_to courseware_path(@courseware), notice: 'Courseware was successfully created.' }
        format.json { render json: @courseware, status: :created, location: @courseware }
      else
        format.html { render action: "new", user_type: params[:user_type] }
        format.json { render json: @courseware.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /coursewares/1
  # PUT /coursewares/1.json
  def update
    @courseware = Courseware.find(params[:id])

    respond_to do |format|
      if @courseware.update_attributes(params[:courseware])
        format.html { redirect_to courseware_path(@courseware), notice: 'Courseware was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @courseware.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coursewares/1
  # DELETE /coursewares/1.json
  def destroy
    @courseware = Courseware.find(params[:id])
    @courseware.destroy

    respond_to do |format|
      format.html { redirect_to coursewares_path }
      format.json { head :no_content }
    end
  end

protected
  def login_filter
    if params[:user_type] == :admin
      authenticate_admin!
      @current_admin_page = :coursewares
      @user_type = :admin
    elsif params[:user_type] == :teacher
      authenticate_user!
      @current_teacher_page = :coursewares
      @user_type = :teacher
    else
      redirect_to root_path
    end
  end
end
