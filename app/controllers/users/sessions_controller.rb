# -*- encoding : utf-8 -*-
class Users::SessionsController < Devise::SessionsController
  skip_before_filter :require_no_authentication, :only => [:new]
    
  include SimpleCaptcha::ControllerHelpers

  def new
    if params[:book_id]
      session[:book_id] = params[:book_id]
    end
    if current_user
      #redirect_to root_path
      redirect_to person_home_path({user_id: current_user.id})
    else
      super
    end
  end
  
  def create
    if Rails.env.test? || simple_captcha_valid?
      super
      if user_signed_in?
        session[:require_sign_in_forum] = true
        session[:password] = params[:user][:password]
      end
    else
      build_resource
      clean_up_passwords(resource)
      flash[:error] = "验证码不正确"
      respond_with_navigational(resource) { render :new }
    end
  end
  
  def after_sign_out_path_for(resource_or_scope)
    root_path
  end
  
  def after_sign_in_path_for(resource_or_scope)
    person_home_path({user_id: current_user.id})
    #root_path
    # case resource_or_scope.usertype
    #   when 'teacher' ; "/teacher"
    #   when 'student' ; "/student"
    #   when 'parent' ;  "/parent"
    #   else root_path
    # end
  end
  
end
