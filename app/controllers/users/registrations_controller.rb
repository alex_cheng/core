# -*- encoding : utf-8 -*-
class Users::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :require_no_authentication, :only => [:new]
    
  include SimpleCaptcha::ControllerHelpers

  def new
    if current_user
      #redirect_to root_path
      redirect_to person_home_path({user_id: current_user.id})
    else
      super
    end
  end
  
  def create 
    if simple_captcha_valid?
      # 如果用户注册的邮箱已经存在虚拟用户，那么直接对该虚拟用户进行update操作
      if self.resource = User.find_by_email_and_virtual(params[:user][:email], true)
        # 用户已经存在关系，不允许修改usertype
        params[:user][:usertype] = self.resource.usertype
                
        if self.resource.update_with_password params[:user]
          sign_in self.resource, :bypass => true
          redirect_to after_sign_in_path_for self.resource
        else
          clean_up_passwords self.resource
          respond_with_navigational(self.resource) { render "new" }
        end
      else
        super
      end

      if user_signed_in?
        session[:require_sign_in_forum] = true
        session[:password] = params[:user][:password]
      end
    else
      build_resource
      clean_up_passwords(resource)
      flash[:error] = "验证码不正确"
      respond_with_navigational(resource) { render :new }
    end
  end  
end
