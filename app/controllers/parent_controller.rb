# -*- encoding : utf-8 -*-
class ParentController < ApplicationController
  before_filter :authenticate_user!

  def home
  	redirect_to assignments_path(user_id: current_user.id)
  end
end
