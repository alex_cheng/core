# -*- encoding : utf-8 -*-
class AdminController < ApplicationController
  before_filter :authenticate_admin!
  
  def home
    @current_admin_page = :home
  end

  def questions
    @current_admin_page = :questions
  end

  def exams
    @current_admin_page = :exams
  end

  def profile
    @current_admin_page = :profile
  end
end
