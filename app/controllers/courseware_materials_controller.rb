# -*- encoding : utf-8 -*-
class CoursewareMaterialsController < ApplicationController
  before_filter :login_filter

  # GET /courseware_materials
  # GET /courseware_materials.json
  def index
    courseware_id = session[:editing_courseware_id]
    if courseware_id
      @materials = CoursewareMaterial.find_all_by_courseware_id(courseware_id)
    else
      @materials = CoursewareMaterial.find_all_by_creating_by_who(current_user_or_admin_id)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @materials.map{|material| material.to_jq_upload } }
    end
  end

  # GET /courseware_materials/1
  # GET /courseware_materials/1.json
  def show
    @material = CoursewareMaterial.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @material }
    end
  end

  # GET /courseware_materials/new
  # GET /courseware_materials/new.json
  def new
    @material = CoursewareMaterial.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @material }
    end
  end

  # GET /courseware_materials/1/edit
  def edit
    @material = CoursewareMaterial.find(params[:id])
  end

  # POST /courseware_materials
  # POST /courseware_materials.json
  def create
    @material = CoursewareMaterial.new(params[:courseware_material])
    courseware_id = session[:editing_courseware_id]
    if courseware_id
      @material.courseware_id = courseware_id
    else
      @material.creating_by_who = current_user_or_admin_id
    end

    respond_to do |format|
      if @material.save
        format.html {
          render :json => [@material.to_jq_upload].to_json,
          :content_type => 'text/html',
          :layout => false
        }
        format.json { render json: [@material.to_jq_upload].to_json, status: :created, location: @material }
      else
        format.html { render action: "new" }
        format.json { render json: @material.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /courseware_materials/1
  # PUT /courseware_materials/1.json
  def update
    @material = CoursewareMaterial.find(params[:id])

    respond_to do |format|
      if @material.update_attributes(params[:courseware_material])
        format.html { redirect_to @material, notice: 'Courseware material was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @material.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courseware_materials/1
  # DELETE /courseware_materials/1.json
  def destroy
    @material = CoursewareMaterial.find(params[:id])
    @material.destroy

    respond_to do |format|
      format.html { redirect_to courseware_materials_url }
      format.json { head :no_content }
    end
  end

protected
  def login_filter
    if user_signed_in? || admin_signed_in?
      true
    else
      redirect_to root_path
    end
  end

private
  def current_user_or_admin_id
    (current_admin.id if admin_signed_in?) || (current_user.id if user_signed_in?)
  end

end
