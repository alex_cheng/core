# -*- encoding : utf-8 -*-
class GroupMailer < ActionMailer::Base
  default from: "test@51kehou.com"
  
  def invite invitation
    @invitation = invitation
    mail(to: invitation.email, subject: "#{invitation.group.founder.name}老师邀请你加入小组: #{invitation.group.name}")
  end
  
end
