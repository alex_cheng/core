/**
 * @author Alex Cheng
 *
 * Practice Engine provides users with capability that they could answer questions, get scors and
 * review the answers they gave and the standard answers.
 *
 * It requires jQuery framework.
 */

// Mode has two values: 1. play; 2. review
g_mode = 'play';

// A class representing a quesiton entity.
function Question(jsonQuesData, sectionNum) {
    var self = this;
    this.id = jsonQuesData.id;
    this.number = jsonQuesData.num;
    this.content = jsonQuesData.image1;
    this.revise = jsonQuesData.image2;
    this.partType = jsonQuesData.partType;
    this.score = jsonQuesData.score;
    this.type = jsonQuesData.type;
    if(this.type === "radio") {
        this.optionCount = jsonQuesData.option;
    }
    this.answer = jsonQuesData.answer;
    this.section = sectionNum;

    if(jsonQuesData.explain != null) {
        self.expAudio = jsonQuesData.explain.replace(':8080', '');
        var linesUrl = "/clearsky-data/explain/xml/" + jsonQuesData.id + ".xml";
        $.ajax({
            url : linesUrl,
            dataType : "xml",
            success : function(data) {
                self.expDraws = []
                var elements = data.documentElement.getElementsByTagName("l");
                $(elements).each(function() {
                    self.expDraws.push({
                        t : this.getAttribute("t"),
                        x : this.getAttribute("x"),
                        y : this.getAttribute("y"),
                        points : $(this).text().split(",")
                    });
                });
            }
        });
    }

    this.isRadio = function() {
        return this.type === "radio";
    }

    this.isText = function() {
        return this.type === "text";
    }

    this.isMultiLineText = function() {
        return this.type === "textarea";
    }

    this.isListeningType = function() {
        return this.partType == "listening";
    }

    this.hasAnswered = function() {
        return typeof this.userAnswer != "undefined";
    }

    this.isAnswerRight = function() {
        var _this = this;

        function trimSpaceNearComma(s) {
            return $.map(s.split(","), function(e) { return e.trim() }).join(",");
        }

        function checkAnswer(expected, actual) {
            if($.isArray(expected)) {
                for(var i = 0; i < expected.length; ++i) {
                    if( checkAnswer(expected[i], actual) ) {
                        return true;
                    }
                }
                return false;
            }
            else {
                if(expected == actual) {
                    return true;
                }
                else if(_this.isText && trimSpaceNearComma(expected) == trimSpaceNearComma(actual)) {
                    return true;
                }
                else if(expected.length == 1 && expected.toUpperCase() == actual.toUpperCase()) {
                    return true;
                }
                return false;
            }
        }

        return checkAnswer(_this.answer, _this.userAnswer)
    }

    this.hasExplanation = function() {
        return typeof this.expAudio != "undefined";
    }

    // BEGIN - Events handlers ListenPlayer.
    this.onShown = function() {
        if(g_mode == "play") {
            if(typeof this.listening != "undefined") {
                this.listening.play();
            }
        }
        else {
            if(typeof this.listening != "undefined") {
                this.listening.play();
            }
            else{
                CoreObjects.$ListenPlayer.jPlayer("pause");
            }
        }
    }

    this.onRevised = function(ctx) {
        if(typeof this.listening != "undefined") {
            this.listening.revise();
        }
    }

    this.onPaused = function() {
        if(typeof this.listening != "undefined") {
            this.listening.pause();
        }
    }

    this.onResumed = function() {
        if(typeof this.listening != "undefined") {
            this.listening.resume();
        }
    }
    // END - Event handlers
}


function Listening(src, begin, end) {
    this.src = src;
    this.begin = begin;
    this.end = end;

    this.play = function() {
        var $player = CoreObjects.$ListenPlayer;
        if(g_mode == "play") {
            if($player.data("jPlayer").status.src == '') {
                $player.jPlayer("setMedia", {
                    mp3: this.src
                });
                $player.jPlayer("play", 0);
            }
        }
        else {
            if(typeof this.begin != 'undefined' && typeof this.end != 'undefined') {
                $player.jPlayer("setMedia", {
                    mp3: this.src
                });
                $player.data("beginTime", this.begin);
                if(this.end != Number.MAX_VALUE) {
                    $player.data("endTime", this.end);
                }
                else {
                    $player.removeData("endTime");
                }
                $player.jPlayer("play", this.begin);
            }
            else {
                // As no begin time and end time defined, don't interrupt current playing.
                if($player.data("jPlayer").status.src == '') {
                    $player.jPlayer("setMedia", {
                        mp3: this.src
                    });
                    $player.jPlayer("play");
                }
                else if($player.data("jPlayer").status.paused) {
                    $player.jPlayer("play");
                }
            }
        }
    }

    this.pause = function() {
        CoreObjects.$ListenPlayer.jPlayer("pause");
    }

    this.resume = function() {
        CoreObjects.$ListenPlayer.jPlayer("play");
    }

    this.revise = function() {
        var $revPlayer = CoreObjects.$ListenPlayer;
        $revPlayer.removeData("onEnded");

        if(typeof this.begin != 'undefined' && typeof this.end != 'undefined') {

            $revPlayer.jPlayer("setMedia", {
                mp3: this.src
            });

            $revPlayer.data("beginTime", this.begin);
            $revPlayer.data("endTime", this.end);
            $revPlayer.jPlayer("play", this.begin);
        }
        else {
            // As no begin time and end time defined, don't interrupt current playing.
            if($revPlayer.data("jPlayer").status.src == '') {
                $revPlayer.jPlayer("setMedia", {
                    mp3: this.src
                });
                $revPlayer.jPlayer("play");
            }
            else if($revPlayer.data("jPlayer").status.paused) {
                $revPlayer.jPlayer("play");
            }
        }
    }
}
// This class is view-model of PracticeEngineUI, representing abstrac UI of Practice Engine. It acts as
// bridge between models and UIs.
//
// It accept a HTML object 'view' as parameter. The parameter refers to HTML element where Practice Engine UI
// is defined. It must contain HTML elmements defined with following IDs:
// - name
// - time
// - continuePause
// - submit
// - progress
// - question
// - questionNav
// - answerSelector
// - answerTextbox
// - nextButton
// - previousButton
// - answerForRevise
// - score
// - totalScore
// - date
// - practice_part_types
// - practice_sections
// - callbacks = {
//    "submitted",
//    ""
// - statPanel
//}
function PracticeEngineViewModel(view, callbacks) {
    var $view = $(view);
    var $name = $view.find("#name");
    var $time = $view.find("#time");
    var $continuePause = $view.find("#continuePause");
    var $progress = $view.find("#progress");
    $progress.children(".bar").progressbar({
        value: 0
    });
    var $question = $view.find("#question");
    var $questionNav = $view.find("#questionNav");
    var $answerSelector = $view.find("#answerSelector");
    var $answerTextbox = $view.find("#answerTextbox");
    var $writingTextBox = $view.find("#writingTextbox");
    var $expHelpDiv = $view.find("#help_explanation");
    $answerTextbox.empty();
    $answerTextbox.html("答案：").append($("<input type=text>").keypress(function (event) {
            if(event.which == 13) {
                  if(typeof submitCallback !== "undefined") {
                      submitCallback(quesIndex, this.value);
                      this.value = "";
                  }
            }
        })
    );
    var $nextButton = $view.find("#nextButton");
    var $previousButton = $view.find("#previousButton");
    $nextButton.text("提交并到下一题").click(function() {
        if(questions[quesIndex-1].hasAnswered()) {
            context.moveToNext();
        }
        else {
            submitCallback(quesIndex, getUserAnswer());
        }
    });
    $previousButton.text("上一题").click(function() {
        context.moveToLast();
    });


    var $answerForRevise = $view.find("#answerForRevise");
    var $score = $view.find("#score");
    var $totalScore = $view.find("#totalScore");
    var $date = $view.find("#date");
    var $parts = $view.find("#practice_part_types");
    var $sections = $view.find("#practice_sections");

    var $reviseHeader = $view.find("#reviseHeader");
    var $examHeader = $view.find("#examHeader");

    var $statPanel = $view.find("#statPanel");
    $statPanel.children(".close").click(function() {
        $statPanel.hide();
        $view.find(".maskdiv").hide();
    });
    $statPanel.find(".statPanelClose").click(function() {
        $statPanel.hide();
        $view.find(".maskdiv").hide();
        $questionNav.children().eq(0).click();
    });

    var $expCanvas = $view.find("#expCanvas");
    var $expDiv = $view.find("#explanationDiv");
    var $listenDiv = $view.find("#listeningDiv");

    // listen to explanation player's event.
    CoreObjects.$ExpPlayer.bind($.jPlayer.event.timeupdate, function(event) {
        if(typeof currentQues == 'undefined' || currentQues == null)
            return;

        if(typeof currentQues.expDraws == 'undefined' || currentQues.expDraws == null)
            return;

        var canvas = $expCanvas[0];
        var ctx = canvas.getContext('2d');

        var lines = currentQues.expDraws;
        var current = event.jPlayer.status.currentTime * 10;

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        ctx.beginPath();
        ctx.strokeStyle = "rgb(200, 20, 20)";
        ctx.lineWidth = 1;

        // for testing the canvas.
        // ctx.moveTo(0, 0);
        // ctx.lineTo(canvas.width, canvas.height);
        // ctx.moveTo(canvas.width, 0);
        // ctx.lineTo(0, canvas.height);

        for(var i = 0; i < lines.length; i++) {
            var line = lines[i];
            if(current >= line.t) {
                ctx.moveTo(line.x * 1.0, line.y * 1.0);
                var v =  Math.min(line.points.length, Math.round( current - line.t)) - 1;
                ctx.lineTo(line.points[v] * 1.0, line.y * 1.0);
                break;
            } else {
                break;
            }
        }

        ctx.stroke();
    });

    var $timerText = $view.find("#timer");

    var timerId, elapsed = 0; // unit is second, not millisecond.

    function setPracticeTimer() {
        var start = new Date();
        timerId = window.setInterval(function () {
            elapsed ++;
            var obj = new Date(elapsed * 1000);
            // TODO: the open-source string-format lib has a bug. It wrongly produces string "0:0:0" instead "00:00:00".
            // Need to fix the lib or replace it.
            $timerText.text("%02d:%02d:%02d".format(obj.getUTCHours(), obj.getUTCMinutes(), obj.getUTCSeconds()));
        }, 1000);
    }
    $timerText.text("00:00:00");

    setPracticeTimer();
    $continuePause.addClass("pause");
    $continuePause.text("暂停答题");

    $continuePause.click(function() {
        var question = questions[quesIndex-1];
        if($(this).hasClass("pause")) {
            window.clearInterval(timerId);
            $(this).removeClass("pause").addClass("resume");
            $continuePause.text("继续答题");
            question.onPaused();
        }
        else {
            setPracticeTimer();
            $(this).removeClass("resume").addClass("pause");
            $continuePause.text("暂停答题");
            question.onResumed();
        }
    });


    var questions = [];

    // Parts = {
    //    <part1 name>: { 1: [question1, question2, ...], 2: [questions...]},
    //    <part2 name>: { 1: [question1, question2, ...] }
    // }
    var parts = [];
    var quesIndex = 0;
    var currentQues;
    var context = this;

    if(typeof callbacks !== "undefined") {
        if(typeof callbacks["submitted"] !== "undefined") {
            var submitCallback = callbacks["submitted"];
        }
    }

    this.setName = function(name) {
        $name.text(name);
    }

    // 设置进度。 Parameter 'n' is 0~100.
    this.setProgress = function(n) {
        $progress.children(".bar").progressbar("option", "value", n);
        $progress.children(".text").text("%d%%".format(n));
    }

    // argQuestions is parameter of questions.
    this.setQuestions = function(argQuestions) {
        if(argQuestions.length == 0)
            return;

        parts = categorize(argQuestions);
        questions = parts.getOrderedQuestions();
        parts.fillPartsUI($parts);

        updateQuesNav(questions);
        showQuestion(1);
    }

    // set score.
    this.setScore = function(score) {
        $score.text(score);
        $totalScore.text(score);
    }

    // set score, progress
    this.updateState = function(state) {
        this.setProgress(parseInt(100*state.progress/state.totalCount));
        this.setScore(state.score);
        $date.text((new Date()).toLocaleDateString());
    }

    // set test time.
    this.setTestTime = function(testTime) {
        $time.text(testTime);
    }

    this.moveToIndex = function(index) {
        showQuestion(index);
    }

    this.moveToNext = function() {
        if (quesIndex+1 > questions.length)
        {
            showQuestion(questions.length);
        }

        showQuestion(quesIndex+1);
    }

    this.moveToLast = function() {
        if (quesIndex-1 < 1)
            return;

        showQuestion(quesIndex-1);
    }

    // index is 1-based.
    this.markAsCorrect = function(index) {
        $questionNav.children().eq(index - 1)
            .removeClass("error")
            .addClass("correct");
    }

    // index is 1-based.
    this.markAsError = function(index) {
        $questionNav.children().eq(index - 1)
            .removeClass("correct")
            .addClass("error");
    }

    this.showStats = function(stats) {
        var $table = $statPanel.find("table"), scoreWith100AsFull,
        prompts = ["题目有点难，继续努力！", "还不错，有进步哟，继续努力！", "高分！你真棒，继续努力！"];
        $table.find("tr:not(.summary1,.summary2)").remove();
        for(var type in stats.statsByType) {
            var item = stats.statsByType[type];
            $table.prepend(
                $("<tr></tr>")
                    .append("<th>%s</th>".format(type))
                    .append("<td>共%d题 对%d题 正确率%d%%".format(item.total, item.right, Math.round(item.right * 100 / item.total)))
            );
        }
        scoreWith100AsFull = Math.round(stats.rightCount * 100 / stats.totalCount);
        $table.find(".summary1 td").text("共%d题 正确%d题 正确率%d%%".format(
            stats.totalCount, stats.rightCount, scoreWith100AsFull));
        $table.find(".summary2 td").text("%d分（百分制）".format(stats.score));

        $statPanel.find(".testScore")
            .removeClass("low mid high")
            .addClass(scoreWith100AsFull < 70 ? "low": scoreWith100AsFull < 90 ? "mid" : "high");
        $statPanel.find(".testShare").text(scoreWith100AsFull < 70 ? prompts[0]: scoreWith100AsFull < 90 ? prompts[1] : prompts[2]);
        $statPanel.show();
        $view.find(".maskdiv").show();
        $date.text((new Date()).toLocaleDateString());
        $totalScore.text(stats.score);
    }

    this.close = function() {
        window.clearInterval(timerId);
    }

    // index is from 1 to questions.length.
    function showQuestion(index) {
        //var $expHelpDiv = $view.find("#help_explanation");
        if (index < 1 || index > questions.length)
            return;

        quesIndex = index;
        $questionNav.children().removeClass("on");
        var $theQuesItem = $questionNav.children().eq(index - 1);

        $theQuesItem.addClass("on");
        $questionNav.scrollTo($theQuesItem);

        $question.empty();
        var question = questions[quesIndex-1];
        currentQues = question;

        if (g_mode == "play") {
            $question.append($("<img />").attr("src", question.content));
            $answerForRevise.hide();
            $expDiv.hide();
            $expCanvas.hide();

            if(currentQues.hasAnswered()) {
                $answerSelector.html("您的答案是："+question.userAnswer.replace(/\n/ig, '<br/>'));
                $nextButton.text("提交并到下一题");
            }
            else {
                // Update answer section.
                if(question.isRadio()) {
                    $answerSelector.show();

                    $answerSelector.empty();
                    var options = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
                    for (var i=0; i < question.optionCount; i++) {
                        var $label = $("<label />").append($("<input name=answer type=radio>")
                                .attr("value", options[i])
                                .change(function() {
                                    if(typeof submitCallback !== "undefined") {
                                        submitCallback(quesIndex, this.value);
                                    }
                                }))
                            .append("<span>" + options[i] + "</span> ")
                        $answerSelector.append($label);
                    }

                    $answerTextbox.hide();
                    $writingTextBox.hide();
                }
                else if(question.isText()){
                    $answerSelector.hide();
                    $answerTextbox.show();
                    $answerTextbox.find("input").val("");
                    $writingTextBox.hide();
                }
                else if(question.isMultiLineText()){
                    $answerSelector.hide();
                    $answerTextbox.hide();
                    $writingTextBox.show();
                    $writingTextBox.val("");
                }

                $nextButton.text("提交并到下一题");
            }

            $examHeader.show();
            $reviseHeader.hide();

            CoreObjects.$ExpPlayer.jPlayer("stop");
            CoreObjects.$ExpPlayer.jPlayer("clearMedia");

            $listenDiv.removeClass('revise');
            $listenDiv.addClass('exercise');

            question.onShown();
        }
        else {
            $question.append($("<img />").attr("src", question.revise));
            $expCanvas.show();
            $expCanvas.offset($question.offset());
            $expCanvas.width(700);
            $expCanvas.height(400);
            $expCanvas[0].width = 700;
            $expCanvas[0].height = 400;

            $answerForRevise.show();
            $answerSelector.hide();
            $answerTextbox.hide();
            $writingTextBox.hide();
            $nextButton.text("下一题")
            $answerForRevise.html("您的答案是："+question.userAnswer.replace(/\n/ig, '<br/>'));

            if (question.isAnswerRight()) {
                $answerForRevise.addClass("right");
                $answerForRevise.removeClass("wrong");
            }
            else {
                $answerForRevise.addClass("wrong");
                $answerForRevise.removeClass("right");
            };

            if(question.hasExplanation()) {
                CoreObjects.$ExpPlayer.jPlayer("setMedia", {
                    mp3: question.expAudio
                });

                CoreObjects.$ExpPlayer.data("jPlayer").forceUpdateProgressBar();

                CoreObjects.$ExpPlayer.show();
                if(typeof $expHelpDiv.hasShown === "undefined") {
                    $expHelpDiv.hasShown = true;
                    $expHelpDiv.show();
                }
                $expDiv.show();
            }
            else {
                $expDiv.hide();
                $expHelpDiv.hide();
            }

            $reviseHeader.show();
            $examHeader.hide();

            $listenDiv.removeClass('exercise');
            $listenDiv.addClass('revise');

            question.onRevised();
        };

        if(question.isListeningType()) {
            $listenDiv.show();
        }
        else {
            $listenDiv.hide();
        }

        // Update next and previous buttions.
        if(index == questions.length) {
            if (typeof question.userAnswer === "undefined") {
                $nextButton.text("提交");
            }
            else {
                $nextButton.addClass("disabled");
                $nextButton.attr("disabled", true);
            }
        }
        else {
            $nextButton.removeClass("disabled");
            $nextButton.attr("disabled", false);
        }

        if(index == 1) {
            $previousButton.addClass("disabled");
            $previousButton.attr("disabled", true);
        }
        else {
            $previousButton.removeClass("disabled");
            $previousButton.attr("disabled", false);
        }

        // Update part type UI and section UI.
        parts.fillSectionsUI($sections, question.partType);
        parts.markPart(question.partType);
        parts.markSection(question.section);
    }

    function getUserAnswer() {
        var q = questions[quesIndex-1];
        if(q.isRadio()) {
            // get radio
            var selected;
            $answerSelector.children("input").each(function() {
                if(this.checked == true) {
                    selected = this;
                }
            });
            if(typeof selected !== "undefined") {
                return selected.value;
            }
            else {
                return "";
            }
        }
        else if(q.isText()) {
            // get text;
            return $answerTextbox.find("input").val();
        }
        else if(q.isMultiLineText()) {
            return $writingTextBox.val();
        }
        else {
            // TODO: handle the exceptional case.
            return undefined;
        }
    }

    function updateQuesNav(questions) {
        $questionNav.empty();
        $questionNav.append("<div class='space'></div>")
        for (var i=questions.length; i >=1 ; i--) {
            var ques = questions[i-1];
            var $quesItem = $("<li/>").text(i)
            .data("index", i)
            .click(function(){
                var index = $(this).data("index");
                showQuestion(index);
            })
            .prependTo($questionNav);
            if(ques.hasAnswered()) {
                if(ques.isAnswerRight()) {
                    // mark as right.
                    $quesItem.removeClass("error").addClass("correct");
                }
                else {
                    // mark as error.
                    $quesItem.removeClass("correct").addClass("error");
                }
            }
        };
    }

    function categorize(questions) {
        var _parts = {};
        $(questions).each(function() {
            var part = _parts[this.partType];
            if (typeof part === "undefined"){
                part = { };
                _parts[this.partType] = part;
            }

            var section = part[this.section];
            if(typeof section === "undefined") {
                section = [];
                part[this.section] = section;
            }

            section.push(this);
        });

        return {
            getPartNames: function() {
                var names = [];
                for(var part in _parts) {
                    names.push(part);
                }
                return names;
            },
            getPart: function(partType) {
                return _parts[partType];
            },
            getOrderedQuestions: function() {
                var questions = [];
                for(var partType in _parts) {
                    var part = _parts[partType];
                    for(var sectionName in part) {
                        questions = questions.concat(part[sectionName]);
                    }
                }
                return questions;
            },
            getQuesCount: function(partType, sectionNum) {
                if(typeof partType === "undefined") {
                    return questions.length;
                }
                else if(typeof sectionNum === "undefined") {
                    var count = 0;
                    $(questions).each(function() {
                        if(this.partType == partType)
                            count ++
                    });
                    return count;
                }
                else {
                    var count = 0;
                    $(questions).each(function() {
                        if(this.partType == partType && this.section == sectionNum)
                            count ++
                    });
                    return count;
                }
            },
            fillPartsUI: function($partsContainer) {
                $partsContainer.empty();
                for(var partType in _parts) {
                    $partsContainer.append($("<a href='###'>" + partType + " (" + this.getQuesCount(partType) + ")</a>")
                    .click(function() {
                        var partType = $(this).data("partType");
                        var part = _parts[partType];
                        showQuestion(part["1"][0].number);
                    })
                    .data("partType", partType));
                }
                $partsContainer.append("<div class='space'></div>");
                this.$partsContainer = $partsContainer;
            },
            fillSectionsUI: function($sectionContainer, partType) {
                $sectionContainer.empty();
                var part = _parts[partType];

                for(var sectionId in part) {
                    var section = part[sectionId], question = section[0];
                    $sectionContainer.append($("<li href='###'>Section " + sectionId + " (" + this.getQuesCount(partType, sectionId) + ")</li>")
                    .click(function() {
                        $sectionContainer.find("li").addClass("on");
                        $(this).addClass("on");
                        showQuestion($(this).data("startQuesNum"));
                    })
                    .data("section", sectionId)
                    .data("startQuesNum", question.number));

                }
                //$sectionContainer.append("<div class='space'></div>");
                this.$sectionContainer = $sectionContainer;
            },
            markPart: function(partType) {
                if(typeof this.$partsContainer !== "undefined") {
                    this.$partsContainer.children().each(function() {
                        if($(this).data("partType") == partType) {
                            $(this).addClass("on");
                        }
                        else {
                            $(this).removeClass("on");
                        }
                    })
                }
            },
            markSection: function(sectionNum) {
                if(typeof this.$sectionContainer !== "undefined") {
                    this.$sectionContainer.children().each(function() {
                        if($(this).data("section") == sectionNum) {
                            $(this).addClass("on");
                        }
                        else {
                            $(this).removeClass("on");
                        }
                    })
                }
            }
        };
    }
}


function PracticeEngine(container, id, assignmentAttemptId, userAnswers) {
    var $container = $(container),
    viewModel,
    questions;

    $.ajax({
        url : "/examxml/" + id,
        dataType : "json",
        success : function(data) {
            initViewModel(eval(data));
        }
    });

    function initViewModel(jsonData) {
        questions = new Array();
        $.each(jsonData.exam.parts, function(idx, part) {
            var sound, timePoints, maxN = Number.MIN_VALUE; // maxN is got from <d><p></p></d> data.

            if(part.type == "listening") {
                sound = part.sound.replace(':8080', '');
                if(this.d != null) {
                    timePoints = { };
                    $.each(this.d.p, function(idx, p) {
                        timePoints[p['n']] = p['v'] * 1;
                        maxN = Math.max(maxN, p['n'])
                    });
                }
            }

            $.each(this.sections, function(idx, section) {
                var sectionNum = section.num;

                $.each(section.questions, function(idx, ques) {
                    var q = new Question(this, sectionNum);
                    // Either sound and timePoints are all assigned, or they are all not assigned.
                    if(typeof sound != "undefined") {
                        if(typeof timePoints != "undefined") {
                            function getBeginTime() {
                                for(var i = q.number; i >= 1; i--) {
                                    if(typeof timePoints[i] != "undefined") {
                                        return timePoints[i];
                                    }
                                }
                                return 0;
                            }

                            function getEndTime() {
                                // end time is the begin time of last question.
                                for(var i = q.number + 1; i <= maxN; i++) {
                                    if(typeof timePoints[i] != "undefined") {
                                        return timePoints[i];
                                    }
                                }
                                return Number.MAX_VALUE;
                            }

                            // The following line is for the purpose of debugging time points.
                            // alert("from " + getBeginTime() + " to " + getEndTime());
                            q.listening = new Listening(sound, getBeginTime(), getEndTime());
                        }
                        else {
                            q.listening = new Listening(sound)
                        }
                    }

                    questions.push(q);
                });
            });
        });

        // Attached existing user answers.
        if(typeof userAnswers != 'undefined') {
            for(var i = 0; i < questions.length; ++i ) {
                var ques = questions[i];
                var quesNum = ques.number;
                if(typeof userAnswers[quesNum] != 'undefined') {
                    ques.userAnswer = userAnswers[quesNum];
                }
            }
            if(userAnswers.length == questions.length) {
                g_mode = "review";
            }
            else {
                g_mode = "play";
            }
        }

        viewModel = new PracticeEngineViewModel(container,
        {
            "submitted" : function(index, answer) {
                var theQues = questions[index-1],
                    answersToPass = {},
                    tokentag, state;

                theQues.userAnswer = answer;
                $(questions).each(function() {
                    if(this.hasAnswered()) {
                        answersToPass[this.number] = this.userAnswer;
                    }
                });

                tokentag = $('#tokentag').val()
                state = getStatistics();

                var postSuccessCallback = function(data) {
                    viewModel.updateState(state);
                    viewModel.moveToNext();

                    if(theQues.isAnswerRight()) {
                        viewModel.markAsCorrect(index);
                    }
                    else {
                        viewModel.markAsError(index);
                    }

                    if(state.progress == state.totalCount) {
                        // Show statistic result.
                        g_mode = "review"
                        viewModel.showStats(state);
                        viewModel.moveToIndex(1);
                        CoreObjects.$ListenPlayer.jPlayer("pause");
                    }
                };

                if(assignmentAttemptId != null) {
                    $.post('/assignment_attempts/' + assignmentAttemptId + '/post_answer',
                        { 'user_answers': JSON.stringify(answersToPass), 'state': JSON.stringify(state), 'authenticity_token': tokentag },
                        postSuccessCallback, 'json');
                }
                else {
                    // directly invoke the callback.
                    postSuccessCallback();
                }
            }
        });

        //viewModel.setName(jsonData.name).text());
        viewModel.setQuestions(questions);
        viewModel.updateState(getStatistics())

        if(questions.length == 0) {
            // TODO: prompt that no questions available.
        }

    }

    function getStatistics() {
        var statsByType = {},
            rightCount = 0,
            fullScore = 0,
            progress = 0,
            score = 0;
        $(questions).each(function() {
            var item = statsByType[this.partType];
            if (typeof item === "undefined") {
                item = {
                    type: this.partType,
                    total: 0,
                    right: 0
                };
                statsByType[this.partType] = item;
            }

            item.total ++;
            fullScore += (this.score * 1);
            if(this.hasAnswered()) {
                progress ++;

                if(this.isAnswerRight()) {
                    item.right ++;
                    rightCount ++;
                    score += (this.score * 1);
                }
            }
        });

        var result = {
            statsByType: statsByType,
            totalCount: questions.length,
            rightCount: rightCount,
            score: Math.round(score * 100.0 / fullScore),
            progress: progress
        };
        return result;
    }

    this.close = function() {
        viewModel.close();
        CoreObjects.$BgPlayer.jPlayer("clearMedia");
        CoreObjects.$ListenPlayer.jPlayer("clearMedia");
        CoreObjects.$ExpPlayer.jPlayer("clearMedia");
    }
}


$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - scrollPane.offset().top - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
}
