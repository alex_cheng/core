/**
 * Created by alexcheng on 12/22/13.
 */

// Require jquery.jplayer.js
// Require jQuery

function initPlayer(playerId, containerId) {
    var paused = true, count = 0;
    var $audioPlayer = $("#"+playerId),
        audioPlayerData,
        options = {
            preload: "auto",
            ready: function (event) {
                // Hide the volume slider on mobile browsers. ie., They have no effect.
                if(event.jPlayer.status.noVolume) {
                    // Add a class and then CSS rules deal with it.
                    $(".jp-gui").addClass("jp-no-volume");
                }
            },
            timeupdate: function(event) {
                var beginTime = audioPlayerData.getBeginTime(),
                    endTime = audioPlayerData.getEndTime(),
                    current = event.jPlayer.status.currentTime,
                    duration = event.jPlayer.status.duration;

                myControl.progress.slider("value", (current - beginTime) * 100.0 / (endTime - beginTime));

                if(!event.jPlayer.status.paused) {
                    if(current >= endTime) {
                        $(this).jPlayer("stop");

                        if($(this).data("onEnded")) {
                            $(this).data("onEnded")();
                        }
                    }
                    else if(current < beginTime) {
                        $(this).jPlayer("playHead", beginTime / duration * 100);
                    }
                }
            },
            volumechange: function(event) {
                if(event.jPlayer.options.muted) {
                    myControl.volume.slider("value", 0);
                } else {
                    myControl.volume.slider("value", event.jPlayer.options.volume);
                }
            },
            swfPath: "/js",
            supplied: "mp3",
            cssSelectorAncestor: "#"+containerId,
            wmode: "window",
            play: function(event) {
                if(paused) {
                    paused = false;
                    $audioPlayer.data("paused", false);
                }
            },
            pause: function(event) {
                if(!paused) {
                    paused = true;
                    $audioPlayer.data("paused", true);
                }
            }
        },
        myControl = {
            progress: $(options.cssSelectorAncestor + " .jp-progress-slider"),
            volume: $(options.cssSelectorAncestor + " .jp-volume-slider")
        };

    // Instance jPlayer
    $audioPlayer.jPlayer(options);

    // A pointer to the jPlayer data object
    audioPlayerData = $audioPlayer.data("jPlayer");

    audioPlayerData._updateInterface = function() {
        if(this.css.jq.currentTime.length) {
            this.css.jq.currentTime.text($.jPlayer.convertTime(this.status.currentTime - this.getBeginTime()));
        }
        if(this.css.jq.duration.length) {
            this.css.jq.duration.text($.jPlayer.convertTime(this.getEndTime() - this.getBeginTime()));
        }
    }

    audioPlayerData.getBeginTime = function() {
        if(this.getMode() == "play") {
            return 0; // ignore begin and end time defined in XML.
        }
        else {
            var beginTime = $audioPlayer.data("beginTime");
            if(typeof beginTime == "undefined")
                beginTime = 0;
            return beginTime;
        }
    }

    audioPlayerData.getEndTime = function() {
        if(this.getMode() == "play") {
            return this.status.duration; // ignore begin and end time defined in XML.
        }
        else {
            var endTime = $audioPlayer.data("endTime");
            if(typeof endTime == "undefined")
                endTime = this.status.duration;
            return endTime;
        }
    }

    audioPlayerData.forceUpdateProgressBar = function () {
        myControl.progress.slider("value", 0);
    }

    audioPlayerData.getMode = function() {
        if(typeof g_mode == "undefined")
            return "play";
        else
            return g_mode;
    }

    // Define hover states of the buttons
    $('li.jp-volume-max,li.jp-mute,li.jp-unmute,li.jp-stop,li.jp-pause,li.jp-play').hover(
        function() { $(this).addClass('ui-state-hover'); },
        function() { $(this).removeClass('ui-state-hover'); }
    );

    // Create the progress slider control
    myControl.progress.slider({
        animate: "fast",
        max: 100,
        range: "min",
        step: 0.1,
        value : 0,
        slide: function(event, ui) {
            var sp = audioPlayerData.status.seekPercent;
            if(audioPlayerData.getMode() == "play")
                return; // Don't allow adjust progress in "play" mode.

            if(sp > 0) {
                // Move the play-head to the value and factor in the seek percent.
                var seekPos = (audioPlayerData.getEndTime() - audioPlayerData.getBeginTime()) * ui.value / 100.0 + audioPlayerData.getBeginTime();
                $audioPlayer.jPlayer("playHead", seekPos / audioPlayerData.status.duration * 100.0);
            } else {
                // Create a timeout to reset this slider to zero.
                setTimeout(function() {
                    myControl.progress.slider("value", 0);
                }, 0);
            }
        }
    });

    // Create the volume slider control
    myControl.volume.slider({
        animate: "fast",
        max: 1,
        range: "min",
        step: 0.01,
        value : $.jPlayer.prototype.options.volume,
        slide: function(event, ui) {
            $audioPlayer.jPlayer("option", "muted", false);
            $audioPlayer.jPlayer("option", "volume", ui.value);
        }
    });
}