#-*- coding: UTF-8 -*-

MYSQL_HOST = "127.0.0.1"
MYSQL_PORT = 3306
MYSQL_DATABASE = "discuz"
MYSQL_USER = "root"
MYSQL_PASSWORD = "12345"
MYSQL_TABLE_PRE = "discuz"

DOMAIN = u'http://%s/forum/' % MYSQL_HOST
#USERS = [(u"admin", u"admin")]
USERS = [(u"万老师", u"51kehouwin"),(u"小踢妈", u"51kehouwin")]
LOGINFIELD = u'username'
COOKIETIME = 2592000

HOMEURL = DOMAIN + u'forum.php'
LOGINURL = DOMAIN + u'member.php?mod=logging&action=login&loginsubmit=yes&frommessage&inajax=1'
POSTURL = DOMAIN + u'forum.php?mod=post&action=newthread&fid=FID&extra=&topicsubmit=yes'
REPLYURL = DOMAIN + u'forum.php?mod=post&action=reply&tid=TID&extra=&replysubmit=yes'


CACHE_POSTS = "cache_posts"
HISTORY_POSTED_LOG = "posted_list.log"
SCRIPT_LOG = "auto_post.log"
SYS_DECODE = 'utf-8'
FOLDER = "/pool/batchupload/bbs/pending"
POSTED_FOLDER = "/pool/batchupload/bbs/done"
ONE_DAY_MAX_POST = 2

#FOLDER = "articles"
#POSTED_FOLDER = "posted"

#CONTENT_DATA_SPAN = "|"*15
#CONTENT_DATA_SPLIT = "="*30 + "\r\n"*5
#CONTENT_SPLIT = 100

#forum_fid = 2