#-*- coding: UTF-8 -*-

import urllib
import urllib2
import cookielib 
import os, sys
import re
from datetime import date, datetime, timedelta
from time import strptime, strftime
import time
import codecs
import copy
import shutil
import random
import logging

import config

logging.basicConfig(filename=config.SCRIPT_LOG,\
                      level=logging.DEBUG, filemode = 'w', \
                      format = '%(asctime)s - %(levelname)s: %(message)s')

reload(sys)
sys.setdefaultencoding('utf8')
 
class Discuz(object):
    def __init__(self):
        self.operate = ''  # response的对象（不含read）
        self.formhash = ''  # 没有formhash不能发帖
        
        self.cj = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        urllib2.install_opener(self.opener)
 
        self.formhash_pattern = re.compile(r'<input type="hidden" name="formhash" value="([0-9a-zA-Z]+)" />')
    
    def login(self, user, questionid = 0, answer = ''):
        postdata = {
                         'loginfield': config.LOGINFIELD,
                         'username': user[0],
                         'password': user[1],
                         'questionid': questionid,
                         'answer': answer,
                         'cookietime': config.COOKIETIME,
        }
        
        login_success_pattern = re.compile(ur"\('succeedlocation'\).innerHTML = '(?u)(.+)，现在将转入登录前页面';")
        login_fail_pattern = re.compile(r"{errorhandle_\('(?u)(.+)', {'loginperm':")
 
        # 取得登录成功/失败的提示信息
        self.operate = self._get_response(config.LOGINURL, postdata)
        login_tip_page = self.operate.read().decode('utf-8')
        login_success_info = login_success_pattern.search(login_tip_page)
        login_fail_info = login_fail_pattern.search(login_tip_page)
    
        # 显示登录成功/失败信息
        if login_success_info:
            print login_success_info.group(1)
            self.formhash = self._get_formhash(self._get_response(config.HOMEURL).read())
            return True
        elif login_fail_info:
            print login_fail_info.group(1)
        else:
            print '无法获取登录状态'
        return False
 
    def _get_response(self, url, data = None):
        if data is not None:
            req = urllib2.Request(url, urllib.urlencode(data))
        else:
            req = urllib2.Request(url)
        
        response = self.opener.open(req)
        return response
    
    def _get_formhash(self, page_content):
        try:
            self.formhash = self.formhash_pattern.search(page_content).group(1)
            return self.formhash
        except Exception, ex:
            print str(ex)
    
    def _interception(self, string, length):
        if (len(string) > (length / 3)):
            return string[0:(length / 3 - 1)] + '...'
        else:
            return string


class Post(Discuz):
    def __init__(self):
        super(Post, self).__init__()
 
        self.post_success_pattern = re.compile(r'<meta name="keywords" content="(?u)(.+)" />')  # 发帖成功时匹配
        self.post_fail_pattern = re.compile(r'<div id="messagetext" class="alert_error">')  # 发贴失败时匹配
        self.post_error_pattern = re.compile(r'<p>(?u)(.+)</p>')  # 发贴失败的错误信息
        
    def newthread(self, fid, subject, message):
        postdata = {
                    'subject': subject,
                    'message': message,
                    'formhash': self.formhash,
        }
        
        base_url = config.POSTURL
        url = base_url.replace('FID', fid)
        self.operate = self._get_response(url, postdata)

        prefix = '主题 "%s" ' % postdata['subject']
        return self.__verify_post_status(prefix)
        
    def reply(self, tid, message):
        postdata = {
                    'message': message,
                    'formhash': self.formhash,
        }
        
        base_url = config.REPLYURL
        url = base_url.replace('TID', tid)
        self.operate = self._get_response(url, postdata)
        
        prefix = '回复 "%s" ' % self._interception(message, 80)
        self.__verify_post_status(prefix)
    
    def __verify_post_status(self, prefix):
        page_content = self.operate.read()
        
        p = re.compile(ur"forum.php\?mod=viewthread\&tid=\d+")
        r = p.search(page_content)
        if r:
            tid = r.group().split("=")[-1]
        
        if self.post_success_pattern.search(page_content):
            print "%s发布成功！" % prefix
        elif self.post_fail_pattern.search(page_content):
            post_error_message = self.post_error_pattern.search(page_content)
            try:
                print "%s发布失败！原因是：%s。" % (prefix, post_error_message.group(1))
            except:
                print "%s发布失败！原因是：未知原因。" % prefix
        else:
            print "无法确定%s发布状态" % prefix
        return tid


class AutoPost():
    def __init__(self):
        self.current_time = "0000-00-00 00:00:00"
        self.has_posted = 0
        self.file_list = []
        self.history_log = {}
        
    def get_post_files(self):
        file_list = []
        files = os.listdir(config.FOLDER)
        for fid in files:
            try:
                fid = int(fid)  # test forum's fid is digiter
                fid = str(fid)
                files_in = os.listdir(os.path.join(config.FOLDER, fid))
                for f in files_in:
                    if f[0] != ".":  # remove hidden file
                        f_name = encoding_utf8(f)
                        p = os.path.join(config.FOLDER, fid, f)
                        if(os.path.isfile(p)):
                            file_list.append({"f_name": f, "f_subject": f_name[:f_name.rfind(".")], "is_file": True, "fid": fid, })
                        elif os.path.isdir(p):
                            ffs = os.listdir(p)
                            ffs.sort()
                            for ff in ffs:
                                file_list.append({"f_name": f, "f_subject": f_name, "is_file": False, "sub_f_name": ff, "fid": fid, })
                                break
            except Exception, ex:
                pass
        self.file_list = file_list
        logging.info("start new posting")
        logging.info("read files:")
        logging.info("\n -- ".join([f["f_subject"] for f in file_list]))
        logging.info("start to posting thread ...")
        logging.info("="*30)
        return file_list
    
    def get_history_log(self):
        c = ""
        if os.path.exists(config.CACHE_POSTS):
            fp = codecs.open(config.CACHE_POSTS, "r+", config.SYS_DECODE)
            c = fp.read()
            fp.close()
        if c:
            self.history_log = eval(c)
        else:
            self.history_log = {}
            
    def set_history_log(self):
        fp = codecs.open(config.CACHE_POSTS, "w+", config.SYS_DECODE)
        fp.write(repr(self.history_log))
        fp.close()
    
    def start_post(self):
        for file in self.file_list:
            try:
                if self.has_posted < config.ONE_DAY_MAX_POST:
                    if file["is_file"]:
                        fp = codecs.open(os.path.join(config.FOLDER, file["fid"], file["f_name"]), "r+")
                    else:
                        fp = codecs.open(os.path.join(config.FOLDER, file["fid"], file["f_name"], file["sub_f_name"]), "r+")
                    content = fp.read()
                    fp.close()
                    if content:
                        logging.info("start to post: %s" % encoding_utf8(file["f_subject"]).encode("utf-8"))
                        file["f_content"] = encoding_utf8(content) #.decode(config.SYS_DECODE)
                        #content = self.format_content(file)
                        self.post_content(file)
                        sleep_time = 60*60*2 + int(60*60*(2*random.random()-1))
                        logging.info("sleep %d seconds to next file." % sleep_time)
                        time.sleep(sleep_time)
                    else:
                        logging.debug("no content in %s" % file["f_subject"])
                else:
                    pass
                    #logging.debug("have post %s today %s" % self.has_posted)
            except Exception, ex:
                import traceback
                logging.error(repr(traceback.format_exc()))
                
    def post_content(self, file):
        history = self.history_log.get(file["f_subject"])
        last_date, last_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S").split()
        if last_time < "07:00:00" or last_time > "23:30:00":
            logging.debug("now time: not in 07:00:00 ~ 23:30:00")
            return
        if history:
            if history["last_date"] >= last_date:
                logging.debug("wait for next day")
                return 
            user = history["user"]
        else:
            user = random.choice(config.USERS)
        my_account = Post()
        my_account.login(user)
        if history:
            cur_post_tid = history["tid"]
            my_account.reply(cur_post_tid, file["f_content"])
        else:
            cur_post_tid = my_account.newthread(file["fid"], \
                                                file["f_subject"], \
                                                file["f_content"])
            #cur_post_tid = my_account.get_post_tid(file["f_subject"])
        self.history_log.setdefault(file["f_subject"], {"user": user, \
                                                        "last_date": last_date, \
                                                        "last_time": last_time, \
                                                        "tid": cur_post_tid, })
        self.history_log[file["f_subject"]]["last_date"] = last_date
        self.history_log[file["f_subject"]]["last_time"] = last_time
        self.history_log[file["f_subject"]]["tid"] = cur_post_tid
        self.set_history_log()
        self.has_posted += 1
        #self.add_posted_log(file["f_subject"], self.history_log[file["f_subject"]])
        if file["is_file"]:
            try:
                os.makedirs(os.path.join(config.POSTED_FOLDER, file["fid"]))
            except:
                pass
            shutil.move(os.path.join(config.FOLDER, file["fid"] ,file["f_name"]), \
                        os.path.join(config.POSTED_FOLDER, file["fid"], file["f_name"]))
        else:
            p_dir = os.path.join(config.POSTED_FOLDER, file["fid"], file["f_name"])
            if not os.path.exists(p_dir):
                os.makedirs(p_dir)
            shutil.move(os.path.join(config.FOLDER, file["fid"], file["f_name"], file["sub_f_name"]), \
                        os.path.join(config.POSTED_FOLDER, file["fid"], file["f_name"], file["sub_f_name"]))
            try:
                os.rmdir(os.path.join(config.FOLDER, file["fid"], file["f_name"]))
            except:
                pass
    
    def add_posted_log(self, subject, content):
        c = copy.deepcopy(content)
        c["f_subject"] = subject.encode("utf8")
        fp = codecs.open(config.HISTORY_POSTED_LOG, "a+", config.SYS_DECODE)
        fp = open(config.HISTORY_POSTED_LOG, "a+")
        fp.write(str(c)+"\n")
        fp.close()
    
    def run(self):
        if self.current_time.split()[0] != datetime.now().strftime("%Y-%m-%d"):
            self.current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self.has_posted = 0
            self.get_history_log()
            self.get_post_files()
            self.start_post()

#    def format_content(self, file):
#        content = file["f_content"]
#        if content:
#            f = open(file["f_path"], "w")
#            try:
#                content = content.decode(config.SYS_DECODE)
#            except Exception, ex:
#                pass
#            try:
#                datalist, data = split_data([], content)
#                user = random.choice(config.USERS)
#                add_str = "0%s-1%s%s%s%s" % (config.CONTENT_DATA_SPAN, \
#                                         config.CONTENT_DATA_SPAN, \
#                                         str(get_time_change(str(self.current_time), day=-1)), \
#                                         config.CONTENT_DATA_SPAN, \
#                                         "%s+++%s" % (user[0], user[1]))
#                datalist.insert(0, add_str)
#                file["f_content"] = config.CONTENT_DATA_SPLIT.join(datalist)
#                f.write(file["f_content"])
#                f.close()
#            finally:
#                return file
#    
#    def post_content_old(self, file):
#        try:
#            cl = file["f_content"].split(config.CONTENT_DATA_SPLIT)
#            tmp_ = cl[0].split(config.CONTENT_DATA_SPAN)
#            cur_post = int(tmp_[0]) + 1
#            cur_post_tid = int(tmp_[1])
#            cur_post_datetime = str(tmp_[2])
#            cur_user = str(tmp_[3]).split("+++")
#            post_data = cl[min(cur_post, (len(cl)-1))]
#            
#            if cur_post_datetime:
#                my_account = Post()
#                if cur_post_tid <= 0:
#                    my_account.login(user)
#                    cur_post_id = my_account.newthread(str(config.FORUM_FID), \
#                                                       file["f_subject"][:file["f_subject"].rfind(".")], \
#                                                       post_data)
#                    #cur_post_tid = my_account.get_post_tid(file["f_subject"])
#                    add_str = "%s%s%s%s%s%s" % (  cur_post, \
#                                                  config.CONTENT_DATA_SPAN, \
#                                                  cur_post_tid, \
#                                                  config.CONTENT_DATA_SPAN, \
#                                                  str(self.current_time),
#                                                  str(user))
#                    cl[0] = add_str
#                    file["f_content"] = config.CONTENT_DATA_SPLIT.join(datalist)
#                else:
#                    my_account.login(random.choice(config.USERS))
#                    my_account.reply(cur_post_tid, post_data)
#        except Exception, ex:
#            print "post_content error: ", str(ex)
#    
#    def update_file(self, file):
#        f = open(file["f_path"], "w")
        
   
        
#def split_data(datalist, data):
#    if data:
#        if len(data) <= config.CONTENT_SPLIT*3/2:
#            datalist.append(data)
#            data = ""
#        else:
#            t = data[:config.CONTENT_SPLIT]
#            t_l = data[config.CONTENT_SPLIT:].split("\n")
#            t = t + t_l.pop(0)
#            datalist.append(t)
#            data = "\n".join(t_l)
#            split_data(datalist, data)
#    return datalist, data
#    
# return the time changed
def get_time_change(oldTime,year=0,month=0,day=0,hour=0,minute=0,second=0):
    if len(oldTime.split(" "))==2:
        FMTTIME="%Y-%m-%d %H:%M:%S"
    elif len(oldTime.split(" "))==1:
        FMTTIME="%Y-%m-%d"
    else:
        FMTTIME="%Y-%m-%d"
    try:
        timeTouple = time.strptime(oldTime, FMTTIME)
        if len(timeTouple) == 9:
            year += timeTouple[0]
            month += timeTouple[1]
            day += timeTouple[2]
            hour += timeTouple[3]
            minute += timeTouple[4]
            second += timeTouple[5]
            tmp1 = timeTouple[6]
            tmp2 = timeTouple[7]
            tmp3 = timeTouple[8]
            newTimeTouple = (year, month, day, hour, minute, second, tmp1, tmp2, tmp3)
            return time.strftime(FMTTIME, time.localtime(time.mktime(newTimeTouple)))
        else:
            return oldTime
    except:
        logging.error("get_time_change error: wrong out_time in olist: " + str(oldTime))
        return oldTime
    

def encoding_utf8(stri):
    for c in ('utf-8', 'gbk', 'big5', 'jp', 'euc_kr','utf16','utf32'): 
        try: 
            return stri.decode(c).encode('utf8') 
        except: 
            pass 
    return stri 

def main():
    autopost = AutoPost()
    while True:
        autopost.run()
        sleep_time = 60*60*4 + int((2*random.random()-1)*60*60)
        now_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        next_time = get_time_change(now_time, second=sleep_time)
        logging.debug("finished in, waiting next time %s ..." % (next_time))
        time.sleep(sleep_time)

if __name__ == "__main__":
    main()
    
#if __name__ == "__main__":
#    my_account = Post()
#    my_account.login(config.USERNAME, config.PASSWORD)
#    my_account.newthread('2', u'....2', u'....2')
#    #my_account.reply('10', u'....')
    


