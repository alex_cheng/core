// JavaScript Document

function getPageSize()
{  
	var xScroll, yScroll;
	if (window.innerHeight && window.scrollMaxY)
	{
		xScroll = document.body.scrollWidth + window.scrollMaxX;
		yScroll = window.innerHeight + window.scrollMaxY;
	}
	else if (document.body.scrollHeight > document.body.offsetHeight)
	{ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	}
	else
	{ // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.scrollWidth;
		//xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight)
	{  // all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	}
	else if (document.documentElement && document.documentElement.clientHeight)
	{ // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	}
	else if (document.body)
	{ // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}  
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight)
	{
		pageHeight = windowHeight;
	}
	else
	{ 
		pageHeight = yScroll;
	}
	
	if(xScroll < windowWidth)
	{  
		pageWidth = windowWidth;
	}
	else
	{
		pageWidth = xScroll;
	}
	
	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight,xScroll,yScroll) 
	return arrayPageSize;
}

function unicode(s){
	var len=s.length;
	var rs="";
	for(var i=0;i<len;i++){
	var k=s.substring(i,i+1);
	rs+="\\u"+s.charCodeAt(i)+";";
	}
	return rs;
	//alert(rs);
}

/***** float msg obj on top *****/  
var ajaxGlobalMsgT = null;
function showGlobalAjaxMsg(msg, style, timeout){
	if(msg!="")
	{
		var pageSizes = getPageSize();
		var MsgDiv = $("#divAjaxGlobalMsg");
		$(MsgDiv).removeClass("warningMsgDiv");
		$(MsgDiv).css("width","");
		if(ajaxGlobalMsgT!=null)
		{
			clearTimeout(ajaxGlobalMsgT);
		}
		if(style=="warning")
		{
			$(MsgDiv).addClass("warningMsgDiv");
		}
		$(MsgDiv).html("<p>"+msg+"</p>");
		var width = $(MsgDiv).width();
		$(MsgDiv).css("left", ((pageSizes[2]-width)/2)+"px");
		$(MsgDiv).css("width", width+"px");
		$(MsgDiv).css("display",'inline');
		ajaxGlobalMsgT = setTimeout("hideGlobalAjaxMsg()", timeout);
	}
	else
	{
		hideGlobalAjaxMsg();
	}
}
function hideGlobalAjaxMsg()
{
	$("#divAjaxGlobalMsg").hide();
	ajaxGlobalMsgT = null;
}
/***** float msg obj on top *****/  


// add help tip to each label
function addHelpTipsFunc()
{
	$('.tips').mousemove(function(event){
		
		var current = $(this);
		var flag = 0;
		
		var text = "";
		var text1 = $(this).attr("title");
		var text2 = $(this).attr("notice");
		
		if(text2&&text2!="")
		{
			text = 	text2;
		}
		else
		{
			if(text1&&text1!="")
			{
				text = text1;	
			}
		}
		
		if($(".helpTip").html()==null||$(".helpTip").html()=="")
		{
			
			if(text!="")
			{
				$("body").append("<div class='helpTip'><div class='tipContent'>"+text+"</div></div>");	
			}
		}
		
		if(event.pageX+$(".helpTip").width()>($("body").width()-25))
		{
			$(".helpTip").addClass("helpTipRight");
			$(".helpTip").css("left",event.pageX-$(".helpTip").width() - 5+"px");
			$(".helpTip").css("top",event.pageY -8 +"px");
		}
		else
		{
			$(".helpTip").removeClass("helpTipRight");
			$(".helpTip").css("left",event.pageX + 15+"px");
			$(".helpTip").css("top",event.pageY -8 +"px");	
		}
		$(".helpTip").mouseout(function(){
			$(this).remove("div");
		})
	});
	$('.tips').mouseout(function(){
		$(".helpTip").remove("div");
	});
	
	
	$(".helpTips").bind("mousemove",function(event){
	})
	$(".helpTips").bind("mouseout",function(){
		$(this).remove("div");
	})
}

/* remove duplicate value in list */
function distinct_list(list)
{
	var arr = [];
	var len = list.length;

	for ( var i = 0; i < len; i++ )
	{
		for( var j = i+1; j < len; j++ )
		{
			if( list[i] === list[j] ){
				j = ++i;
			}
		}
		arr.push( list[i] );
	}
	return arr;
}

/* replace upload img name to "_" in it */
function change_img_name(src, type)
{
	if(type==1)
	{
		return src.split(".")[0]+"_."+src.split(".")[1];
	}
	else
	{
		return src.replace("_.", ".");
	}
}

/* limit input
	type: 1: only alphabet
*/
function format_input(id, type)
{
	var txt = $("#"+id).val();
	if(type == 1)
	{
		var regex = /[a-zA-Z0-9]/;
		var str = ""
		for(var a=0; a<txt.length; a++)
		{
			if(regex.test(txt[a]))
			{
				str += txt[a];
			}
		}
	}
	$("#"+id).val(str);
}


function set_disabled(type,obj)
{
	if(type)
	{
		$(obj).addClass("errorInput");	
	}
	else
	{
		$(obj).removeClass("errorInput");
	}
}

function re_val(type, str_in)
{
	var re = /^/
	var msg = "";
	if (type == "email")
	{
		re =  /^[a-zA-Z0-9_][a-zA-Z0-9_\-\.]*@[a-zA-Z0-9_\-]+(\.[a-zA-Z0-9_\-]+)+$/
		msg = "eg: xxx@xxx.xx";
	}
	else if(type == "password" || type == "name")
	{
		re =  /^[a-zA-Z0-9_]{3,12}/
		msg = "must be 3-12 alphabet or number";
	}
	return [re.test(str_in), msg];
}